﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Sams.Dtos
{
   
    /// <summary>
    /// Benutzerobjekt zur Rechtedefinition und Authentifikation
    /// </summary>
    public class BenutzerDto
    {
      
        /// <summary>
        /// LDAP-Active Directory Identifikator des Benutzers
        /// </summary>
        public string Sid            { get; set; }
        /// <summary>
        /// Voller Anmeldename bei Windows (CUBEOFFICE\\xx)
        /// </summary>
        public string Loginname      { get; set; }
        /// <summary>
        /// Vor und Zuname des Mitarbeiters
        /// </summary>
        public string Name           { get; set; }
        /// <summary>
        /// Domänenname zur Anfrage des richtigen AD-Controllers
        /// </summary>
        public string Organization   { get; set; }
        /// <summary>
        /// E-Mailadrese des Mitarbeiters oder leer bei Systemaccount
        /// </summary>
        public string EmailAddress   { get; set; }
        /// <summary>
        /// Mitarbeiter darf alle Meldungen einsehen
        /// </summary>
        public bool   HasRoleGF      { get; set; }
        /// <summary>
        /// Mitarbeiter darf Meldungen erstellen
        /// </summary>
        public bool   HasRoleMA      { get; set; }
        /// <summary>
        /// Mitarbeiter darf Meldungen bearbeiten
        /// </summary>
        public bool   HasRoleQS      { get; set; }
        /// <summary>
        /// Mitarbeiter ist Admin
        /// </summary>
        public bool   HasRoleADM     { get; set; }
        /// <summary>
        /// Mitarbeiter ist Risikobewerter
        /// </summary>
        public bool   HasRoleRM      { get; set; }
    }
}
