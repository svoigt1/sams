﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Sams.Dtos
{
    
    /// <summary>
    /// Meldungsliste geben
    /// </summary>
    public class MeldungListDto
    {

        /// <summary>
        /// Gesamtanzahl Meldungen die bei dieser Filterung gefunden werden würden
        /// </summary>
        public int InlineCount       { get; set; }
        /// <summary>
        /// Meldungszähler zur Zuordnung bei asynchronen Zugriff
        /// </summary>
        public int Draw              { get; set; }
        /// <summary>
        /// Eine Seite der Meldungsliste
        /// </summary>
        public List<MeldungDto> Data { get; set; }
    }
}
