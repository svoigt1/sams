﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Sams.Dtos
{
   
    /// <summary>
    /// Person zur Identifikation. 
    /// </summary>
    public class PersonDto
    {
       
        /// <summary>
        /// Datenbankschlüssel, ist 0 wenn er direkt aus dem AD gelesen wurde
        /// </summary>
        public int Id                { get; set; }
        /// <summary>
        /// LDAP-Active Directory Identifikator des Benutzers
        /// </summary>
        public string Sid            { get; set; }
        /// <summary>
        /// Voller Anmeldename bei Windows (CUBEOFFICE\\xx)
        /// </summary>
        public string Loginname      { get; set; }
        /// <summary>
        /// Vor und Zuname des Mitarbeiters
        /// </summary>
        public string Name           { get; set; }
        /// <summary>
        /// Domänenname zur Anfrage des richtigen AD-Controllers
        /// </summary>
        public string Organization   { get; set; }
        /// <summary>
        /// E-Mailadrese des Mitarbeiters oder leer bei Systemaccount
        /// </summary>
        public string EmailAddress   { get; set; }
    }
}
