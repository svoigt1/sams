﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Sams.Dtos
{

    /// <summary>
    /// Filterung einzelner Attribute
    /// </summary>
    public class MeldungFilterPair
    {

        /// <summary>
        /// Zu filterndes Attribut
        /// </summary>
        public String key      { get; set; }
        /// <summary>
        /// Suchtext
        /// </summary>
        public String value    { get; set; }
    }


    /// <summary>
    /// Sortierung einzelner Attribute, z.Z. nur eine Ebene unterstützt
    /// </summary>
    public class MeldungSortPair
    {

        /// <summary>
        /// Zu sortierendes Attribut
        /// </summary>
        public String key      { get; set; }
        /// <summary>
        /// asc - aufsteigend, desc - absteigend sortieren
        /// </summary>
        public String order    { get; set; }
    }


    /// <summary>
    /// Filterobjekt für Suche nach Meldungen
    /// </summary>
    public class MeldungFilter
    {

        /// <summary>
        /// Anzahl Sätze die auf dieser Seite zurückgegeben werden sollen
        /// </summary>
        public int                  Count           { get; set; } 
        /// <summary>
        /// darzustellende Seite, ab 1 beginnend
        /// </summary>
        public int                  Page            { get; set; }    
        /// <summary>
        /// "all" - alle MEldungen sehen, "private" - meine Meldungen sehen
        /// </summary>
        public String               Ownership       { get; set; } 
        /// <summary>
        /// Pakete werden nummeriert damit sie bei asynchronen Zugriff zugeordnet werden können 
        /// </summary>
        public int                  Draw            { get; set; }    
        /// <summary>
        /// Volltextsuche über Stichwort
        /// </summary>
        public String               Query           { get; set; } 
        /// <summary>
        /// Id's der Meldetypen die angezeigt werden sollen 
        /// </summary>
        public int[]                MeldeTypen      { get; set; }
        /// <summary>
        /// Filterung einzelner Attribute
        /// </summary>
        public MeldungFilterPair[]  Filter          { get; set; }
        /// <summary>
        /// Sortierung einzelner Attribute
        /// </summary>
        public MeldungSortPair[]    Sorting         { get; set; }
    }
}
