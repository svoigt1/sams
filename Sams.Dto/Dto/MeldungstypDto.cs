﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Sams.Dtos
{

    /// <summary>
    /// Typ wie Bevor, Cirs, ...
    /// </summary>
    public class MeldungstypDto 
    {

        /// <summary>
        /// Datenbankschlüssel
        /// </summary>
        public int Id               { get; set; }
        /// <summary>
        /// Meldungstypname
        /// </summary>
        public String   Name        { get; set; }
        /// <summary>
        /// Farbe des Meldungstyps, wird SAMS-weit zur besseren Erkennung verwendet
        /// </summary>
        public String   Farbe       { get; set; }
        
        /// <summary>
        /// Flag, Vorschau Btn zeigen aber nicht klickbar, wenn false
        /// </summary>
        public bool IstAktiv { get; set; }
    }
}
