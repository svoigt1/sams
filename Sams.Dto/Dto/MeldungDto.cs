﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Sams.Dtos
{
    
    /// <summary>
    /// Meldung zur Anzeige und zur Bearbeitung
    /// </summary>
    public class MeldungDto
    {

        /// <summary>
        /// Datenbankschlüssel
        /// </summary>
        public int              Id                  { get; set; }
        /// <summary>
        /// Nummer der Meldung zur Referenz, muss noch spezifiziert werden (bleibt leer bei Neuerstellung durch den Melder)
        /// </summary>
        public String           Nummer              { get; set; }
        /// <summary>
        /// Melder wünscht keine Speicherung seiner Identität (Backend liest den Melder nicht aus dem AD aus)
        /// </summary>
        public bool             IstAnonym           { get; set; }
        /// <summary>
        /// Bevor, Cirs, (es wird das gesamte Meldungstyp-Objekt gegeben, damit die Farbinformationen enthalten sind. 
        /// Bei Neuerstellung kann auch einfach ein Objekt gegeben werden bei dem die nur die Id gesetzt ist, Name und Color bleiben leer) 
        /// </summary>
        public MeldungstypDto   Meldungstyp         { get; set; }
        /// <summary>
        /// ID Meldungstyp zum Senden neuer Meldung
        /// </summary>
        public int              Meldungstyp_Id      { get; set; }
        /// <summary>
        /// Erstellung der Meldung (nicht unbedingt Zeitpunkt des Auftretens des Vorkommnisses, bleibt leer bei Neuerstellung durch den Melder)
        /// </summary>
        public DateTime         Erstellt            { get; set; }
        /// <summary>
        /// alternative Angabe des Melders (falls Station nur ein AD-Account besitzt) 
        /// </summary>
        public String           Meldername          { get; set; }
        /// <summary>
        /// alternative Angabe der E-Mail des Melders (falls Station nur ein AD-Account besitzt) 
        /// </summary>
        public String           MelderEMail         { get; set; }
        /// <summary>
        /// Textform der Einrichtung zu einfachen Darstellung (bleibt leer bei Neuerstellung durch den Melder)
        /// </summary>
        public String           Einrichtung         { get; set; }
        /// <summary>
        /// zum Speichern der Meldung: ID der Einrichtung erstes Level (z.B. Krankenhaus), Pflichtfeld
        /// </summary>
        public int              Einrichtung1_Id     { get; set; }
        /// <summary>
        /// zum Speichern der Meldung: ID der Einrichtung zweites Level (z.B. Abteilung), Pflichtfeld
        /// </summary>
        public int              Einrichtung2_Id     { get; set; }
        /// <summary>
        /// zum Speichern der Meldung: ID der Einrichtung drittes Level (z.B. Station), optional
        /// </summary>
        public int?             Einrichtung3_Id     { get; set; }
        /// <summary>
        /// zum Speichern der Meldung: ID der Einrichtung viertes Level (z.B. Raum), optional
        /// </summary>
        public int?             Einrichtung4_Id     { get; set; }
        /// <summary>
        /// Neu = 0, *Bearbeitung = 1*, Wartend = 2, Abgeschlossen = 3 (bleibt leer bei Neuerstellung durch den Melder)
        /// </summary>
        public int              Status              { get; set; }
        /// <summary>
        /// Meldung ist öffentlich (bleibt leer bei Neuerstellung durch den Melder)
        /// </summary>
        public bool             IstPublic           { get; set; }
        /// <summary>
        /// Undefiniert = 0, Abgelehnt = 1, Akzeptiert = 2, Realisiert = 3, Geplant = 4,  Spam = 5 (bleibt leer bei Neuerstellung durch den Melder)
        /// </summary>
        public int              Ergebnis            { get; set; }
        /// <summary>
        /// Begründung Ergebnis durch den Sachbearbeiter (bleibt leer bei Neuerstellung durch den Melder)
        /// </summary>
        public String           Begruendung         { get; set; }
        /// <summary>
        /// Titel der Meldung, ausgefüllt vom Melder
        /// </summary>
        public String           Betreff             { get; set; }
        /// <summary>
        /// Body der Meldung, ausgefüllt vom Melder
        /// </summary>
        public String           Kurzbeschreibung    { get; set; }
        /// <summary>
        /// Verbesserungsvorschlag, ausgefüllt vom Melder, optional
        /// </summary>
        public String           Vorschlag           { get; set; }
        /// <summary>
        /// Undefiniert= 0, Planung= 1, Personal= 2, Organisation= 3,OperativesGeschäft= 4,Rechnungswesen= 5,Sicherheit= 6,ITRisiken= 7,
        /// JuristischeRisiken  = 8,MedizinischeRisiken = 9,Umweltschutz= 10 (bleibt leer bei Neuerstellung durch den Melder)
        /// </summary>
        public int              Risikoklasse        { get; set; }
        /// <summary>
        /// Melder (Anzeige bei QS, bleibt leer bei Neuerstellung durch den Melder)
        /// </summary>
        public PersonDto        Melder              { get; set; }
        /// <summary>
        /// Bearbeiter der Meldung (Anzeige bei QS, bleibt leer bei Neuerstellung durch den Melder)
        /// </summary>
        public PersonDto        Bearbeiter          { get; set; }
        /// <summary>
        /// Verantwortlicher der Meldung (Anzeige bei QS, bleibt leer bei Neuerstellung durch den Melder)
        /// </summary>
        public PersonDto        Verantwortlicher    { get; set; }
    }
}
