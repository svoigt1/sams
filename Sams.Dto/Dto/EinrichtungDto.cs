﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Sams.Dtos
{

    /// <summary>
    /// Einrichtungsobjekt, in bis zu vier Ebenen verschachtelt (die Hierarchie ist in den einzelnen Krankehäusern unterschiedlich)
    /// </summary>
    public class EinrichtungDto
    {

        public int      Id      { get; set; }
        public String   Name    { get; set; }
        public ICollection<EinrichtungDto>  Children    { get; set; }
    }
}
