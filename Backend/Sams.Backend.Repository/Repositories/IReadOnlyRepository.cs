﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;


namespace Sams.Repositories
{
   
    public interface IReadOnlyRepository<T> where T : class
    {
    
        IQueryable<T>        Get();
        
        Task<T>              FirstAsync(Expression<Func<T,bool>> match, IEnumerable<Expression<Func<T, object>>> includes = null, CancellationToken ct = default(CancellationToken));
        Task<T>              SingleAsync(Expression<Func<T,bool>> match, IEnumerable<Expression<Func<T, object>>> includes = null, CancellationToken ct = default(CancellationToken));
        Task<T>              GetByIDAsync(object id, CancellationToken ct = default(CancellationToken));
        Task<List<TResult>>  FindAllAsync<TResult>(IEnumerable<Expression<Func<T, bool>>> predicate = null,
                                                   Func<IQueryable<T>, IOrderedQueryable<T>> order = null,
                                                   IEnumerable<Expression<Func<T, object>>> includes = null,
                                                   Expression<Func<T, TResult>> selector = null,
                                                   int? start = null, int? pageSize = null, 
                                                   CancellationToken ct = default(CancellationToken));
        Task<int>            CountAsync<TResult>(IEnumerable<Expression<Func<T, bool>>> predicate = null, CancellationToken ct = default(CancellationToken));
        Task<decimal?>       SumAsync<TResult>(Expression<Func<T, decimal?>> selector, IEnumerable<Expression<Func<T, bool>>> predicate = null, CancellationToken ct = default(CancellationToken));
    }
}
