﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;


namespace Sams.Repositories
{

    public interface IRepository<T> where T : class
    {
    
        IQueryable<T>  Get();

        Task<T>        GetByIDAsync(object id, CancellationToken ct = default(CancellationToken));
        Task<T>        FirstAsync(Expression<Func<T, bool>> match, IEnumerable<Expression<Func<T, object>>> includes = null, CancellationToken ct = default(CancellationToken));
        Task<T>        SingleAsync(Expression<Func<T, bool>> match, IEnumerable<Expression<Func<T, object>>> includes = null, CancellationToken ct = default(CancellationToken));
        Task<List<T>>  FindAllAsync(IEnumerable<Expression<Func<T, bool>>> where = null,
                                    Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
                                    IEnumerable<Expression<Func<T, object>>> includes = null,
                                    int? start = null, int? pageSize = null, 
                                    CancellationToken ct = default(CancellationToken));

        void           Delete(int id);
        void           Delete(IEnumerable<T> modellist);
        void           Delete(T model);
        void           Insert(T entity);
        void           Insert(IEnumerable<T> modellist);
        void           Update(T entity);
    }
}
