﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;


namespace Sams
{
    
    public static class RepositoryExtensions
    {

        public static Task<List<TSource>> MaterializeAsync<TSource>(this IQueryable<TSource> source, CancellationToken ct = default(CancellationToken))    
        {
            return source.ToListAsync(ct);   
        }


        public static Task<TSource> FirstAsync<TSource>(this IQueryable<TSource> source, CancellationToken ct = default(CancellationToken))    
        {
            return source.FirstOrDefaultAsync(ct);   
        }
    }
}
