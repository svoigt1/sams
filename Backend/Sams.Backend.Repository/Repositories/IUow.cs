﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;


namespace Sams.Repositories
{
  
    public interface IUow
    {

        IReadOnlyRepository<TModel> Reader<TModel>() where TModel: class;
        IRepository<TModel>         Writer<TModel>() where TModel: class;
        
        IQueryable<TModel>          ReaderQuery<TModel>() where TModel: class;
        IQueryable<TModel>          WriterQuery<TModel>() where TModel: class;
        
        Task                        SaveAsync(System.Threading.CancellationToken ct = default(CancellationToken));
    }
}
