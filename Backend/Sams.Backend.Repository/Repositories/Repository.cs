﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Sams.Storage;


namespace Sams.Repositories
{
    
    public class Repository<T> : IRepository<T> where T : class
    {
       
        protected SamsStore context;
        internal DbSet<T> dbSet;


        public Repository(SamsStore context)
        {
            
            this.context = context;
            this.dbSet = context.Set<T>();
        }


        public virtual async Task<T> GetByIDAsync(object id, CancellationToken ct = default(CancellationToken))
        {
            
            if (ct != default(CancellationToken)) return await dbSet.FindAsync(ct, id).ConfigureAwait(false); 
            return await dbSet.FindAsync(id).ConfigureAwait(false);
        }


        public virtual async Task<T> FirstAsync(Expression<Func<T, bool>> match, IEnumerable<Expression<Func<T, object>>> includes = null, CancellationToken ct = default(CancellationToken))
        {

            ct.ThrowIfCancellationRequested();
            IQueryable<T> query = dbSet;

            if (includes != null) includes.ForEach(p => query = query.Include(p));
            if (match == null) return await query.FirstOrDefaultAsync(ct).ConfigureAwait(false);
            return await query.FirstOrDefaultAsync(match, ct).ConfigureAwait(false);
        }
         

        public virtual async Task<T> SingleAsync(Expression<Func<T, bool>> match, IEnumerable<Expression<Func<T, object>>> includes = null, CancellationToken ct = default(CancellationToken))
        {

            ct.ThrowIfCancellationRequested();
            IQueryable<T> query = dbSet;

            if (includes != null) includes.ForEach(p => query = query.Include(p));
            if (match == null) return await query.SingleOrDefaultAsync(ct).ConfigureAwait(false);
            return await query.SingleOrDefaultAsync(match, ct).ConfigureAwait(false);
        }
         

        public virtual IQueryable<T> Get()
        {
            return dbSet;
        }
         

        public virtual void Insert(T model)
        {
            dbSet.Add(model);
        }
         

        public virtual void Insert(IEnumerable<T> modellist)
        {
            dbSet.AddRange(modellist);
        }


        public virtual void Delete(int id)
        {
            
            T model = dbSet.Find(id);
            dbSet.Remove(model);
        }


        public virtual void Delete(T model)
        {
            dbSet.Remove(model);
        }


        public virtual void Delete(IEnumerable<T> modellist)
        {
            dbSet.RemoveRange(modellist);
        }


        public virtual void Update(T model)
        {
           
            dbSet.Attach(model);
            context.Entry(model).State = EntityState.Modified;
        }


        /// <remarks>
        /// http://blog.longle.net/2013/05/11/genericizing-the-unit-of-work-pattern-repository-pattern-with-entity-framework-in-mvc/
        /// </remarks>
        public async Task<List<T>>  FindAllAsync(IEnumerable<Expression<Func<T, bool>>> where = null,
                                                 Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
                                                 IEnumerable<Expression<Func<T, object>>> includes = null,
                                                 int? start = null, int? pageSize = null, 
                                                 CancellationToken ct = default(CancellationToken))
        {

            ct.ThrowIfCancellationRequested();
            IQueryable<T> query = dbSet;

            if (includes != null) includes.ForEach(p => query = query.Include(p));
            if (orderBy != null) query = orderBy(query);
            if (where != null) where.ForEach(p => query = query.Where(p));
            if (start.HasValue && pageSize.HasValue) query = query.Skip(start.Value).Take(pageSize.Value);

            return await query.ToListAsync(ct).ConfigureAwait(false);
        }
    }
}
