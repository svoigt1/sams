﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Sams.Storage;


namespace Sams.Repositories
{

    public class Uow : IUow
    {

        private readonly SamsStore store;
        private readonly IDictionary<Type, object> writers;
        private readonly IDictionary<Type, object> readers;


        public Uow(SamsStore store)
        {
            this.store = store;
            writers = new Dictionary<Type, object>();
            readers = new Dictionary<Type, object>();
        }


        public IReadOnlyRepository<TModel> Reader<TModel>() where TModel: class
        {

            object repository;
            if (readers.TryGetValue(typeof(TModel), out repository)) return (IReadOnlyRepository<TModel>)repository;
            
            var newrep = new ReadOnlyRepository<TModel>(store);
            readers[typeof(TModel)] = newrep;
            return newrep;
        }


        public IQueryable<TModel> ReaderQuery<TModel>() where TModel: class
        {

            var rep = Reader<TModel>();
            return rep.Get();
        }


        public IRepository<TModel> Writer<TModel>() where TModel: class
        {

            object repository;
            if (writers.TryGetValue(typeof(TModel), out repository)) return (IRepository<TModel>)repository;
            
            var newrep = new Repository<TModel>(store);
            writers[typeof(TModel)] = newrep;
            return newrep;
        }


        public IQueryable<TModel> WriterQuery<TModel>() where TModel: class
        {

            var rep = Writer<TModel>();
            return rep.Get();
        }


        public async Task SaveAsync(CancellationToken ct = default(CancellationToken))
        {
            await store.SaveChangesAsync(ct).ConfigureAwait(false);
        }
    }
}
