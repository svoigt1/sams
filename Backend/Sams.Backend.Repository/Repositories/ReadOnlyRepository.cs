﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Sams.Storage;


namespace Sams.Repositories
{
    
    public class ReadOnlyRepository<T> : IReadOnlyRepository<T> where T : class 
    {
       
        protected SamsStore context;
        internal DbSet<T> dbSet;


        public ReadOnlyRepository(SamsStore context)
        {
            
            this.context = context;
            this.dbSet = context.Set<T>();
        }


        public virtual async Task<T> GetByIDAsync(object id, CancellationToken ct = default(CancellationToken))
        {
            
            if (ct != default(CancellationToken)) return await dbSet.FindAsync(ct, id).ConfigureAwait(false); 
            return await dbSet.FindAsync(id).ConfigureAwait(false);
        }


        public virtual async Task<T> FirstAsync(Expression<Func<T, bool>> match, IEnumerable<Expression<Func<T, object>>> includes = null, CancellationToken ct = default(CancellationToken))
        {

            ct.ThrowIfCancellationRequested();
            IQueryable<T> query = dbSet.AsNoTracking();

            if (includes != null) includes.ForEach(p => query = query.Include(p));
            if (match == null) return await query.FirstOrDefaultAsync(ct).ConfigureAwait(false);
            return await query.FirstOrDefaultAsync(match, ct).ConfigureAwait(false);
        }


        public virtual async Task<T> SingleAsync(Expression<Func<T, bool>> match, IEnumerable<Expression<Func<T, object>>> includes = null, CancellationToken ct = default(CancellationToken))
        {

            ct.ThrowIfCancellationRequested();
            IQueryable<T> query = dbSet.AsNoTracking();

            if (includes != null) includes.ForEach(p => query = query.Include(p));
            if (match == null) return await query.SingleOrDefaultAsync(ct).ConfigureAwait(false);
            return await query.SingleOrDefaultAsync(match, ct).ConfigureAwait(false);
        }
         

        public virtual IQueryable<T> Get()
        {
            return dbSet.AsNoTracking();
        }


        /// <remarks>
        /// http://blog.longle.net/2013/05/11/genericizing-the-unit-of-work-pattern-repository-pattern-with-entity-framework-in-mvc/
        /// </remarks>
        public async Task<List<TResult>> FindAllAsync<TResult>(IEnumerable<Expression<Func<T, bool>>> predicate = null,
                                                               Func<IQueryable<T>, IOrderedQueryable<T>> order = null,
                                                               IEnumerable<Expression<Func<T, object>>> includes = null,
                                                               Expression<Func<T, TResult>> selector = null,
                                                               int? start = null, int? pageSize = null,
                                                               CancellationToken ct = default(CancellationToken))
        {
            
            IQueryable<T> query = dbSet.AsNoTracking();

            if (includes != null) includes.ForEach(p => query = query.Include(p));
            if (order != null) query = order(query);
            if (predicate != null) predicate.ForEach(p => query = query.Where(p));
            if (start.HasValue && pageSize.HasValue) query = query.Skip(start.Value).Take(pageSize.Value);
            if (selector == null) {
            
                var list = await query.Cast<TResult>().ToListAsync(ct).ConfigureAwait(false);
                return list;
            }

            return (await query.Select<T, TResult>(selector).ToListAsync(ct).ConfigureAwait(false)).ToList();
        }


        public async Task<int> CountAsync<TResult>(IEnumerable<Expression<Func<T, bool>>> predicate = null,
                                                   CancellationToken ct = default(CancellationToken))
        {
            
            IQueryable<T> query = dbSet.AsNoTracking();

            if (predicate != null) predicate.ForEach(p => query = query.Where(p));
            return await query.CountAsync().ConfigureAwait(false);
        }


        public async Task<decimal?> SumAsync<TResult>(Expression<Func<T, decimal?>> selector, IEnumerable<Expression<Func<T, bool>>> predicate = null,
                                                      CancellationToken ct = default(CancellationToken))
        {
            
            IQueryable<T> query = dbSet.AsNoTracking();

            if (predicate != null) predicate.ForEach(p => query = query.Where(p));
            return await query.SumAsync(selector, ct).ConfigureAwait(false);
        }
    }
}
