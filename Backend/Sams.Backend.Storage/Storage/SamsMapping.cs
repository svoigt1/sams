﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Sams.Models;


namespace Sams.Storage
{

    internal class Einrichtung1Config : EntityTypeConfiguration<Einrichtung1> 
    {

        public Einrichtung1Config()
        {
            
            HasKey(x => x.Id);

            Property(x => x.Id).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.Name).IsRequired().HasMaxLength(100);
        }
    }


    internal class Einrichtung2Config : EntityTypeConfiguration<Einrichtung2> 
    {

        public Einrichtung2Config()
        {
            
            HasKey(x => x.Id);

            Property(x => x.Id).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.Name).IsRequired().HasMaxLength(100);
            Property(x => x.EMailVerteiler).IsRequired().HasMaxLength(100);

            HasRequired(p => p.Parent).WithMany(p => p.Children).HasForeignKey(p => p.Parent_Id);
        }
    }  


    internal class Einrichtung3Config : EntityTypeConfiguration<Einrichtung3> 
    {

        public Einrichtung3Config()
        {
            
            HasKey(x => x.Id);

            Property(x => x.Id).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.Name).IsRequired().HasMaxLength(100);

            HasRequired(p => p.Parent).WithMany(p => p.Children).HasForeignKey(p => p.Parent_Id);
        }
    }  


    internal class Einrichtung4Config : EntityTypeConfiguration<Einrichtung4> 
    {

        public Einrichtung4Config()
        {
            
            HasKey(x => x.Id);

            Property(x => x.Id).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.Name).IsRequired().HasMaxLength(100);

            HasRequired(p => p.Parent).WithMany(p => p.Children).HasForeignKey(p => p.Parent_Id);
        }
    }  


    internal class KommunikationConfig : EntityTypeConfiguration<Kommunikation> 
    {

        public KommunikationConfig()
        {
            
            HasKey(x => x.Id);

            Property(x => x.Id).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Absender).IsRequired().HasMaxLength(100);
            Property(x => x.Empfänger).IsRequired().HasMaxLength(100);
            Property(x => x.Betreff).IsRequired().HasMaxLength(100);
            Property(x => x.Inhalt).IsRequired().HasMaxLength(5000);

            HasRequired(p => p.Meldung).WithMany(p => p.Kommunikationen).HasForeignKey(p => p.Meldung_Id);
        }
    }

 
    internal class MeldungConfig : EntityTypeConfiguration<Meldung> 
    {

        public MeldungConfig()
        {
            
            HasKey(x => x.Id);

            Property(x => x.Id).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Nummer).HasMaxLength(64);
            Property(x => x.Meldername).HasMaxLength(100);
            Property(x => x.MelderEMail).HasMaxLength(100);
            Property(x => x.Betreff).IsRequired().HasMaxLength(100);             
            Property(x => x.Kurzbeschreibung).IsRequired().HasMaxLength(5000);    

            HasRequired(p => p.Einrichtung1).WithMany(p => p.Meldungen).HasForeignKey(p => p.Einrichtung1_Id).WillCascadeOnDelete(false);
            HasRequired(p => p.Einrichtung2).WithMany(p => p.Meldungen).HasForeignKey(p => p.Einrichtung2_Id).WillCascadeOnDelete(false);
            HasOptional(p => p.Einrichtung3).WithMany(p => p.Meldungen).HasForeignKey(p => p.Einrichtung3_Id).WillCascadeOnDelete(false);
            HasOptional(p => p.Einrichtung4).WithMany(p => p.Meldungen).HasForeignKey(p => p.Einrichtung4_Id).WillCascadeOnDelete(false);
            
            HasOptional(p => p.Original).WithMany(p => p.Kopien).HasForeignKey(p => p.Original_Id).WillCascadeOnDelete(false);
            HasOptional(p => p.Meldungstyp).WithMany(p => p.Meldungen).HasForeignKey(p => p.Meldungstyp_Id).WillCascadeOnDelete(false);

            HasOptional(p => p.Melder).WithMany(p => p.MeldungenAlsMelder).HasForeignKey(p => p.Melder_Id).WillCascadeOnDelete(false);
            HasOptional(p => p.Bearbeiter).WithMany(p => p.MeldungenAlsBearbeiter).HasForeignKey(p => p.Bearbeiter_Id).WillCascadeOnDelete(false);
            HasOptional(p => p.Verantwortlicher).WithMany(p => p.MeldungenAlsVerantwortlicher).HasForeignKey(p => p.Verantwortlicher_Id).WillCascadeOnDelete(false);
        }
    }


    internal class MeldungstypConfig : EntityTypeConfiguration<Meldungstyp> 
    {

        public MeldungstypConfig()
        {
            
            HasKey(x => x.Id);

            Property(x => x.Id).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.Name).IsRequired().HasMaxLength(100);
            Property(x => x.Farbe).HasMaxLength(100);
        }
    }  


    internal class PersonConfig : EntityTypeConfiguration<Person> 
    {

        public PersonConfig()
        {
            
            HasKey(x => x.Id);

            Property(x => x.Id).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Sid).HasMaxLength(80);
            Property(x => x.Name).HasMaxLength(200);
            Property(x => x.Organization).HasMaxLength(100);
            Property(x => x.EmailAddress).HasMaxLength(100);            
        }
    }


    internal class PersonfunktionConfig : EntityTypeConfiguration<Personfunktion> 
    {

        public PersonfunktionConfig()
        {
            
            HasKey(x => x.Id);

            Property(x => x.Id).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.EMail).IsRequired().HasMaxLength(100);            
            Property(x => x.Funktion).HasMaxLength(100);          
        }
    }
}
