﻿using System;
using System.Linq;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Threading;
using System.Threading.Tasks;
using Sams.Models;


namespace Sams.Storage
{
   
    public class SamsStore : DbContext
    {

        public DbSet<Meldung>           Meldungen           { get; set; }
        public DbSet<Kommunikation>     Kommunikationen     { get; set; }
        public DbSet<Person>            Personen            { get; set; }
        public DbSet<Personfunktion>    Personfunktionen    { get; set; }
        public DbSet<Meldungstyp>       Meldungstyp         { get; set; }
        public DbSet<Einrichtung1>      Einrichtungen1      { get; set; }
        public DbSet<Einrichtung2>      Einrichtungen2      { get; set; }
        public DbSet<Einrichtung3>      Einrichtungen3      { get; set; }
        public DbSet<Einrichtung4>      Einrichtungen4      { get; set; }


        public SamsStore() : base("SamsConnection")
        {
       
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = true;
            Configuration.ValidateOnSaveEnabled = false;
            Configuration.AutoDetectChangesEnabled = true;
         
            //Database.SetInitializer(new DropCreateDatabaseIfModelChanges<SamsStore>());
            Database.SetInitializer<SamsStore>(null);
        }
 

        public override async Task<int> SaveChangesAsync(CancellationToken ct = default(CancellationToken))
        {

            try {

                ConvertEmptyStrings();
                return await base.SaveChangesAsync(ct).ConfigureAwait(false);
            }
            catch (DbUpdateException x) 
            {
                throw new StoreValidationException(x);
            }
        }

       
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Ignore<Benutzer>();

            modelBuilder.Configurations.Add(new Einrichtung1Config());
            modelBuilder.Configurations.Add(new Einrichtung2Config());
            modelBuilder.Configurations.Add(new Einrichtung3Config());
            modelBuilder.Configurations.Add(new Einrichtung4Config());
            modelBuilder.Configurations.Add(new KommunikationConfig());
            modelBuilder.Configurations.Add(new MeldungConfig());
            modelBuilder.Configurations.Add(new MeldungstypConfig());
            modelBuilder.Configurations.Add(new PersonConfig());
            modelBuilder.Configurations.Add(new PersonfunktionConfig());
        }
    
    
        ///<remarks>
        ///http://stackoverflow.com/questions/1119615/is-there-an-option-to-make-entity-framework-revert-empty-strings-to-null
        ///</remarks>
        private void ConvertEmptyStrings()
        {

            var objectContext = ((IObjectContextAdapter)this).ObjectContext;
            var savingEntries = objectContext.ObjectStateManager.GetObjectStateEntries(EntityState.Added | EntityState.Modified);

            foreach (var entry in savingEntries) {
                
                var curValues = entry.CurrentValues;
                var fieldMetadata = curValues.DataRecordInfo.FieldMetadata;
                var stringFields = fieldMetadata.Where(f => f.FieldType.TypeUsage.EdmType.Name == "String");
                
                foreach (var stringField in stringFields) {

                    var ordinal = stringField.Ordinal;
                    var curValue = curValues[ordinal] as string;
                    if (curValue != null && curValue.All(char.IsWhiteSpace)) curValues.SetValue(ordinal, null);
                }
            }
        }    
    }
}
