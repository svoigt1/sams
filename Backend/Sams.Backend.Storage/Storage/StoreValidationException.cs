﻿using System;
using System.Data.Entity.Infrastructure;
using System.Linq;


namespace Sams.Storage
{
   
    [Serializable]
    public class StoreValidationException : UserException
    {

        public StoreValidationException(Exception source) : base(Explain(source), source)
        {}


        public static String Explain(Exception source)
        {

            var msg = source.UnwrapMessage();

            var duce = source as DbUpdateConcurrencyException;
            if (duce != null) {

                if (msg.Contains("Entities may have been modified or deleted since entities were loaded.")) 
                    return "Die Daten wurden inzwischen von einem anderen Bearbeiter geändert.";
            }

            var dbue = source as DbUpdateException;
            if (dbue != null) {

                if (msg.Contains("würden abgeschnitten") || msg.Contains("value too long")) {

                    var entity = dbue.Entries.FirstOrDefault();
                    var res = entity.GetValidationResult().ValidationErrors.FirstOrDefault();     
                    if (res != null) return String.Format("Der Text ist zu lang um gespeichert werden zu können.{0}({1})", System.Environment.NewLine, res.ErrorMessage);
                    return "Der Text ist zu lang um gespeichert werden zu können.";
                }

                if (msg.Contains("TimeTicks was either too large or too small")) return "Die angegebene Uhrzeit ist nicht gültig.";

                
                if (new String[] { "null", "auauftragg" }.All(msg.Contains)) return "Jeder Auftraggeber muss einen Namen besitzen.";
                if (new String[] { "null", "auauftragl" }.All(msg.Contains)) return "Jeder Auftragnehmer muss einen Namen besitzen.";
                if (new String[] { "null", "rfnr" }.All(msg.Contains)) return "Jede Einheit muss ein Zeichen besitzen.";
                if (new String[] { "null", "rfname" }.All(msg.Contains)) return "Jede Einheit muss eine Bezeichnung besitzen.";
                if (new String[] { "null", "periodename" }.All(msg.Contains)) return "Betriebsferien müssen eine Bezeichnung besitzen.";
                if (new String[] { "null", "fttext" }.All(msg.Contains)) return "Feiertage müssen eine Bezeichnung besitzen.";
                if (new String[] { "null", "ftdatum" }.All(msg.Contains)) return "Feiertage müssen ein Datum besitzen.";
                if (new String[] { "null", "kwvon" }.All(msg.Contains)) return "Betriebsferien müssen ein Startdatum und ein Endedatum aufweisen.";
                if (new String[] { "null", "kwbis" }.All(msg.Contains)) return "Betriebsferien müssen ein Startdatum und ein Endedatum aufweisen.";
                if (new String[] { "null", "lanr" }.All(msg.Contains)) return "Jede Leistungsart muss eine Nummer besitzen.";
                if (new String[] { "null", "laname" }.All(msg.Contains)) return "Jede Leistungsart muss eine Bezeichnung besitzen.";
                if (new String[] { "null", "genummer" }.All(msg.Contains)) return "Jedes Gewerk muss eine Nummer besitzen.";
                if (new String[] { "null", "gename" }.All(msg.Contains)) return "Jedes Gewerk muss eine Bezeichnung besitzen.";
                if (new String[] { "null", "auauftragg" }.All(msg.Contains)) return "Jeder Auftraggeber muss einen Namen besitzen.";
                if (new String[] { "null", "auauftragl" }.All(msg.Contains)) return "Jeder Auftragnehmer muss einen Namen besitzen.";
                if (new String[] { "null", "lfname1" }.All(msg.Contains)) return "Jeder Lieferant muss einen Namen 1 besitzen.";
                if (new String[] { "null", "aufschlagnamekurz" }.All(msg.Contains)) return "Jeder Aufschlag muss ein Kürzel besitzen.";
                if (new String[] { "null", "aufschlagnamelang" }.All(msg.Contains)) return "Jeder Aufschlag muss eine Bezeichnung besitzen.";
                if (new String[] { "null", "suname" }.All(msg.Contains)) return "Jede Schadensursache muss eine Bezeichnung besitzen.";
                if (new String[] { "null", "ssuname" }.All(msg.Contains)) return "Jede Subschadensursache muss eine Bezeichnung besitzen.";
                if (new String[] { "null", "sbname" }.All(msg.Contains)) return "Jedes Schadensbild muss eine Bezeichnung besitzen.";
                if (new String[] { "null", "ssbname" }.All(msg.Contains)) return "Jedes Subschadensbild muss eine Bezeichnung besitzen.";
                if (new String[] { "null", "objgr_name" }.All(msg.Contains)) return "Jede Objektgruppe muss eine Bezeichnung besitzen.";
                if (new String[] { "null", "objart_name" }.All(msg.Contains)) return "Jede Objektart muss eine Bezeichnung besitzen.";
                if (new String[] { "null", "pzname" }.All(msg.Contains)) return "Jeder Standort muss eine Bezeichnung besitzen.";
                if (new String[] { "null", "pznr" }.All(msg.Contains)) return "Jeder Standort muss eine Nummer besitzen.";
                if (new String[] { "null", "pzkey" }.All(msg.Contains)) return "Jeder Standort muss einen Schlüssel besitzen.";
                if (new String[] { "null", "kstname" }.All(msg.Contains)) return "Jede Kostenstelle muss eine Bezeichnung besitzen.";
                if (new String[] { "null", "kstnr" }.All(msg.Contains)) return "Jede Kostenstelle muss eine Nummer besitzen.";
                if (new String[] { "null", "mtname" }.All(msg.Contains)) return "Jedes Material muss eine Bezeichnung besitzen.";
                if (new String[] { "null", "mtnr" }.All(msg.Contains)) return "Jedes Material muss eine Nummer besitzen.";
                if (new String[] { "null", "aukurzb" }.All(msg.Contains)) return "Der Auftrag muss eine Kurzbeschreibung besitzen.";


#region F O R E I G N   K E Y S


                if (new String[] { "$1","tbl_auftrag","tblsusub"                              }.All(msg.Contains)) return "Die Subschadensursache darf nicht gelöscht werden da sie Aufträgen zugeordnet ist.";
                if (new String[] { "$1","tblmtstamm","tbl_einheit"                            }.All(msg.Contains)) return "Die Einheit darf nicht gelöscht werden da sie Material zugeordnet ist.";
                if (new String[] { "$1","tblmt","tblwarenausgang"                             }.All(msg.Contains)) return "Der Warenausgang darf nicht gelöscht werden da er Material zugeordnet ist.";
                if (new String[] { "$1","tblbestellpos","tblmtstamm"                          }.All(msg.Contains)) return "Das Material darf nicht gelöscht werden da es Bestellpositionen zugeordnet ist.";
                if (new String[] { "$1","tblbestellung","tblps"                               }.All(msg.Contains)) return "Das Personal darf nicht gelöscht werden da es Bestellungen zugeordnet ist.";
                if (new String[] { "$1","tblps","tblgewerk"                                   }.All(msg.Contains)) return "Das Gewerk darf nicht gelöscht werden da es Personal zugeordnet ist.";
                if (new String[] { "$1","tblwarenausgang","tbl_ih_kst"                        }.All(msg.Contains)) return "Das IH-Objekt darf nicht gelöscht werden da es Warenausgängen zugeordnet ist.";
                if (new String[] { "$1","tblreservierung","tblmtstamm"                        }.All(msg.Contains)) return "Das Material darf nicht gelöscht werden da es Reservierungen zugeordnet ist.";
                if (new String[] { "$1","tblwareneingang","tblmtstamm"                        }.All(msg.Contains)) return "Das Material darf nicht gelöscht werden da es Wareneingängen zugeordnet ist.";
                if (new String[] { "$1","tblwareneinlagerung","tblwareneingang"               }.All(msg.Contains)) return "Der Wareneingang darf nicht gelöscht werden da er Wareneinlagerungen zugeordnet ist.";
                if (new String[] { "$1","tblih","tbllfs"                                      }.All(msg.Contains)) return "Der Lieferant darf nicht gelöscht werden da er IH-Objekten zugeordnet ist.";
                if (new String[] { "$10","tbl_auftrag","tblsb"                                }.All(msg.Contains)) return "Das Schadensbild darf nicht gelöscht werden da es Aufträgen zugeordnet ist.";
                if (new String[] { "$11","tbl_auftrag","tbl_auftgb"                           }.All(msg.Contains)) return "Der Auftraggeber  darf nicht gelöscht werden da er Aufträgen zugeordnet ist.";
                if (new String[] { "$13","tbl_auftrag","tbl_auftgl"                           }.All(msg.Contains)) return "Der Auftragnehmer darf nicht gelöscht werden da er Aufträgen zugeordnet ist.";
                if (new String[] { "$2","tbl_auftrag","tblsbsub"                              }.All(msg.Contains)) return "Das Subschadensbild darf nicht gelöscht werden da es Aufträgen zugeordnet ist.";
                if (new String[] { "$2","tblwarenausgang","tblmtstamm"                        }.All(msg.Contains)) return "Das Material darf nicht gelöscht werden da es Warenausgängen zugeordnet ist.";
                if (new String[] { "$2","tblwareneinlagerung","tbllagerort"                   }.All(msg.Contains)) return "Der Lagerort darf nicht gelöscht werden da er Wareneinlagerungen zugeordnet ist.";
                if (new String[] { "$2","tblmtstamm","tblwarengruppe"                         }.All(msg.Contains)) return "Die Warengruppe darf nicht gelöscht werden da sie Material zugeordnet ist.";
                if (new String[] { "$2","tblbestellung","tbllfap"                             }.All(msg.Contains)) return "Der Ansprechpartner darf nicht gelöscht werden da er Bestellungen zugeordnet ist.";
               
                if (new String[] { "$2","tblaufschlag","tblaufschlagtyp"                      }.All(msg.Contains)) return "Der Aufschlagtyp darf nicht gelöscht werden da er Aufschlägen zugeordnet ist.";
                if (new String[] { "$2","tblrechnung","tblprojekt"                            }.All(msg.Contains)) return "Das Projekt darf nicht gelöscht werden da es Rechnungen zugeordnet ist.";
                if (new String[] { "$2","tbl_ih_pz","tblpz"                                   }.All(msg.Contains)) return "Der Standort darf nicht gelöscht werden da er IH-Objekten zugeordnet ist.";
                if (new String[] { "$2","tbl_ih_kst","tblkst"                                 }.All(msg.Contains)) return "Die Kostenstelle darf nicht gelöscht werden da sie IH-Objekten zugeordnet ist.";
                if (new String[] { "$2","tblle","tblps"                                       }.All(msg.Contains)) return "Das Personal darf nicht gelöscht werden da es Leistungen zugeordnet ist.";
                if (new String[] { "$2","tblih","tbllfs"                                      }.All(msg.Contains)) return "Der Lieferant darf nicht gelöscht werden da er IH-Objekten zugeordnet ist.";
                if (new String[] { "$3","tblwareneinlagerung","tblmtstamm"                    }.All(msg.Contains)) return "Das Material darf nicht gelöscht werden da es Wareneinlagerungen zugeordnet ist.";
                if (new String[] { "$3","tbl_auftrag","tblgewerk"                             }.All(msg.Contains)) return "Das Gewerk darf nicht gelöscht werden da es Aufträgen zugeordnet ist.";
                if (new String[] { "$3","tblih","tblpz"                                       }.All(msg.Contains)) return "Der Standort darf nicht gelöscht werden da er IH-Objekten  zugeordnet ist.";
                if (new String[] { "$3","tblreservierung","tblps"                             }.All(msg.Contains)) return "Das Personal darf nicht gelöscht werden da es Reservierungen zugeordnet ist.";
                if (new String[] { "$3","tblangebot","tbl_ih_kst"                             }.All(msg.Contains)) return "Das IH-Objekt darf nicht gelöscht werden da es Angeboten zugeordnet ist.";
                if (new String[] { "$3","tblbestellung","tbllfs"                              }.All(msg.Contains)) return "Der Lieferant darf nicht gelöscht werden da er Bestellungen zugeordnet ist.";
                if (new String[] { "$4","tblangebot","tblprojekt"                             }.All(msg.Contains)) return "Das Projekt darf nicht gelöscht werden da es Angeboten zugeordnet ist.";
                if (new String[] { "$4","tblih","tblkst"                                      }.All(msg.Contains)) return "Die Kostenstelle darf nicht gelöscht werden da sie IH-Objekten zugeordnet ist.";
                if (new String[] { "$4","tblanfrage","tbl_ih_kst"                             }.All(msg.Contains)) return "Das IH-Objekt darf nicht gelöscht werden da es Anfragen zugeordnet ist.";
                if (new String[] { "$4","tblmt","tbllfs"                                      }.All(msg.Contains)) return "Der Lieferant darf nicht gelöscht werden da er Material zugeordnet ist.";
                if (new String[] { "$5","tblwarenausgang","tblkst"                            }.All(msg.Contains)) return "Die Kostenstelle darf nicht gelöscht werden da sie Warenausgängen zugeordnet ist.";
                if (new String[] { "$5","tbl_auftrag","tblprojekt"                            }.All(msg.Contains)) return "Das Projekt darf nicht gelöscht werden da es Aufträgen zugeordnet ist.";
                if (new String[] { "$5","tblanfrage","tblprojekt"                             }.All(msg.Contains)) return "Das Projekt darf nicht gelöscht werden da es Anfragen zugeordnet ist.";
                
                if (new String[] { "$5","tblmt","tbl_einheit"                                 }.All(msg.Contains)) return "Die Einheit darf nicht gelöscht werden da sie Material zugeordnet ist.";
                if (new String[] { "$5","tblih","tbllfs"                                      }.All(msg.Contains)) return "Der Lieferant darf nicht gelöscht werden da er IH-Objekten zugeordnet ist.";
                if (new String[] { "$6","tblwarenausgang","tbl_auftrag"                       }.All(msg.Contains)) return "Der Auftrag darf nicht gelöscht werden da er Warenausgängen zugeordnet ist.";
                if (new String[] { "$6","tbl_auftrag","tbl_ih_kst"                            }.All(msg.Contains)) return "Die Kostenstelle darf nicht gelöscht werden da sie Aufträgen zugeordnet ist.";
                if (new String[] { "$6","tblmt","tblmtstamm"                                  }.All(msg.Contains)) return "Der Materialstamm darf nicht gelöscht werden da er Materialleistungen zugeordnet ist.";
                if (new String[] { "$8","tbl_auftrag","tblla"                                 }.All(msg.Contains)) return "Der Leistungsart darf nicht gelöscht werden da sie Aufträgen zugeordnet ist.";
                if (new String[] { "$8","tblih","tblobjektart"                                }.All(msg.Contains)) return "Die Objektart darf nicht gelöscht werden da sie IH-Objekten zugeordnet ist.";
                if (new String[] { "$9","tblih","tblobjektgruppe"                             }.All(msg.Contains)) return "Die Objektgruppe darf nicht gelöscht werden da sie IH-Objekten zugeordnet ist.";
                if (new String[] { "$9","tbl_auftrag","tblsu"                                 }.All(msg.Contains)) return "Die Schadensursache darf nicht gelöscht werden da er Aufträgen zugeordnet ist.";
                if (new String[] { "fgn_ih_aupar","tblih","tbl_auftrag"                       }.All(msg.Contains)) return "Der Auftrag darf nicht gelöscht werden da er IH-Objekten zugeordnet ist.";
               // if (new String[] { "fgn_ihparam_param","tblparamih","tblparam"                }.All(msg.Contains)) return "Der Auftragnehmer darf nicht gelöscht werden da er Auftrag zugeordnet ist.";
               // if (new String[] { "fgn_paramih_grp","tblparamih","tblpargrp"                 }.All(msg.Contains)) return "Der Auftragnehmer darf nicht gelöscht werden da er Auftrag zugeordnet ist.";
               // 
               // if (new String[] { "fgn_paramtereinheit","tblparam","tbl_einheit"             }.All(msg.Contains)) return "Der Auftragnehmer darf nicht gelöscht werden da er Auftrag zugeordnet ist.";
               // if (new String[] { "fgn_pargrpih_grp","tblpargrpih","tblpargrp"               }.All(msg.Contains)) return "Der Auftragnehmer darf nicht gelöscht werden da er Auftrag zugeordnet ist.";
               // if (new String[] { "fgn_parihval_au","tblparihval","tbl_auftrag"              }.All(msg.Contains)) return "Der Auftragnehmer darf nicht gelöscht werden da er Auftrag zugeordnet ist.";
               // if (new String[] { "fgn_parihval_ihpar","tblparihval","tblparamih"            }.All(msg.Contains)) return "Der Auftragnehmer darf nicht gelöscht werden da er Auftrag zugeordnet ist.";
               // if (new String[] { "fgn_parprofil_au","tblparprofil","tbl_auftrag"            }.All(msg.Contains)) return "Der Auftragnehmer darf nicht gelöscht werden da er Auftrag zugeordnet ist.";
               // if (new String[] { "fgn_parsession_ps","tblparsession","tblps"                }.All(msg.Contains)) return "Der Auftragnehmer darf nicht gelöscht werden da er Auftrag zugeordnet ist.";
                if (new String[] { "fgn_terminhist_real","tblauterminhistory","tblrealzyklus" }.All(msg.Contains)) return "Der Auftragnehmer darf nicht gelöscht werden da er in der Terminhistorie verwendet wird.";
               // if (new String[] { "tblparamih_keyeinheit_fkey","tblparamih","tbl_einheit"    }.All(msg.Contains)) return "Der Auftragnehmer darf nicht gelöscht werden da er Auftrag zugeordnet ist.";

#endregion


                if (msg.Contains("cts_auzyklus_bez")) return "Betriebsferien müssen ein Startdatum und ein Endedatum aufweisen und das Startdatum muss vor dem Ende liegen.";
                if (msg.Contains("cts_la_isplan")) return "Eine Leistungsart kann nicht gleichzeitig planmäßig und unplanmäßig sein.";
                if (msg.Contains("cts_la_bez")) return "Jede Leistungsart muss eine Bezeichnung besitzen.";
                if (msg.Contains("idx_la_nr")) return "Jede Leistungsart muss eine eindeutige Nummer besitzen.";
                if (msg.Contains("ctr_notempty")) return "Jedes Gewerk muss eine Nummer und eine Bezeichnung besitzen.";
                if (msg.Contains("idx_gewerk_nummer")) return "Jedes Gewerk muss eine eindeutige Nummer besitzen.";
                if (msg.Contains("cts_einheit_bez")) return "Jede Einheit muss ein Symbol und eine Bezeichnung besitzen.";
                if (msg.Contains("idx_einheit_nr")) return "Jede Einheit muss ein eindeutiges Symbol besitzen.";
                if (msg.Contains("idx_tbl_auftgl_name")) return "Jeder Auftragnehmer muss einen eindeutigen Namen besitzen.";
                if (msg.Contains("idx_auauftragg")) return "Jeder Auftraggeber muss einen eindeutigen Namen besitzen.";
                if (msg.Contains("cts_auftgl")) return "Jeder Auftragnehmer muss einen Namen besitzen.";
                if (msg.Contains("cts_auauftragg")) return "Jeder Auftraggeber muss einen Namen besitzen.";
                if (msg.Contains("idx_su_nr")) return "Jede Schadensursache muss eine eindeutige Nummer besitzen.";
                if (msg.Contains("idx_ssunr")) return "Jede Subschadensursache muss eine eindeutige Nummer besitzen.";
                if (msg.Contains("idx_sb_nr")) return "Jedes Schadensbild muss eine eindeutige Nummer besitzen.";
                if (msg.Contains("idx_ssbnr")) return "Jedes Subschadensbild muss eine eindeutige Nummer besitzen.";
                if (msg.Contains("idx_objgr_key")) return "Jede Objektgruppe muss eine eindeutige Nummer besitzen.";
                if (msg.Contains("idx_objart_key")) return "Jede Objektart muss eine eindeutige Nummer besitzen.";
                if (msg.Contains("tblsu_sunr_cst")) return "Die Nummer der Schadensursache muss zwischen 0 und 9 liegen";
                if (msg.Contains("tblsusub_ssunr_cst")) return "Die Nummer der Subschadensursache muss zwischen 1 und 9 liegen";
                if (msg.Contains("tblsb_sbnr_cst")) return "Die Nummer des Schadensbildes muss zwischen 0 und 9 liegen";
                if (msg.Contains("tblsbsub_ssbnr_cst")) return "Die Nummer des Subschadensbildes muss zwischen 1 und 9 liegen";
                if (msg.Contains("cts_objgr_key")) return "Die Nummer der Objektgruppe muss zwischen 1 und 9 liegen";
                if (msg.Contains("cts_objart_nr")) return "Die Nummer der Objektart muss zwischen 1 und 9 liegen";
                if (msg.Contains("idx_kst_nr")) return "Jede Kostenstelle muss eine eindeutige Nummer besitzen.";
                if (msg.Contains("idx_pz_pzkey")) return "Jeder Standort muss einen eindeutigen Schlüssel besitzen.";
                if (msg.Contains("idx_pz_pznr")) return "Jeder Standort muss eine eindeutige Nummer besitzen.";
            }

            return source.Message;
        }
    }
}
