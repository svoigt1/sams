﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Sams.Repositories;


namespace Sams.Installers
{
   
    public class RepositoriesInstaller : IWindsorInstaller
    {
	   
        public void Install(IWindsorContainer container, IConfigurationStore store)
	    {
		   
            container.Register(Component.For(typeof(IUow)).ImplementedBy(typeof(Uow)).LifestylePerWebRequest());       
	    }
    }
}