﻿using Castle.Facilities.Logging;
using Castle.Facilities.TypedFactory;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Sams.Factories;
   

namespace Sams.Installers
{

    public class FacilityInstaller : IWindsorInstaller
    {
	    
        public void Install(IWindsorContainer container, IConfigurationStore store)
	    {
          
            //ermöglicht Transient support
            container.AddFacility<TypedFactoryFacility>();  
            //gibt Create/Release-Factories für die Erzeugung transienter Objekte
            container.Register(Component.For(typeof(IFactory<>)).AsFactory());
            //packt die Produkte dieser Factories in ein IDisposable-Wrapper damit die Objekte sich automatisch abmelden können
            container.Register(Component.For(typeof(IContainerFactory<>)).ImplementedBy(typeof(ContainerFactory<>)));       
		    

            container.AddFacility<LoggingFacility>(f => f.UseLog4Net());
            container.Register(Component.For<Sams.ILogger>().ImplementedBy<Sams.Logger>()); 
	    }
    }
}