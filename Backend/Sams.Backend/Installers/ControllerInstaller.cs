﻿using System.Web.Http;
using System.Web.Http.Controllers;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;


namespace Sams.Installers
{
    
    public class ControllerInstaller : IWindsorInstaller
    {
	   
        public void Install(IWindsorContainer container, IConfigurationStore store)
	    {
		   
            container.Register(Component.For(typeof(ISettings)).ImplementedBy(typeof(SettingsImpl)));       
            
            container.Register(Classes.FromThisAssembly()
		                   	           .BasedOn<ApiController>()
		                   	           .LifestylePerWebRequest());
	    }
    }
}