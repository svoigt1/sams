﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Sams.Interceptors;


namespace Sams.Installers
{

    public class InterceptorInstaller : IWindsorInstaller
    {
        
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<StoreLogInterceptor>()); 
        }
    }
}
