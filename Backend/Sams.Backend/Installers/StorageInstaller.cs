﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Sams.Storage;


namespace Sams.Installers
{
   
    public class StorageInstaller : IWindsorInstaller
    {
	   
        public void Install(IWindsorContainer container, IConfigurationStore store)
	    {
            
            container.Register(Component.For<SamsStore>().LifeStyle.PerWebRequest);
	    }
    }
}