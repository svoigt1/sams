﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;


namespace Sams.Installers
{
   
    public class ServicesInstaller : IWindsorInstaller
    {
	   
        public void Install(IWindsorContainer container, IConfigurationStore store)
	    {
            
            container.Register(Classes.FromAssemblyNamed("Sams.Backend.Services")
		                   	          .Where(p => p.Name.EndsWith("Service"))
                                      .WithServiceDefaultInterfaces()
                                      .LifestylePerWebRequest());
            
            container.Register(Classes.FromAssemblyNamed("Sams.Backend.Services")
		                   	          .Where(p => p.Name.EndsWith("Fabrik"))
                                      .WithServiceDefaultInterfaces()
                                      .LifestylePerWebRequest());
            
            container.Register(Classes.FromAssemblyNamed("Sams.Backend.Infrastructure")
		                   	          .Where(p => p.Name.EndsWith("Service"))
                                      .WithServiceDefaultInterfaces()
                                      .LifestylePerWebRequest());
	    }
    }
}