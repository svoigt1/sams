﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Sams
{

    public class Logger : Sams.ILogger
    {

        public Castle.Core.Logging.ILogger LoggerCore { get; set; }

       
        public void Debug(string message)
        {
            LoggerCore.Debug(message);
        }

        public void DebugFormat(string format,params object[] args)
        {
            LoggerCore.DebugFormat(format, args);
        }

        public void Error(string message)
        {
            LoggerCore.Error(message);
        }

        public void Error(Exception ex)
        {
            LoggerCore.Error(ex.Message, ex);
        }

        public void ErrorFormat(string format,params object[] args)
        {
            LoggerCore.ErrorFormat(format, args);
        }

        public void Info(string message)
        {
            LoggerCore.Info(message);
        }

        public void InfoFormat(string format,params object[] args)
        {
            LoggerCore.InfoFormat(format, args);
        }

        public void Warn(string message)
        {
            LoggerCore.Warn(message);
        }

        public void WarnFormat(string format,params object[] args)
        {
            LoggerCore.WarnFormat(format, args);
        }

        public void Error(Exception ex,string message)
        {
            LoggerCore.Error(message, ex);
        }

        public void ErrorFormat(Exception ex,string format,params object[] args)
        {
            LoggerCore.ErrorFormat(ex, format, args);
        }
    }
}