﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Configuration;


namespace Sams
{

    public class SettingsImpl 
    {

        public String Domain { get { return WebConfigurationManager.AppSettings["Domain"]; } }
    }
}