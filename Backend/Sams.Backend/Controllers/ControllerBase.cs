﻿using System;
using System.DirectoryServices.AccountManagement;
using System.Security.Claims;
using System.Threading;
using System.Web.Http;
using Sams.Models;


namespace Sams.Controllers
{
    
    public abstract class ControllerBase : ApiController
    {

       // protected readonly ISettings settings;

       /*
        public ControllerBase(ISettings settings)
        {
            this.settings = settings;
        } */


        private Benutzer currentUser;
        protected Benutzer CurrentUser 
        { 
        
            get {  
        
                if (currentUser != null) return currentUser;
        
                var identity = (ClaimsIdentity)(User.Identity);
                currentUser = new Benutzer { Organization = "CUBEOFFICE"};

                //Domäne muss angegeben werden damit bei einem Deployment des Backends der Benutzer weiterarbeiten kann,
                //also er wieder am AD authentifiziert wird
                using (var pc = new PrincipalContext(ContextType.Domain, ""))
                using (var adUser = UserPrincipal.FindByIdentity(pc, identity.Name)) {

                    CurrentUser.HasRoleMA      = adUser.IsMemberOf(pc, IdentityType.Name, "g_sams_ma");
                    CurrentUser.HasRoleQS      = adUser.IsMemberOf(pc, IdentityType.Name, "g_sams_qm");
                    CurrentUser.HasRoleGF      = adUser.IsMemberOf(pc, IdentityType.Name, "g_sams_elgf");
                    CurrentUser.HasRoleADM     = adUser.IsMemberOf(pc, IdentityType.Name, "g_sams_adm");
                    CurrentUser.HasRoleRM      = adUser.IsMemberOf(pc, IdentityType.Name, "g_sams_rm");
                    CurrentUser.Sid            = adUser.Sid.Value;
                    CurrentUser.Loginname      = identity.Name;
                    CurrentUser.Name           = adUser.DisplayName;
                    CurrentUser.EmailAddress   = adUser.EmailAddress ?? String.Empty;
                }

                return currentUser;
            }
        }


        protected CancellationToken CancellationToken
        {

            get {

                var version = System.Web.HttpRuntime.IISVersion;
                if (version == null) return default(CancellationToken);
                var minVersion = new Version(7, 5);
                if (version < minVersion) return default(CancellationToken);
                if (!System.Web.HttpRuntime.UsingIntegratedPipeline) return default(CancellationToken);
                return System.Web.HttpContext.Current.Response.ClientDisconnectedToken;
            }
        }
    }
}
