﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Sams.Dtos;
using Sams.Services;


namespace Sams.Controllers
{

    public class EinrichtungController : ControllerBase
    {

        private readonly ILogger logger;
        private readonly IEinrichtungsService store;


        public EinrichtungController(ILogger logger, IEinrichtungsService store) 
        {

            this.logger = logger;
            this.store = store;
        }


        /// <summary>
        /// Gibt Einrichtungsverzeichnis mit bis zu vier Ebenen
        /// </summary>
        /// <remarks>
        /// Die Ebenen unterscheiden sich an den verschiedenen Standorten. Die Stationsbezeichnung kann beispielsweise Ebene 3 oder 4 sein.
        /// </remarks>
        /// <response code="500">Fehler in der Datenbank</response>
        /// <response code="200"></response>
        /// <returns>List EinrichtungDto: Liste mit Einrichtungen Ebene 1 und anderen verschachtelten Ebenen</returns>
        [Route("api/einrichtungen")]
        [ResponseType(typeof(EinrichtungDto))]
        public async Task<IHttpActionResult> Get()
        {

            try {
            
                var list = await store.GetVerzeichnisAsync(CancellationToken);
                return Ok(list);
            }
            catch (UserException x) {

                logger.Error(x);
                return BadRequest(x.Explain());
            }
            catch (Exception x) {

                logger.Error(x);
                throw;   
            }
        }
    }
}
