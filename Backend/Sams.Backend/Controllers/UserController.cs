﻿using System;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using Sams.Dtos;


namespace Sams.Controllers
{
    
    public class UserController : ControllerBase
    {

        private readonly ILogger logger;


        public UserController(ILogger logger)
        {
            this.logger = logger;
        }
    

        /// <summary>
        /// Gibt aktuellen Benutzer
        /// </summary>
        /// <remarks>
        /// Gibt den durch Windows authentifizierten AD-Benutzer mit zugeordneten Rollen
        /// </remarks>
        /// <response code="403">Benutzer ist nicht an Windows angemeldet</response>
        /// <response code="500">Interner Fehler im Backend</response>
        /// <response code="200"></response>
        /// <returns>UserDto: AD-Benutzer mit zugeordneten Rollen</returns>
        [Route("api/user")]
        [ResponseType(typeof(BenutzerDto))]
        public IHttpActionResult Get()
        {

            try {
            
                var user = Mapper.Map<BenutzerDto>(CurrentUser);
                return Ok(user);
            }
            catch (UnauthorizedAccessException x) {

                logger.Error(x);
                return ResponseMessage(new HttpResponseMessage(System.Net.HttpStatusCode.Forbidden));
            }
            catch (UserException x) {

                logger.Error(x);
                return BadRequest(x.Explain());
            }
            catch (Exception x) {

                logger.Error(x);
                throw;   
            }
        }
    }
}
