﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using Sams.Services;


namespace Sams.Controllers
{

    public class AdminController : ControllerBase
    {

        private readonly ILogger logger;
        private readonly IAdminService store;


        public AdminController(ILogger logger, IAdminService store) 
        {

            this.logger = logger;
            this.store = store;
        }


        [Route("api/db")]
        public async Task<IHttpActionResult> Get()
        {

            try {
            
                var cnt = await store.CountAsync(CancellationToken);
                return Ok(cnt);
            }
            catch (UserException x) {

                logger.Error(x);
                return BadRequest(x.Explain());
            }
            catch (Exception x) {

                logger.Error(x);
                throw;   
            }
        }
    }
}
