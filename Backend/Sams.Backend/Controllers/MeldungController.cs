﻿using System;
using System.Net.Http;
using System.Security;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Sams.Dtos;
using Sams.Services;


namespace Sams.Controllers
{
    
    public class MeldungController : ControllerBase
    {

        private readonly ILogger logger;
        private readonly IMeldungService store;
        private readonly IMeldungEditService editStore;


        public MeldungController(ILogger logger, IMeldungService store, IMeldungEditService editStore) 
        {

            this.logger = logger;
            this.store = store;
            this.editStore = editStore;
        }


        /// <summary>
        /// Gibt alle Meldungstypen
        /// </summary>
        /// <remarks>
        /// Meldungstypen können auch per ID hartkodiert werden, hier zur Diagnose verwendet (stimmen die Typen im Backend mit dem Frontend noch überein)
        /// </remarks>
        /// <response code="500">Fehler in der Datenbank</response>
        /// <response code="200"></response>
        [Route("api/meldungstypen")]
        [ResponseType(typeof(MeldungstypDto))]
        public async Task<IHttpActionResult> GetMeldungstypen()
        {

            try {

                var dtolist = await store.MeldungstypenAsync(CancellationToken);
                return Ok(dtolist);
            }
            catch (UnauthorizedAccessException x) {

                logger.Error(x);
                return ResponseMessage(new HttpResponseMessage(System.Net.HttpStatusCode.Forbidden));
            }
            catch (UserException x) {

                logger.Error(x);
                return BadRequest(x.Explain());
            }
            catch (Exception x) {

                logger.Error(x);
                throw;   
            }
        }


        /// <summary>
        /// Gibt eine Meldung per DB-Id
        /// </summary>
        /// <remarks>
        /// Benutzer muß Eigentümer der Meldung sein oder Rolle SAMS_QM, SAMS_ELGF, SAMS_ADM oder SAMS_RB haben oder Meldung muß öffentlich sein
        /// </remarks>
        /// <response code="200"></response>
        /// <response code="403">Benutzer hat kein Zugriffsrecht auf diese Meldung</response>
        /// <response code="404">Es gibt keine Meldung mit dieser ID</response>
        /// <response code="500">Fehler in der Datenbank</response>
        [Route("api/meldung/{id}")]
        [ResponseType(typeof(MeldungDto))]
        public async Task<IHttpActionResult> GetMeldung(int id)
        {

            try {

                var dto = await store.MeldungAsync(id, CurrentUser, CancellationToken);
                return Ok(dto);
            }
            catch (UnauthorizedAccessException x) {

                logger.Error(x);
                return ResponseMessage(new HttpResponseMessage(System.Net.HttpStatusCode.Forbidden));
            }
            catch (SamsNotFoundException x) {

                logger.Error(x);
                return ResponseMessage(new HttpResponseMessage(System.Net.HttpStatusCode.NotFound));
            }
            catch (UserException x) {

                logger.Error(x);
                return BadRequest(x.Explain());
            }
            catch (Exception x) {

                logger.Error(x);
                throw;   
            }
        }


        /// <summary>
        /// Gibt Meldungen anhand eines Filters in einer "Paged List"
        /// </summary>
        /// <remarks>
        /// +
        /// * Benutzer muß Eigentümer der Meldung sein oder Rolle SAMS_QM, SAMS_ELGF, SAMS_ADM oder SAMS_RB haben oder Meldung muß öffentlich sein
        /// * Meldung muß letzte Version sein (muß noch definiert werden wie QS diese Liste benutzt)
        /// * Listenobjekt enthält eine Anzahl von Meldungen als Seite
        /// </remarks>
        /// <response code="200"></response>
        /// <response code="400">Filterobjekt nicht richtig übergeben</response>
        /// <response code="403">Keine Berechtigung</response>
        /// <response code="500">Fehler in der Datenbank</response>
        /// <returns>List MeldungListDto: reduziertes Listenobjekt (Detailobjekt bei Klicken anfordern)</returns>
        [Route("api/meldungen")]
        [ResponseType(typeof(MeldungListDto))]
        public async Task<IHttpActionResult> PostMeldungen(MeldungFilter filter)
        {

            if (!ModelState.IsValid) { return BadRequest(ModelState); }

            try {
            
                var list = await store.MeldungenAsync(filter, CurrentUser, CancellationToken);
                return Ok(list);
            }
            catch (UnauthorizedAccessException x) {

                logger.Error(x);
                return ResponseMessage(new HttpResponseMessage(System.Net.HttpStatusCode.Forbidden));
            }
            catch (UserException x) {

                logger.Error(x);
                return BadRequest(x.Explain());
            }
            catch (Exception x) {

                logger.Error(x);
                throw;   
            }
        }


        /// <summary>
        /// Gibt öffentliche Meldungen 
        /// </summary>
        /// <remarks>
        /// +
        /// * Benutzer muß Eigentümer der Meldung sein oder Rolle SAMS_QM, SAMS_ELGF, SAMS_ADM oder SAMS_RB haben oder Meldung muß öffentlich sein
        /// * Meldung muß letzte Version sein (muß noch definiert werden wie QS diese Liste benutzt)
        /// * Listenobjekt enthält eine Anzahl von Meldungen als Seite
        /// </remarks>
        /// <response code="200"></response>
        /// <response code="400">Filterobjekt nicht richtig übergeben</response>
        /// <response code="403">Keine Berechtigung</response>
        /// <response code="500">Fehler in der Datenbank</response>
        /// <returns>List MeldungListDto: reduziertes Listenobjekt (Detailobjekt bei Klicken anfordern)</returns>
        [Route("api/meldungen/publicTop10")]
        [ResponseType(typeof(MeldungDto))]
        public async Task<IHttpActionResult> GetPublicTop10()
        {

            try {
            
                var list = await store.PublicTop10Async(CurrentUser, CancellationToken);
                return Ok(list);
            }
            catch (UnauthorizedAccessException x) {

                logger.Error(x);
                return ResponseMessage(new HttpResponseMessage(System.Net.HttpStatusCode.Forbidden));
            }
            catch (UserException x) {

                logger.Error(x);
                return BadRequest(x.Explain());
            }
            catch (Exception x) {

                logger.Error(x);
                throw;   
            }
        }


        /// <summary>
        /// Speichert eine neue Meldung
        /// </summary>
        /// <remarks>
        /// +
        /// * Benutzer muß Rolle SAMS_MA haben 
        /// </remarks>
        /// <response code="200"></response>
        /// <response code="400">Meldeobjekt nicht richtig übergeben</response>
        /// <response code="403">Keine Berechtigung</response>
        /// <response code="500">Fehler in der Datenbank</response>
        [Route("api/meldung")]
        [ResponseType(typeof(MeldungDto))]
        public async Task<IHttpActionResult> PostMeldung(MeldungDto dto)
        {

            if (!ModelState.IsValid) { return BadRequest(ModelState); }

            try {
            
                dto = await editStore.InsertAsync(dto, CurrentUser, CancellationToken);
                return Ok(dto);
            }
            catch (UnauthorizedAccessException x) {

                logger.Error(x);
                return ResponseMessage(new HttpResponseMessage(System.Net.HttpStatusCode.Forbidden));
            }
            catch (UserException x) {

                logger.Error(x);
                return BadRequest(x.Explain());
            }
            catch (Exception x) {

                logger.Error(x);
                throw;   
            }
        }


        /// <summary>
        /// Aktualisiert eine Meldung
        /// </summary>
        /// <remarks>
        /// +
        /// * Benutzer muß Eigentümer der Meldung sein oder Rolle SAMS_QM, SAMS_ELGF, SAMS_ADM oder SAMS_RB haben
        /// * es muß noch spezifiziert werden wann eine Meldung überschrieben und wann eine Kopie erzeugt wird
        /// </remarks>
        /// <response code="200"></response>
        /// <response code="400">Meldeobjekt nicht richtig übergeben</response>
        /// <response code="403">Benutzer hat kein Zugriffsrecht auf diese Meldung</response>
        /// <response code="404">Es gibt keine Meldung mit dieser ID</response>
        /// <response code="500">Fehler in der Datenbank</response>
        [Route("api/meldung")]
        [ResponseType(typeof(MeldungDto))]
        public async Task<IHttpActionResult> PutMeldung(MeldungDto dto)
        {

            if (!ModelState.IsValid) { return BadRequest(ModelState); }

            try {
            
                dto = await editStore.UpdateAsync(dto, CurrentUser, CancellationToken);
                return Ok(dto);
            }
            catch (UnauthorizedAccessException x) {

                logger.Error(x);
                return ResponseMessage(new HttpResponseMessage(System.Net.HttpStatusCode.Forbidden));
            }
            catch (SamsNotFoundException x) {

                logger.Error(x);
                return ResponseMessage(new HttpResponseMessage(System.Net.HttpStatusCode.NotFound));
            }
            catch (UserException x) {

                logger.Error(x);
                return BadRequest(x.Explain());
            }
            catch (Exception x) {

                logger.Error(x);
                throw;   
            }
        }


        /// <summary>
        /// Löscht eine Meldung
        /// </summary>
        /// <remarks>
        /// +
        /// * Benutzer muß Eigentümer der Meldung sein oder Rolle SAMS_QM, SAMS_ELGF, SAMS_ADM oder SAMS_RB haben
        /// * es muß noch spezifiziert werden wann eine Meldung gelöscht werden darf
        /// </remarks>
        /// <response code="200"></response>
        /// <response code="400">ID nicht richtig übergeben</response>
        /// <response code="403">Benutzer hat kein Zugriffsrecht auf diese Meldung</response>
        /// <response code="404">Es gibt keine Meldung mit dieser ID</response>
        /// <response code="500">Fehler in der Datenbank</response>
        [Route("api/meldung/{id}")]
        public async Task<IHttpActionResult> DeleteMeldung(int id)
        {

            if (!ModelState.IsValid) { return BadRequest(ModelState); }

            try {
            
                await editStore.DeleteAsync(id, CurrentUser, CancellationToken);
                return Ok();
            }
            catch (UnauthorizedAccessException x) {

                logger.Error(x);
                return ResponseMessage(new HttpResponseMessage(System.Net.HttpStatusCode.Forbidden));
            }
            catch (SamsNotFoundException x) {

                logger.Error(x);
                return ResponseMessage(new HttpResponseMessage(System.Net.HttpStatusCode.NotFound));
            }
            catch (UserException x) {

                logger.Error(x);
                return BadRequest(x.Explain());
            }
            catch (Exception x) {

                logger.Error(x);
                throw;   
            }
        }
    }
}
