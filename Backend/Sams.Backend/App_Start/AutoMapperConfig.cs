﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Sams.Dtos;
using Sams.Models;


namespace Sams
{
            
    public static class AutoMapperConfig
    {

        public static void CreateMaps()
        {
        
            Mapper.CreateMap<Einrichtung1,   EinrichtungDto>();
            Mapper.CreateMap<Einrichtung2,   EinrichtungDto>();
            Mapper.CreateMap<Einrichtung3,   EinrichtungDto>();
            Mapper.CreateMap<Einrichtung4,   EinrichtungDto>().ForMember(p => p.Children, opt => opt.Ignore());
            Mapper.CreateMap<Meldungstyp,    MeldungstypDto>();
            Mapper.CreateMap<Person,         PersonDto>();
            Mapper.CreateMap<Benutzer,       BenutzerDto>();

            Mapper.CreateMap<Meldung,        MeldungDto>().ForMember(p => p.IstPublic, opt => opt.Ignore())
                                                          .ForMember(p => p.Begruendung, opt => opt.Ignore())
                                                          .ForMember(p => p.Vorschlag, opt => opt.Ignore())
                                                          .AfterMap((model, dto) => { 
                                                                dto.IstPublic = model.IstÖffentlich;
                                                                dto.Begruendung = model.Begründung;
                                                                dto.Vorschlag = model.Lösungsvorschlag;
                                                           });

            Mapper.CreateMap<MeldungDto,     Meldung>().ConvertUsing(new MeldungConverter());

         //   Mapper.Configuration.Seal();
            Mapper.AssertConfigurationIsValid(); 
        }
        

        public class MeldungConverter : ITypeConverter<MeldungDto, Meldung>
        {

            public Meldung Convert(ResolutionContext context)
            {

                var src = context.SourceValue as MeldungDto;   
                var dst = context.DestinationValue == null? new Meldung(): context.DestinationValue as Meldung;

                if (src.Nummer                      != dst.Nummer             ) dst.Nummer              = src.Nummer             ;
                if (src.Meldername                  != dst.Meldername         ) dst.Meldername          = src.Meldername         ;
                if (src.MelderEMail                 != dst.MelderEMail        ) dst.MelderEMail         = src.MelderEMail        ;
                if (src.IstAnonym                   != dst.IstAnonym          ) dst.IstAnonym           = src.IstAnonym          ;
                if (src.Einrichtung                 != dst.Einrichtung        ) dst.Einrichtung         = src.Einrichtung        ;
                if (src.Einrichtung1_Id             != dst.Einrichtung1_Id    ) dst.Einrichtung1_Id     = src.Einrichtung1_Id    ;
                if (src.Einrichtung2_Id             != dst.Einrichtung2_Id    ) dst.Einrichtung2_Id     = src.Einrichtung2_Id    ;
                if (src.Einrichtung3_Id             != dst.Einrichtung3_Id    ) dst.Einrichtung3_Id     = src.Einrichtung3_Id    ;
                if (src.Einrichtung4_Id             != dst.Einrichtung4_Id    ) dst.Einrichtung4_Id     = src.Einrichtung4_Id    ;
                if ((Meldungsstatus)src.Status      != dst.Status             ) dst.Status              = (Meldungsstatus)src.Status;
                if (src.IstPublic                   != dst.IstÖffentlich      ) dst.IstÖffentlich       = src.IstPublic          ;
                if ((Ergebnistyp)src.Ergebnis       != dst.Ergebnis           ) dst.Ergebnis            = (Ergebnistyp)src.Ergebnis;
                if (src.Begruendung                 != dst.Begründung         ) dst.Begründung          = src.Begruendung        ;
                if (src.Meldungstyp_Id              != dst.Meldungstyp_Id     ) dst.Meldungstyp_Id      = src.Meldungstyp_Id     ;
                if (src.Betreff                     != dst.Betreff            ) dst.Betreff             = src.Betreff            ;
                if (src.Kurzbeschreibung            != dst.Kurzbeschreibung   ) dst.Kurzbeschreibung    = src.Kurzbeschreibung   ;
                if (src.Vorschlag                   != dst.Lösungsvorschlag   ) dst.Lösungsvorschlag    = src.Vorschlag          ;
                if ((Risikoklasse)src.Risikoklasse  != dst.Risikoklasse       ) dst.Risikoklasse        = (Risikoklasse)src.Risikoklasse;
                
                return dst;   
            }
        }
    }
}