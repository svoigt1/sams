﻿using System;
using System.Linq;
using System.Data.Common;
using System.Data.Entity.Infrastructure.Interception;
using System.Diagnostics;
using System.Text;
using Castle.Core.Logging;


namespace Sams.Interceptors
{

    internal class StoreLogInterceptor : DbCommandInterceptor
    {
        
        private readonly ILogger logger;
        private readonly Stopwatch stopwatch = new Stopwatch();


        public StoreLogInterceptor(ILogger logger)
        {
            this.logger = logger;
        }


        public override void ScalarExecuting(DbCommand command, DbCommandInterceptionContext<object> interceptionContext)
        {
            
            base.ScalarExecuting(command, interceptionContext);
            stopwatch.Restart();
        }


        public override void ScalarExecuted(DbCommand command, DbCommandInterceptionContext<object> interceptionContext)
        {
            
            stopwatch.Stop();
           
            if (interceptionContext.Exception != null) {

                logger.Error(interceptionContext.Exception, command.CommandText);
            
            } else {

                logger.DebugFormat("StoreLogInterceptor.ScalarExecuted, {0} ms{2}{1}{2}{3}", stopwatch.Elapsed, command.CommandText, System.Environment.NewLine, Parameters(command));
            }

            base.ScalarExecuted(command, interceptionContext);
        }


        public override void NonQueryExecuting(DbCommand command, DbCommandInterceptionContext<int> interceptionContext)
        {
            
            base.NonQueryExecuting(command, interceptionContext);
            stopwatch.Restart();
        }


        public override void NonQueryExecuted(DbCommand command, DbCommandInterceptionContext<int> interceptionContext)
        {
            
            stopwatch.Stop();
            
            if (interceptionContext.Exception != null) {

                logger.Error(interceptionContext.Exception, command.CommandText);
            
            } else {

                logger.DebugFormat("StoreLogInterceptor.NonQueryExecuted, {0} ms{2}{1}{2}{3}", stopwatch.Elapsed, command.CommandText, System.Environment.NewLine, Parameters(command));
            }

            base.NonQueryExecuted(command, interceptionContext);
        }


        public override void ReaderExecuting(DbCommand command, DbCommandInterceptionContext<DbDataReader> interceptionContext)
        {
            
            base.ReaderExecuting(command, interceptionContext);
            stopwatch.Restart();
        }


        public override void ReaderExecuted(DbCommand command, DbCommandInterceptionContext<DbDataReader> interceptionContext)
        {
            
            stopwatch.Stop();
            
            if(interceptionContext.Exception != null) {

                logger.Error(interceptionContext.Exception, command.CommandText);
            
            } else {

                logger.DebugFormat("StoreLogInterceptor.ReaderExecuted, {0} ms{2}{1}{2}{3}", stopwatch.Elapsed, command.CommandText, System.Environment.NewLine, Parameters(command));
            }

            base.ReaderExecuted(command, interceptionContext);
        }


        private String Parameters(DbCommand command)
        {

            if (command == null || command.Parameters == null) return null;

            var bld = new StringBuilder();
            foreach (var par in command.Parameters) {
                   
                var pInfo = par.GetType().GetProperties();
                var a = pInfo.First(p => p.Name == "ParameterName");
                bld.Append(a.GetValue(par)).Append(" => ");
                var b = pInfo.First(p => p.Name == "Value");
                bld.AppendLine(b.GetValue(par).ToString());
            }

            return bld.ToString();
        }
    }
}