﻿using System;
using System.Web;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Mvc;
using Castle.Windsor;
using Castle.Windsor.Installer;
using Sams.Installers;
using Sams.Interceptors;


namespace Sams
{

    public class WebApiApplication : System.Web.HttpApplication
    {

        private static IWindsorContainer container;
        private static ILogger logger;


        protected void Application_Start()
        {

            GlobalConfiguration.Configure(WebApiConfig.Register);
            GlobalConfiguration.Configuration.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Always;
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);

            BootstrapContainer();

            try {

                AutoMapperConfig.CreateMaps();
            }
            catch (Exception x) {

                logger.Error(x);
            }

#if DEBUG

            //var storeInterceptor = container.Resolve<StoreLogInterceptor>(); 
            //System.Data.Entity.Infrastructure.Interception.DbInterception.Add(storeInterceptor);

#endif

        }


        protected void Application_End()
        {
	        if (container != null) container.Dispose();
        }


        protected void Application_Error() 
        {

            HttpContext ctx = HttpContext.Current;
            var ex = ctx.Server.GetLastError();
            logger.Error(ex, String.Format("Fehler bei {0}", ctx.Request.Url));
        }


        private static void BootstrapContainer()
        {

            container = new WindsorContainer().Install(FromAssembly.This());
            logger = container.Resolve<ILogger>();
            GlobalConfiguration.Configuration.Services.Replace(typeof(IHttpControllerActivator), new WindsorCompositionRoot(container));
        }
    }
}
