﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Sams.Models
{

    public class Meldung : Entity
    {

        public int              Version                     { get; set; }
        public int?             Original_Id                 { get; set; }
        public String           Nummer                      { get; set; }
        public bool             IstAnonym                   { get; set; }
        public DateTime         Erstellt                    { get; set; }
        public String           Einrichtung                 { get; set; }
        public int              Einrichtung1_Id             { get; set; }
        public int              Einrichtung2_Id             { get; set; }
        public int?             Einrichtung3_Id             { get; set; }
        public int?             Einrichtung4_Id             { get; set; }
        public Meldungsstatus   Status                      { get; set; }
        public bool             IstÖffentlich               { get; set; }
        public bool             IstLetzte                   { get; set; }
        public Ergebnistyp      Ergebnis                    { get; set; }
        public String           Begründung                  { get; set; }
        public int?             Meldungstyp_Id              { get; set; }
        public String           Betreff                     { get; set; }
        public String           Kurzbeschreibung            { get; set; }
        public String           Lösungsvorschlag            { get; set; }
        public String           Meldername                  { get; set; }
        public String           MelderEMail                 { get; set; }

        public int?             Verantwortlicher_Id         { get; set; }
        public int?             Bearbeiter_Id               { get; set; }
        public int?             Melder_Id                   { get; set; }
        
        public Risikoklasse     Risikoklasse                { get; set; }
       
       
        public virtual Meldung          Original            { get; set; }
        public virtual Meldungstyp      Meldungstyp         { get; set; }
        public virtual Person           Bearbeiter          { get; set; }
        public virtual Person           Verantwortlicher    { get; set; }
        public virtual Person           Melder              { get; set; }
        public virtual Einrichtung1     Einrichtung1        { get; set; }
        public virtual Einrichtung2     Einrichtung2        { get; set; }
        public virtual Einrichtung3     Einrichtung3        { get; set; }
        public virtual Einrichtung4     Einrichtung4        { get; set; }

       
        public virtual ICollection<Kommunikation>  Kommunikationen  { get; set; }
        public virtual ICollection<Meldung>        Kopien           { get; set; }
    
        
        public void ValidateErstellen(Benutzer currentUser, Person dbUser)    
        {

            if (!currentUser.HasAnyRole) throw new UnauthorizedAccessException();
            var vlist = ValidateProperties();
            if (vlist.Count > 0) throw new SamsValidationException("Die Meldung kann nicht erstellt werden.", vlist);

            Version = 1;
            if (!IstAnonym) Melder = dbUser;
            Status = Meldungsstatus.Neu;
            IstLetzte = true;
            Erstellt = DateTime.Now;
        }

        
        public void ValidateAktualisieren(Meldung copy, Benutzer currentUser, Person dbUser)    
        {

            bool isUpdateable = currentUser.IstVerwalter || IstMelder(currentUser) && currentUser.HasAnyRole;
            if (!isUpdateable) throw new UnauthorizedAccessException();
            var vlist = ValidateProperties();
            if (vlist.Count > 0) throw new SamsValidationException("Die Meldung kann nicht aktualisiert werden.", vlist);
            
            if (this.Risikoklasse != copy.Risikoklasse && !currentUser.HasRoleRM) 
                throw new UnauthorizedAccessException("Die Risikoklasse einer Meldung darf nur von einem Risikomanager bearbeitet werden.");

            Emboss(copy);
          //  copy.Version = this.Version + 1;
          //  copy.Original_Id = this.Original_Id ?? this.Id;
          //  copy.Erstellt = DateTime.Now;
          //  copy.IstLetzte = true;
          //  this.IstLetzte = false;
            if (currentUser.HasRoleQS) Bearbeiter = dbUser;             
        }

        
        public void ValidateLöschen(Benutzer currentUser)    
        {

            bool isDeleteable = currentUser.IstVerwalter || IstMelder(currentUser) && currentUser.HasAnyRole;
            if (!isDeleteable) throw new UnauthorizedAccessException();
        }


        private bool IstMelder(Benutzer currentUser)
        {
            return Melder != null && Melder.Sid == currentUser.Sid;
        }


        private List<String> ValidateProperties() 
        {

            var list = new List<String>();
            
            if (Einrichtung1_Id == 0) list.Add("Es wurde keine Einrichtung auf der ersten Ebene angegeben.");   
            if (Einrichtung2_Id == 0) list.Add("Es wurde keine Einrichtung auf der zweiten Ebene angegeben.");   
            if (String.IsNullOrWhiteSpace(Betreff)) list.Add("Es wurde kein Betreff angegeben.");  
            if (Betreff != null && Betreff.Length > 100) list.Add("Der Betreff ist zu lang. Er darf aus maximal 100 Zeichen bestehen.");  
            if (String.IsNullOrWhiteSpace(Kurzbeschreibung)) list.Add("Es wurde keine Kurzbeschreibung angegeben.");  
        
            return list;    
        }


        public void Emboss(Meldung copy)
        {

            if (copy.Nummer              != this.Nummer             ) this.Nummer              = copy.Nummer;
            if (copy.Meldername          != this.Meldername         ) this.Meldername          = copy.Meldername;
            if (copy.MelderEMail         != this.MelderEMail        ) this.MelderEMail         = copy.MelderEMail;
            if (copy.IstAnonym           != this.IstAnonym          ) this.IstAnonym           = copy.IstAnonym;
            if (copy.Einrichtung         != this.Einrichtung        ) this.Einrichtung         = copy.Einrichtung;
            if (copy.Einrichtung1_Id     != this.Einrichtung1_Id    ) this.Einrichtung1_Id     = copy.Einrichtung1_Id;
            if (copy.Einrichtung2_Id     != this.Einrichtung2_Id    ) this.Einrichtung2_Id     = copy.Einrichtung2_Id;
            if (copy.Einrichtung3_Id     != this.Einrichtung3_Id    ) this.Einrichtung3_Id     = copy.Einrichtung3_Id;
            if (copy.Einrichtung4_Id     != this.Einrichtung4_Id    ) this.Einrichtung4_Id     = copy.Einrichtung4_Id;
            if (copy.Status              != this.Status             ) this.Status              = copy.Status;
            if (copy.IstÖffentlich       != this.IstÖffentlich      ) this.IstÖffentlich       = copy.IstÖffentlich;
            if (copy.Ergebnis            != this.Ergebnis           ) this.Ergebnis            = copy.Ergebnis;
            if (copy.Begründung          != this.Begründung         ) this.Begründung          = copy.Begründung;
            if (copy.Meldungstyp_Id      != this.Meldungstyp_Id     ) this.Meldungstyp_Id      = copy.Meldungstyp_Id;
            if (copy.Betreff             != this.Betreff            ) this.Betreff             = copy.Betreff;
            if (copy.Kurzbeschreibung    != this.Kurzbeschreibung   ) this.Kurzbeschreibung    = copy.Kurzbeschreibung;
            if (copy.Lösungsvorschlag    != this.Lösungsvorschlag   ) this.Lösungsvorschlag    = copy.Lösungsvorschlag;
            if (copy.Risikoklasse        != this.Risikoklasse       ) this.Risikoklasse        = copy.Risikoklasse;
        }
    }
}
