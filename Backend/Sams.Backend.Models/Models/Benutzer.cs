﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Sams.Models
{

    /// <summary>
    /// Ein Benutzer ist die angemeldete Person mit den aus dem AD ermittelten Benutzerrollen
    /// </summary>
    /// <remarks>
    /// EF ist zu blöd zu kapieren dass ich den Benutzer nicht persistieren will, Personen aber schon.
    /// modelbuilder.Ignore() funktioniert nicht beim Einfügen. Kotz!
    /// </remarks>
    public class Benutzer
    {
        
        public String   Sid           { get; set; }
        public String   Loginname     { get; set; }
        public String   Name          { get; set; }
        public String   EmailAddress  { get; set; }
        public String   Organization  { get; set; }
       
        public bool HasRoleMA     { get; set; }
        public bool HasRoleQS     { get; set; }
        public bool HasRoleGF     { get; set; }
        public bool HasRoleADM    { get; set; }
        public bool HasRoleRM     { get; set; }

        public bool HasAnyRole    { get { return IstVerwalter || HasRoleMA; } }
        public bool IstVerwalter  { get { return HasRoleQS || HasRoleGF || HasRoleADM || HasRoleRM; } }


        public Person AsPerson()
        {

            var ps = new Person();

            ps.Sid          = Sid;         
            ps.Loginname    = Loginname;   
            ps.Name         = Name;        
            ps.EmailAddress = EmailAddress;
            ps.Organization = Organization;

            return ps;
        }
    }
}
