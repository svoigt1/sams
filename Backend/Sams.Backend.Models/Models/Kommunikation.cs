﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Sams.Models
{

    public class Kommunikation : Entity
    {

        public int          Meldung_Id      { get; set; } 
        public DateTime     Erstellt        { get; set; } 
        public Ereignistyp  Typ             { get; set; } 
        public String       Absender        { get; set; } 
        public String       Empfänger       { get; set; } 
        public String       Betreff         { get; set; } 
        public String       Inhalt          { get; set; } 
        public int          StatusCode      { get; set; } 
        public int          EventCode       { get; set; } 
        public String       Fehler          { get; set; } 
        
        
        public virtual Meldung    Meldung     { get; set; } 
    }
}
