﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Sams.Models
{
    
    public class Person : Entity
    {

        public String   Sid           { get; set; }
        public String   Loginname     { get; set; }
        public String   Name          { get; set; }
        public String   EmailAddress  { get; set; }
        public String   Organization  { get; set; }
       

        public virtual ICollection<Meldung>    MeldungenAlsMelder               { get; set; }
        public virtual ICollection<Meldung>    MeldungenAlsBearbeiter           { get; set; }
        public virtual ICollection<Meldung>    MeldungenAlsVerantwortlicher     { get; set; }
    
        
        /// <summary>
        /// Benutzer in der DB aktualisieren (Person hat geheiratet und hat jetzt anderen Namen)
        /// </summary>
        /// <param name="user"></param>
        public void Emboss(Benutzer other)
        {

            if (this.Sid != other.Sid) return;
            if (this.Loginname != other.Loginname)       this.Loginname = other.Loginname;
            if (this.Name != other.Name)                 this.Name = other.Name;
            if (this.EmailAddress != other.EmailAddress) this.EmailAddress = other.EmailAddress;
        }

        
        public static String ExtractBasename(String fullname)
        {

            if (String.IsNullOrEmpty(fullname)) return fullname;
            int ndx = fullname.LastIndexOf('\\');
            if (ndx == -1) return fullname;
            var str = new String(fullname.Skip(ndx + 1).ToArray());
            return str;
        }
    }
}
