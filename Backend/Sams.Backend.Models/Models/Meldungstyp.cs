﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Sams.Models
{

    public class Meldungstyp : Entity
    {

        public String   Name        { get; set; }
        public String   Farbe       { get; set; }
        public bool     IstAktiv    { get; set; }
        
        
        public virtual ICollection<Meldung>  Meldungen  { get; set; }
    }
}
