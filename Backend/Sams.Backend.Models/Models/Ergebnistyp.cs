﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sams.Models
{
    public enum Ergebnistyp
    {

        Undefiniert = 0,
        Abgelehnt = 1,
        Akzeptiert = 2,
        Realisiert = 3,
        Geplant = 4,
        Spam = 5 
    }
}
