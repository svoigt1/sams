﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Sams.Models
{

    public class Einrichtung4 : Entity
    {

        public String   Name                { get; set; }
        public int      Parent_Id           { get; set; }
        
        public virtual Einrichtung3 Parent      { get; set; }
        public virtual ICollection<Meldung>      Meldungen   { get; set; }
    }
}
