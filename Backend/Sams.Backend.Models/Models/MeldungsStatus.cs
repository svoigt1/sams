﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Sams.Models
{
    
    public enum Meldungsstatus
    {

        Neu = 0,
        Bearbeitung = 1,
        Wartend = 2,
        Abgeschlossen = 3
    }
}
