﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Sams.Models
{

    public enum Risikoklasse
    {

        Undefiniert          = 0,
        Planung              = 1,
        Personal             = 2,
        Organisation         = 3,
        OperativesGeschäft   = 4,
        Rechnungswesen       = 5,
        Sicherheit           = 6,
        ITRisiken            = 7,
        JuristischeRisiken   = 8,
        MedizinischeRisiken  = 9,
        Umweltschutz         = 10
    }
}
