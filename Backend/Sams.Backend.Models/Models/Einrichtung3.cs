﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Sams.Models
{

    public class Einrichtung3 : Entity
    {

        public String Name                  { get; set; }
        public int    Parent_Id             { get; set; }
        
        public virtual Einrichtung2 Parent  { get; set; }
        public virtual ICollection<Einrichtung4> Children    { get; set; }
        public virtual ICollection<Meldung>      Meldungen   { get; set; }
    }
}
