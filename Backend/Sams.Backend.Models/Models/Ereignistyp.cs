﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Sams.Models
{

    public enum Ereignistyp
    {

        Undefiniert = 0,
        NeueMeldung = 1,
        MeldungBC = 2,
        Modifiziert = 3,
        RückfrageQM = 4,
        Entscheidungsfindung = 5,
        Antwort = 6,
        Fristablauf = 7,
        Abschluss = 10
    }
}
