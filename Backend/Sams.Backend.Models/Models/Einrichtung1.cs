﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sams.Models
{
    
    public class Einrichtung1 : Entity
    {

        public String Name  { get; set; }
    
        public virtual ICollection<Einrichtung2> Children    { get; set; }
        public virtual ICollection<Meldung>      Meldungen   { get; set; }
    }
}
