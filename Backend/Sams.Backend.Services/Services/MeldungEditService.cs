﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Sams.Dtos;
using Sams.Models;
using Sams.Repositories;


namespace Sams.Services
{
    
    public class MeldungEditService : IMeldungEditService 
    {

        private readonly IUow store;
        private readonly IMeldungService reader;


        public MeldungEditService(IUow store, IMeldungService reader)
        {

            this.store = store;
            this.reader = reader;
        }


        public async Task<MeldungDto> InsertAsync(MeldungDto dto, Benutzer currentUser, CancellationToken ct = default(CancellationToken))
        {

            var model = Mapper.Map<Meldung>(dto);
            model.ValidateErstellen(currentUser, await GetPersonAsync(currentUser, ct));
            await EinrichtungVervollständigenAsync(model, ct);

            ct.ThrowIfCancellationRequested();

            store.Writer<Meldung>().Insert(model);
            await store.SaveAsync(ct).ConfigureAwait(false);

            ct.ThrowIfCancellationRequested();

            dto = await reader.MeldungAsync(model.Id, currentUser, ct);
            return dto;
        }


        public async Task<MeldungDto> UpdateAsync(MeldungDto dto, Benutzer currentUser, CancellationToken ct = default(CancellationToken))
        {

            var model = await GebeOriginalAsync(dto.Id, ct);
            
            ct.ThrowIfCancellationRequested();
            
            var updmodel = Mapper.Map<Meldung>(dto);
            model.ValidateAktualisieren(updmodel, currentUser, await GetPersonAsync(currentUser, ct));
            await EinrichtungVervollständigenAsync(model, ct);

            ct.ThrowIfCancellationRequested();

            //store.Writer<Meldung>().Insert(updmodel);
            await store.SaveAsync(ct).ConfigureAwait(false);

            ct.ThrowIfCancellationRequested();

            dto = await reader.MeldungAsync(model.Id, currentUser, ct);
            return dto;
        }


        public async Task DeleteAsync(int id, Benutzer currentUser, CancellationToken ct = default(CancellationToken))
        {

            var model = await GebeOriginalAsync(id, ct);
            
            ct.ThrowIfCancellationRequested();
            
            model.ValidateLöschen(currentUser);
            store.Writer<Meldung>().Delete(model);

            await store.SaveAsync(ct).ConfigureAwait(false);
        }


        private async Task<Meldung> GebeOriginalAsync(int id, CancellationToken ct)
        {

            var includes = new List<Expression<Func<Meldung, object>>> { p => p.Melder };
            var model = await store.Writer<Meldung>().FirstAsync(p => p.Id == id, includes, ct).ConfigureAwait(false);
            if (model == null) throw new SamsNotFoundException();

            return model;
        }


        /// <summary>
        /// Person in DB speichern (nicht jede Person beim AD anfragen)
        /// </summary>
        /// <param name="currentUser">angemeldeter AD user</param>
        /// <returns>DB-persistierter User</returns>
        private async Task<Person> GetPersonAsync(Benutzer currentUser, CancellationToken ct)
        {

            var model = await store.WriterQuery<Person>().Where(p => p.Sid == currentUser.Sid).FirstAsync(ct).ConfigureAwait(false);
            if (model == null) {
            
                var ps = currentUser.AsPerson();
                store.Writer<Person>().Insert(ps);
                return ps;
            }    

            model.Emboss(currentUser);
            return model;
        }        


        /// <summary>
        /// Einrichtung als Text geben
        /// </summary>
        /// <param name="currentUser">angemeldeter AD user</param>
        /// <returns>DB-persistierter User</returns>
        private async Task EinrichtungVervollständigenAsync(Meldung model, CancellationToken ct)
        {

            if (model.Einrichtung4_Id.HasValue) {

                var includes4 = new List<Expression<Func<Einrichtung4, object>>> { p => p.Parent.Parent.Parent };
                var e4 = await store.Reader<Einrichtung4>().FirstAsync(p => p.Id == model.Einrichtung4_Id.Value, includes4, ct).ConfigureAwait(false);
                model.Einrichtung = String.Format("{0}, {1}, {2}, {3}", e4.Parent.Parent.Parent.Name, e4.Parent.Parent.Name, e4.Parent.Name, e4.Name);
                return;
            }

            if (model.Einrichtung3_Id.HasValue) {

                var includes3 = new List<Expression<Func<Einrichtung3, object>>> { p => p.Parent.Parent };
                var e3 = await store.Reader<Einrichtung3>().FirstAsync(p => p.Id == model.Einrichtung3_Id.Value, includes3, ct).ConfigureAwait(false);
                model.Einrichtung = String.Format("{0}, {1}, {2}", e3.Parent.Parent.Name, e3.Parent.Name, e3.Name);
                return;
            }

            var includes2 = new List<Expression<Func<Einrichtung2, object>>> { p => p.Parent };
            var e2 = await store.Reader<Einrichtung2>().FirstAsync(p => p.Id == model.Einrichtung2_Id, includes2, ct).ConfigureAwait(false);
            model.Einrichtung =  String.Format("{0}, {1}", e2.Parent.Name, e2.Name);
        } 
    }
}
