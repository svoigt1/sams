﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Sams.Dtos;
using Sams.Models;


namespace Sams.Services
{
    
    public interface IMeldungService
    {
        
        Task<List<MeldungstypDto>>      MeldungstypenAsync(CancellationToken ct = default(CancellationToken));
        Task<MeldungDto>                MeldungAsync(int id, Benutzer currentUser, CancellationToken ct = default(CancellationToken));
        Task<List<MeldungDto>>          PublicTop10Async(Benutzer currentUser, CancellationToken ct = default(CancellationToken));
        Task<MeldungListDto>            MeldungenAsync(MeldungFilter filter, Benutzer currentUser, CancellationToken ct = default(CancellationToken));
    }
}
