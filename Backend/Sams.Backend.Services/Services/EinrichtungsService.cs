﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Sams.Dtos;
using Sams.Models;
using Sams.Repositories;


namespace Sams.Services
{
   
    public class EinrichtungsService : IEinrichtungsService
    {

        private readonly IUow store;


        public EinrichtungsService(IUow store)
        {
            this.store = store;
        }


        public async Task<List<EinrichtungDto>> GetVerzeichnisAsync(CancellationToken ct = default(CancellationToken))
        {
            
            var includes = new List<Expression<Func<Einrichtung1, object>>> { p => p.Children.Select(q => q.Children.Select(r => r.Children)) };
            var qry = await store.Reader<Einrichtung1>().FindAllAsync<Einrichtung1>(includes:includes, ct:ct).ConfigureAwait(false); 
            var dto = Mapper.Map<List<EinrichtungDto>>(qry);
            
            return dto;
        }
    }
}
