﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Sams.Dtos;
using Sams.Models;
using Sams.Repositories;


namespace Sams.Services
{
    
    public class MeldungService : IMeldungService
    {

        private readonly IUow store;


        public MeldungService(IUow store)
        {
            this.store = store;
        }


        public async Task<List<MeldungstypDto>> MeldungstypenAsync(CancellationToken ct = default(CancellationToken))
        {

            var qry = await store.ReaderQuery<Meldungstyp>().OrderBy(p => p.Id).MaterializeAsync(ct).ConfigureAwait(false);
            return Mapper.Map<List<MeldungstypDto>>(qry);
        }


        public async Task<MeldungDto> MeldungAsync(int id, Benutzer currentUser, CancellationToken ct = default(CancellationToken))
        {

            var includes = new List<Expression<Func<Meldung, object>>> { p => p.Bearbeiter, p => p.Melder, p => p.Meldungstyp, p => p.Verantwortlicher };
            var model = await store.Reader<Meldung>().FirstAsync(p => p.Id == id, includes, ct).ConfigureAwait(false);
            if (model == null) throw new SamsNotFoundException();

            if (!currentUser.IstVerwalter) {

                if (!currentUser.HasAnyRole) throw new UnauthorizedAccessException();
                if (!model.IstÖffentlich && (model.Melder == null || model.Melder.Sid != currentUser.Sid)) throw new UnauthorizedAccessException();
            }
            
            var dto = Mapper.Map<MeldungDto>(model);
            return dto;
        }


        public async Task<MeldungListDto> MeldungenAsync(MeldungFilter filter, Benutzer currentUser, CancellationToken ct = default(CancellationToken))
        {
            
            if (!currentUser.HasAnyRole) throw new UnauthorizedAccessException();

            var includes = new List<Expression<Func<Meldung, object>>> { p => p.Bearbeiter, p => p.Melder, p => p.Meldungstyp, p => p.Verantwortlicher };
            var query = CreateFilter(filter, currentUser);

            var result = new MeldungListDto(); 
            result.InlineCount = await store.Reader<Meldung>().CountAsync<Meldung>(query, ct).ConfigureAwait(false);
            result.Draw = filter.Draw;
            var models = await store.Reader<Meldung>().FindAllAsync<Meldung>(query, CreateSort(filter), includes, null, (filter.Page - 1) * filter.Count, filter.Count, ct).ConfigureAwait(false);
            result.Data = Mapper.Map<List<MeldungDto>>(models);

            return result;
        }


        public async Task<List<MeldungDto>> PublicTop10Async(Benutzer currentUser, CancellationToken ct = default(CancellationToken))
        {
            
            if (!currentUser.HasAnyRole) throw new UnauthorizedAccessException();

            var includes = new List<Expression<Func<Meldung, object>>> { p => p.Bearbeiter, p => p.Melder, p => p.Meldungstyp, p => p.Verantwortlicher };
            var query = new List<Expression<Func<Meldung, bool>>> { p => p.IstLetzte, p => p.IstÖffentlich };

            var models = await store.Reader<Meldung>().FindAllAsync<Meldung>(query, p => p.OrderByDescending(q => q.Erstellt), includes, null, 0, 10, ct:ct).ConfigureAwait(false);
            var result = Mapper.Map<List<MeldungDto>>(models);

            return result;
        }


        /// <remarks>
        /// TODO: in dynamische Linq-Expression wandeln
        /// </remarks>
        private Func<IQueryable<Meldung>, IOrderedQueryable<Meldung>> CreateSort(MeldungFilter filter)
        {

            if (filter.Sorting.IsNullOrEmpty()) return p => p.OrderBy(q => q.Erstellt);   
           
            if (filter.Sorting[0].key == "Nummer" && filter.Sorting[0].order == "desc") return p => p.OrderByDescending(q => q.Nummer);
            if (filter.Sorting[0].key == "Nummer") return p => p.OrderBy(q => q.Nummer);
         
            if (filter.Sorting[0].key == "Meldungstyp" && filter.Sorting[0].order == "desc") return p => p.OrderByDescending(q => q.Meldungstyp.Name);
            if (filter.Sorting[0].key == "Meldungstyp") return p => p.OrderBy(q => q.Meldungstyp.Name);
          
            if (filter.Sorting[0].key == "Einrichtung" && filter.Sorting[0].order == "desc") return p => p.OrderByDescending(q => q.Einrichtung);
            if (filter.Sorting[0].key == "Einrichtung") return p => p.OrderBy(q => q.Einrichtung);
          
            if (filter.Sorting[0].key == "Begruendung" && filter.Sorting[0].order == "desc") return p => p.OrderByDescending(q => q.Begründung);
            if (filter.Sorting[0].key == "Begruendung") return p => p.OrderBy(q => q.Begründung);
          
            if (filter.Sorting[0].key == "Betreff" && filter.Sorting[0].order == "desc") return p => p.OrderByDescending(q => q.Betreff);
            if (filter.Sorting[0].key == "Betreff") return p => p.OrderBy(q => q.Betreff);
          
            if (filter.Sorting[0].key == "Kurzbeschreibung" && filter.Sorting[0].order == "desc") return p => p.OrderByDescending(q => q.Kurzbeschreibung);
            if (filter.Sorting[0].key == "Kurzbeschreibung") return p => p.OrderBy(q => q.Kurzbeschreibung);
          
            if (filter.Sorting[0].key == "Vorschlag" && filter.Sorting[0].order == "desc") return p => p.OrderByDescending(q => q.Lösungsvorschlag);
            if (filter.Sorting[0].key == "Vorschlag") return p => p.OrderBy(q => q.Lösungsvorschlag);
          
            if (filter.Sorting[0].key == "Melder" && filter.Sorting[0].order == "desc") return p => p.OrderByDescending(q => q.Melder.Name);
            if (filter.Sorting[0].key == "Melder") return p => p.OrderBy(q => q.Melder.Name);
          
            if (filter.Sorting[0].key == "Bearbeiter" && filter.Sorting[0].order == "desc") return p => p.OrderByDescending(q => q.Bearbeiter.Name);
            if (filter.Sorting[0].key == "Bearbeiter") return p => p.OrderBy(q => q.Bearbeiter.Name);
          
            if (filter.Sorting[0].key == "Verantwortlicher" && filter.Sorting[0].order == "desc") return p => p.OrderByDescending(q => q.Verantwortlicher.Name);
            if (filter.Sorting[0].key == "Verantwortlicher") return p => p.OrderBy(q => q.Verantwortlicher.Name);
          
            if (filter.Sorting[0].key == "IstPublic" && filter.Sorting[0].order == "desc") return p => p.OrderByDescending(q => q.IstÖffentlich);
            if (filter.Sorting[0].key == "IstPublic") return p => p.OrderBy(q => q.IstÖffentlich);
          
            if (filter.Sorting[0].key == "Ergebnis" && filter.Sorting[0].order == "desc") return p => p.OrderByDescending(q => q.Ergebnis);
            if (filter.Sorting[0].key == "Ergebnis") return p => p.OrderBy(q => q.Ergebnis);
          
            if (filter.Sorting[0].key == "Status" && filter.Sorting[0].order == "desc") return p => p.OrderByDescending(q => q.Status);
            if (filter.Sorting[0].key == "Status") return p => p.OrderBy(q => q.Status);
          
            if (filter.Sorting[0].key == "Risikoklasse" && filter.Sorting[0].order == "desc") return p => p.OrderByDescending(q => q.Risikoklasse);
            if (filter.Sorting[0].key == "Risikoklasse") return p => p.OrderBy(q => q.Risikoklasse);
          
            if (filter.Sorting[0].key == "Erstellt" && filter.Sorting[0].order == "desc") return p => p.OrderByDescending(q => q.Erstellt);
            if (filter.Sorting[0].key == "Erstellt") return p => p.OrderBy(q => q.Erstellt);
           
            return p => p.OrderBy(q => q.Erstellt);           
        }


        private List<Expression<Func<Meldung, bool>>> CreateFilter(MeldungFilter filter, Benutzer currentUser)
        {

            var query = new List<Expression<Func<Meldung, bool>>> { p => p.IstLetzte };
            String[] positives = { "1", "true", "wahr", "ja", "yes" };
            
            if (!String.IsNullOrWhiteSpace(filter.Ownership) && filter.Ownership.ToLower() == "private") query.Add(p => p.Melder.Sid == currentUser.Sid); 
            else if (!currentUser.IstVerwalter) query.Add(p => p.IstÖffentlich);

            if (!String.IsNullOrWhiteSpace(filter.Query)) {
            
                var f = filter.Query.Trim();
                query.Add(p => p.Nummer == f || p.Einrichtung.Contains(f) || p.Kurzbeschreibung.Contains(f) || p.Lösungsvorschlag.Contains(f) || p.Melder.Name.Contains(f) ||
                               p.Melder.EmailAddress.Contains(f) || p.Meldungstyp.Name.Contains(f) || p.Nummer == f || p.Betreff.Contains(f) || p.Lösungsvorschlag.Contains(f));
            }
            
            if (!filter.MeldeTypen.IsNullOrEmpty()) query.Add(p => filter.MeldeTypen.Any(q => q == p.Meldungstyp_Id));
           
            if (!filter.Filter.IsNullOrEmpty()) {

                foreach (var fo in filter.Filter) {

                    if (fo.key == "Nummer")           query.Add(p => p.Nummer == fo.value);    
                    if (fo.key == "Meldungstyp")      query.Add(p => p.Meldungstyp.Name.StartsWith(fo.value)); 
                    if (fo.key == "Einrichtung")      query.Add(p => p.Einrichtung.Contains(fo.value));    
                    if (fo.key == "Begruendung")      query.Add(p => p.Begründung.Contains(fo.value));    
                    if (fo.key == "Betreff")          query.Add(p => p.Betreff.Contains(fo.value));    
                    if (fo.key == "Kurzbeschreibung") query.Add(p => p.Kurzbeschreibung.Contains(fo.value));    
                    if (fo.key == "Vorschlag")        query.Add(p => p.Lösungsvorschlag.Contains(fo.value));    
                    if (fo.key == "Melder")           query.Add(p => p.Melder.Name.Contains(fo.value) || p.Melder.EmailAddress.Contains(fo.value));    
                    if (fo.key == "Bearbeiter")       query.Add(p => p.Bearbeiter.Name.Contains(fo.value) || p.Bearbeiter.EmailAddress.Contains(fo.value));
                    if (fo.key == "Verantwortlicher") query.Add(p => p.Verantwortlicher.Name.Contains(fo.value) || p.Verantwortlicher.EmailAddress.Contains(fo.value));
                
                    if (fo.key == "IstPublic" && positives.Any(p => p == fo.key.ToLower())) query.Add(p => p.IstÖffentlich);    
               
                    int dummy;   
                    if (fo.key == "Ergebnis"        && Int32.TryParse(fo.value, out dummy)) query.Add(p => p.Ergebnis == (Ergebnistyp)dummy);    
                    if (fo.key == "Status"          && Int32.TryParse(fo.value, out dummy)) query.Add(p => p.Status == (Meldungsstatus)dummy);    
                    if (fo.key == "Risikoklasse"    && Int32.TryParse(fo.value, out dummy)) query.Add(p => p.Risikoklasse == (Risikoklasse)dummy);    
                    if (fo.key == "Einrichtung1_Id" && Int32.TryParse(fo.value, out dummy)) query.Add(p => p.Einrichtung1_Id == dummy);    
                    if (fo.key == "Einrichtung2_Id" && Int32.TryParse(fo.value, out dummy)) query.Add(p => p.Einrichtung2_Id == dummy);    
                    if (fo.key == "Einrichtung3_Id" && Int32.TryParse(fo.value, out dummy)) query.Add(p => p.Einrichtung3_Id == dummy);    
                    if (fo.key == "Einrichtung4_Id" && Int32.TryParse(fo.value, out dummy)) query.Add(p => p.Einrichtung4_Id == dummy);    

                    DateTime dt;
                    if (fo.key == "Erstellt" && DateTime.TryParse(fo.value, out dt)) {
                
                        DateTime nd = dt.Date.AddDays(1);
                        query.Add(p => p.Erstellt >= dt.Date && p.Erstellt < nd);    
                    }

                    if (fo.key == "StartDate" && DateTime.TryParse(fo.value, out dt)) {
                
                        query.Add(p => p.Erstellt >= dt.Date);    
                    }

                    if (fo.key == "EndDate" && DateTime.TryParse(fo.value, out dt)) {
                
                        DateTime nd = dt.Date.AddDays(1);
                        query.Add(p => p.Erstellt < nd);    
                    }
                }
            }
        
            return query;
        }
    }
}
