﻿using System;
using System.Threading;

namespace Sams.Services
{
   
    public interface IAdminService
    {
        System.Threading.Tasks.Task<int> CountAsync(System.Threading.CancellationToken ct = default(CancellationToken));
    }
}
