﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Sams.Dtos;
using Sams.Models;


namespace Sams.Services
{
   
    public interface IEinrichtungsService
    {
        
        Task<List<EinrichtungDto>> GetVerzeichnisAsync(CancellationToken ct = default(CancellationToken));
    }
}
