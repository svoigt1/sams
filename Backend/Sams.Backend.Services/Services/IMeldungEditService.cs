﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Sams.Dtos;
using Sams.Models;


namespace Sams.Services
{

    public interface IMeldungEditService
    {
        
        Task                DeleteAsync(int id, Benutzer currentUser, CancellationToken ct = default(CancellationToken));
        Task<MeldungDto>    InsertAsync(MeldungDto dto, Benutzer currentUser, CancellationToken ct = default(CancellationToken));
        Task<MeldungDto>    UpdateAsync(MeldungDto dto, Benutzer currentUser, CancellationToken ct = default(CancellationToken));
    }
}
