﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Sams.Models;
using Sams.Repositories;


namespace Sams.Services
{

    public class AdminService : IAdminService
    {

        private readonly IUow store;


        public AdminService(IUow store)
        {
            this.store = store;
        }


        public async Task<int> CountAsync(CancellationToken ct = default(CancellationToken))
        {
            
            return await store.Reader<Meldung>().CountAsync<Meldung>(ct:ct);
        }
    }
}
