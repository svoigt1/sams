﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Sams.Views.Common;

namespace Sams.Installers
{

    public class ViewInstaller : IWindsorInstaller
    {
	   
        public void Install(IWindsorContainer container, IConfigurationStore store)
	    {
		           
		    container.Register(Component.For<ILegendeView>().ImplementedBy<LegendeView>().LifestyleTransient());

            container.Register(Classes.FromAssemblyNamed("Sams.WpfClient.Views")
		                   	          .Where(p => p.Name.EndsWith("View"))
                                      .WithServiceDefaultInterfaces());
	    }
    }
}
