﻿using Castle.Facilities.Logging;
using Castle.Facilities.TypedFactory;
using Castle.MicroKernel.Registration;
using Sams.Factories;


namespace Sams.Installers
{

    public class FacilityInstaller : IWindsorInstaller
    {
        
        public void Install(Castle.Windsor.IWindsorContainer container, Castle.MicroKernel.SubSystems.Configuration.IConfigurationStore store)
        {
          
            //ermöglicht Transient support
            container.AddFacility<TypedFactoryFacility>();  
            //gibt Create/Release-Factories für die Erzeugung transienter Objekte
            container.Register(Component.For(typeof(IFactory<>)).AsFactory());
            //packt die Produkte dieser Factories in ein IDisposable-Wrapper damit die Objekte sich automatisch abmelden/vernichten können
            container.Register(Component.For(typeof(IContainerFactory<>)).ImplementedBy(typeof(ContainerFactory<>)));       

            //log4net.Config.XmlConfigurator.Configure(); 
            container.AddFacility<LoggingFacility>(f => f.UseLog4Net());
            container.Register(Component.For<Sams.ILogger>().ImplementedBy<Sams.Logger>());
        }
    }
}
