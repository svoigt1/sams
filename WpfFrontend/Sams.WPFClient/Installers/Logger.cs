﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Sams
{

    public class Logger : Sams.ILogger
    {

        private readonly Castle.Core.Logging.ILogger core;


        public Logger(Castle.Core.Logging.ILogger core)
        {
            this.core = core;
        }

       
        public void Debug(string message)
        {
            core.Debug(message);
        }

        public void DebugFormat(string format,params object[] args)
        {
            core.DebugFormat(format, args);
        }

        public void Error(string message)
        {
            core.Error(message);
        }

        public void Error(Exception ex)
        {
            core.Error(ex.Message);
        }

        public void Error(Exception ex, string message)
        {
            core.Error(message, ex);
        }

        public void ErrorFormat(string format,params object[] args)
        {
            core.ErrorFormat(format, args);
        }

        public void ErrorFormat(Exception ex, string format,params object[] args)
        {
            core.ErrorFormat(ex, format, args);
        }

        public void Info(string message)
        {
            core.Info(message);
        }

        public void InfoFormat(string format,params object[] args)
        {
            core.InfoFormat(format, args);
        }

        public void Warn(string message)
        {
            core.Warn(message);
        }

        public void WarnFormat(string format,params object[] args)
        {
            core.WarnFormat(format, args);
        }
    }
}