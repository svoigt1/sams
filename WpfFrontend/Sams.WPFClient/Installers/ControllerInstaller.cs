﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;


namespace Sams.Installers
{

    public class ControllerInstaller : IWindsorInstaller
    {
	   
        public void Install(IWindsorContainer container, IConfigurationStore store)
	    {

            container.Register(Classes.FromAssemblyNamed("Sams.WpfClient.Application")
		                   	          .Where(p => p.Name.EndsWith("Controller"))
                                      .WithServiceAllInterfaces());

            container.Register(Classes.FromAssemblyNamed("Sams.WpfClient.Application")
		                   	          .Where(p => p.Name.EndsWith("Factory"))
                                      .WithServiceDefaultInterfaces());

            container.Register(Classes.FromAssemblyNamed("Sams.WpfClient.Application")
		                   	          .Where(p => p.Name.EndsWith("Command"))
                                      .WithServiceAllInterfaces());
	    }
    }
}
