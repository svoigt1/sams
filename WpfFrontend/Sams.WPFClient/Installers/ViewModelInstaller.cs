﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Sams.ViewModels.Common;

namespace Sams.Installers
{

    public class ViewModelInstaller : IWindsorInstaller
    {
	   
        public void Install(IWindsorContainer container, IConfigurationStore store)
	    {

        
		    container.Register(Component.For<LegendeViewModel>().ImplementedBy<LegendeViewModel>().LifestyleTransient());

            container.Register(Classes.FromAssemblyNamed("Sams.WpfClient.Application")
		                   	          .Where(p => p.Name.EndsWith("ViewModel"))
                                      .WithServiceSelf()
                                      .WithServiceAllInterfaces());
	    }
    }
}
