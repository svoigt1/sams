﻿using System.Waf.Applications.Services;
using System.Waf.Presentation.Services;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Sams.Gateways;

namespace Sams.Installers
{

    public class RepositoriesInstaller : IWindsorInstaller
    {
	   
        public void Install(IWindsorContainer container, IConfigurationStore store)
	    {
            
            container.Register(Component.For(typeof(IRepository)).ImplementedBy(typeof(Repository)));  
            container.Register(Component.For(typeof(IHttpClientProvider)).ImplementedBy(typeof(HttpClientProvider)));  
	    }
    }
}
