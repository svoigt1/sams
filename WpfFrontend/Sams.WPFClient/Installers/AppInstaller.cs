﻿using System.Threading;
using Castle.MicroKernel.Registration;
using Sams.Services;
using Sams.WpfClient;


namespace Sams.Installers
{

    public class AppInstaller : IWindsorInstaller
    {
        
        public void Install(Castle.Windsor.IWindsorContainer container, Castle.MicroKernel.SubSystems.Configuration.IConfigurationStore store)
        {
            
            container.Register(Component.For<ISettings>().ImplementedBy<Settings>());
        }
    }
}
