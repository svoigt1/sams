﻿using System.Waf.Applications.Services;
using System.Waf.Presentation.Services;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Sams.Services;


namespace Sams.Installers
{

    public class ServicesInstaller : IWindsorInstaller
    {
	   
        public void Install(IWindsorContainer container, IConfigurationStore store)
	    {
            
		    container.Register(Component.For(typeof(IFileDialogService)).ImplementedBy(typeof(FileDialogService)));
 
            container.Register(Classes.FromAssemblyNamed("Sams.WpfClient.Services")
		                   	          .Where(p => p.Name.EndsWith("Factory"))
                                      .WithServiceDefaultInterfaces()
                                      .LifestyleTransient());

            container.Register(Classes.FromAssemblyNamed("Sams.WpfClient.Services")
		                   	          .Where(p => p.Name.EndsWith("Strategy"))
                                      .WithServiceDefaultInterfaces());
		   
            container.Register(Classes.FromAssemblyNamed("Sams.WpfClient.Services")
		                   	          .Where(p => p.Name.EndsWith("Service"))
                                      .WithServiceDefaultInterfaces());
		   
            container.Register(Classes.FromAssemblyNamed("Sams.WpfClient.Application")
		                   	          .Where(p => p.Name.EndsWith("Service"))
                                      .WithServiceDefaultInterfaces());
	    }
    }
}
