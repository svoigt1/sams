﻿using System;


namespace Sams.WpfClient
{

    public class Settings : ISettings
    {

        public String       Accent                      { get; set; }
        public String       Theme                       { get; set; }
        public String       ApplicationVersion          { get; set; }
        public String       ApplicationName             { get; set; }
        public String       ServerAddress               { get; set; }


        public Settings()
        {

            Accent = Sams.Properties.Settings.Default.Accent;  
            Theme = Sams.Properties.Settings.Default.Theme;
            var version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
            ApplicationVersion = String.Format("{0}.{1}.{2}", version.Major, version.Minor, version.Build);
            ApplicationName = Sams.Properties.Settings.Default.ApplicationName;
            ServerAddress = Sams.Properties.Settings.Default.ServerAddress;
        }


        public void Save()
        {

            Sams.Properties.Settings.Default.Accent = Accent;   
            Sams.Properties.Settings.Default.Theme = Theme;   

            Sams.Properties.Settings.Default.Save();   
        }
    }
}
