﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net.Security;
using System.Runtime;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using Castle.MicroKernel;
using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using Castle.Windsor;
using Castle.Windsor.Diagnostics;
using Castle.Windsor.Installer;
using Sams.Controllers;


namespace Sams
{

    public partial class App : System.Windows.Application
    {

        private IWindsorContainer container;
        private ApplicationController controller;
        private ILogger logger;

        public App()
        {
     
            ProfileOptimization.SetProfileRoot(@"C:\Sams");
            ProfileOptimization.StartProfile("Startup.Profile");
        }


        protected override void OnStartup(StartupEventArgs e)
        {

            AppDomain.CurrentDomain.UnhandledException += UnhandledExceptionHandler;
            TaskScheduler.UnobservedTaskException += UnhandledExceptionHandler;
            
            try {

                base.OnStartup(e);
                
                InitializeDic();
                InitializeUserInterface();
                IgnoreTestCertificate();
                InitializeMapper();
                
                logger = container.Resolve<ILogger>();
                controller = container.Resolve<ApplicationController>(); 
                controller.Initialize();
                controller.Run();
            }
            catch (Exception x) { 

                Debug.WriteLine(x.ToString());
                MessageBox.Show(x.ToString());
            }
        }
    
    
        protected override void OnExit(ExitEventArgs e)
        {

            if (controller != null) controller.Shutdown();
            if (container != null) container.Dispose();
            TaskScheduler.UnobservedTaskException -= UnhandledExceptionHandler;
            AppDomain.CurrentDomain.UnhandledException -= UnhandledExceptionHandler;
            base.OnExit(e);
        }


        private void UnhandledExceptionHandler(object sender, UnhandledExceptionEventArgs e)
        {
        
            logger.Error(e.ToString());
            MessageBox.Show(e.ExceptionObject.ToString());
        }


        private void UnhandledExceptionHandler(object sender, UnobservedTaskExceptionEventArgs e)
        {
            logger.Error(e.ToString());
        }


        private void InitializeDic()
        {
 
            container = new WindsorContainer();
           
            var propInjector = container.Kernel.ComponentModelBuilder
                         .Contributors
                         .OfType<Castle.MicroKernel.ModelBuilder.Inspectors.PropertiesDependenciesModelInspector>()
                         .Single();
            container.Kernel.ComponentModelBuilder.RemoveContributor(propInjector);
            
            container.Kernel.Resolver.AddSubResolver(new CollectionResolver(container.Kernel));
            container.Install(FromAssembly.This());
            DebugContainer();
        }


        private void InitializeUserInterface()
        {
            
            Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("de-DE");
            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("de-DE");
            
            FrameworkCompatibilityPreferences.KeepTextBoxDisplaySynchronizedWithTextProperty = false;
            FrameworkElement.LanguageProperty.OverrideMetadata(typeof(FrameworkElement), new FrameworkPropertyMetadata(
                XmlLanguage.GetLanguage(CultureInfo.CurrentCulture.IetfLanguageTag)));
           
            Views.Internal.TextBoxSelectAllOnFocus.RegisterTextBox();

            EventManager.RegisterClassHandler(typeof(ListViewItem), ListViewItem.PreviewGotKeyboardFocusEvent,
                                              new RoutedEventHandler((x, _) => (x as ListViewItem).IsSelected = true));
        }


        private void IgnoreTestCertificate()
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback =  new RemoteCertificateValidationCallback((sender, certification, chain, sslPolicyErrors) => { return true; });
        }  


        private void InitializeMapper()
        {
        }  


        private void DebugContainer()
        {

            var host = (IDiagnosticsHost)container.Kernel.GetSubSystem(SubSystemConstants.DiagnosticsKey);
            var misconfigured = host.GetDiagnostic<IPotentiallyMisconfiguredComponentsDiagnostic>();
            var misconfiguredHandlers = misconfigured.Inspect();
            var lifestyles = host.GetDiagnostic<IPotentialLifestyleMismatchesDiagnostic>();
            var lifestyleHandlers = lifestyles.Inspect();
        }  
    }
}
