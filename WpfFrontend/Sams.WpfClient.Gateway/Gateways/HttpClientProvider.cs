﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Authentication;
using System.Threading;
using System.Threading.Tasks;

namespace Sams.Gateways
{

    public class HttpClientProvider : IHttpClientProvider 
    {

        private readonly ILogger logger;
        private readonly ISettings settings;
        private readonly SemaphoreSlim locker = new SemaphoreSlim(1, 1);


        public HttpClientProvider(ILogger logger, ISettings settings)
        {
           
            this.logger = logger;
            this.settings = settings;
        }


        public HttpClient Create()
        {

            var client = CreateClientImpl();
            client.BaseAddress = new Uri(settings.ServerAddress);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            return client;
        }


        private HttpClient CreateClientImpl()
        {

            var handler = new HttpClientHandler { UseDefaultCredentials = true };

            var client = new HttpClient(handler);
            return client;
        }
 

        public async Task EnsureSuccessStatusCodeAsync(HttpResponseMessage response)
        {

            if (response.StatusCode == System.Net.HttpStatusCode.OK) return;
            if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized) throw new AuthenticationException("Sie haben keinen Zugriff auf diese Ressource.");
            if (response.StatusCode == System.Net.HttpStatusCode.MethodNotAllowed) throw new AuthenticationException("Der Server unterstützt nicht diese HTTP-Methode.");
            if (response.StatusCode == System.Net.HttpStatusCode.NotFound) throw new NotSupportedException("Die angeforderte Ressource wurde auf dem Server nicht gefunden.");
           
            var msg = await response.Content.ReadAsStringAsync();
            var ex = new SamsServerException(msg);
            throw ex;
        }
    }
}
