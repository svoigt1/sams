﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;


namespace Sams.Gateways
{

    public interface IHttpClientProvider
    {

        HttpClient  Create();
        Task        EnsureSuccessStatusCodeAsync(HttpResponseMessage response);
    }
}
