﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Formatting;


namespace Sams.Gateways
{

    public class Repository : IRepository
    {

        private readonly IHttpClientProvider provider;
        private readonly ILogger logger;


        public Repository(IHttpClientProvider provider, ILogger logger)
        {
            
            this.provider = provider;
            this.logger = logger;
        }


        public async Task<T> GetAsync<T>(String uri, CancellationToken ct = default(CancellationToken))
        {
           
            logger.Info("GET " + uri); 

            using (var client = provider.Create()) 
            using (var response = await client.GetAsync(uri, ct)) {

                await provider.EnsureSuccessStatusCodeAsync(response).ConfigureAwait(false);
                var result = await response.Content.ReadAsAsync<T>(ct).ConfigureAwait(false);
                return result;
            }
        }


        public async Task PostAsync<T>(String uri, T dto, CancellationToken ct = default(CancellationToken))
        {
           
            logger.Info("POST " + uri); 

            using (var client = provider.Create()) 
            using (var response = await client.PostAsJsonAsync(uri, dto, ct).ConfigureAwait(false)) {
            
                await provider.EnsureSuccessStatusCodeAsync(response).ConfigureAwait(false);
            }
        }


        public async Task<TDst> PostAsync<TSrc, TDst>(String uri, TSrc dto, CancellationToken ct = default(CancellationToken))
        {
           
            logger.Info("POST " + uri); 

            using (var client = provider.Create()) 
            using (var response = await client.PostAsJsonAsync(uri, dto, ct)) {
            
                await provider.EnsureSuccessStatusCodeAsync(response);
                var result = await response.Content.ReadAsAsync<TDst>(ct).ConfigureAwait(false);
                return result;
            }
        }


        public async Task PutAsync<T>(String uri, T dto, CancellationToken ct = default(CancellationToken))
        {
           
            logger.Info("PUT " + uri); 

            using (var client = provider.Create()) 
            using (var response = await client.PutAsJsonAsync(uri, dto, ct)) {
            
                await provider.EnsureSuccessStatusCodeAsync(response);
            }
        }


        public async Task DeleteAsync(String uri, CancellationToken ct = default(CancellationToken))
        {
           
            logger.Info("DELETE " + uri); 

            using (var client = provider.Create()) 
            using (var response = await client.DeleteAsync(uri, ct)) {
            
                await provider.EnsureSuccessStatusCodeAsync(response);
            }
        }
    }
}
