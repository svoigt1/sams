﻿using System;
using System.Threading;
using System.Threading.Tasks;


namespace Sams.Gateways
{

    public interface IRepository
    {
        
        Task<T>     GetAsync<T>(String uri, CancellationToken ct = default(CancellationToken));

        Task        PostAsync<T>(String uri, T dto,System.Threading.CancellationToken ct = default(CancellationToken));
        Task<TDst>  PostAsync<TSrc, TDst>(String uri, TSrc dto, CancellationToken ct = default(CancellationToken));
        Task        PutAsync<T>(String uri, T dto, CancellationToken ct = default(CancellationToken));
        Task        DeleteAsync(String uri, CancellationToken ct = default(CancellationToken));
    }
}
