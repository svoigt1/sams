﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace Sams.Gateways
{

    public class SamsServerException : Exception
    {

        private class Exception3
        {
            public string Message { get; set; }
            public string ExceptionMessage { get; set; }
            public string ExceptionType { get; set; }
            public string StackTrace { get; set; }
        }

        private class Exception2
        {
            public string Message { get; set; }
            public string ExceptionMessage { get; set; }
            public string ExceptionType { get; set; }
            public string StackTrace { get; set; }
            public Exception3 InnerException { get; set; }
        }

        private class Exception1
        {
            public string Message { get; set; }
            public string ExceptionMessage { get; set; }
            public string ExceptionType { get; set; }
            public string StackTrace { get; set; }
            public Exception2 InnerException { get; set; }
        }

        private class Error
        {
            public String error { get; set; }
            public String error_description { get; set; }
        }

        

        private String exceptionMessage;
        private String exceptionType;
        private String stackTrace;
        private String message;
        
        public     String   ExceptionMessage    { get { return exceptionMessage; } }
        public     String   ExceptionType       { get { return exceptionType; } }
        public new String   StackTrace          { get { return stackTrace; } }
        public new String   Message             { get { return message; } }


        public SamsServerException(String text) : base(ParseMessage(text))
        {
            
            if (text.Trim().StartsWith("{")) ParseJson(text);
            else ParseHtml(text);
        }


        public override string ToString()
        {
           
            var bld = new StringBuilder();
            if (!String.IsNullOrWhiteSpace(ExceptionType)) bld.Append(ExceptionType).Append(": ");
            if (!String.IsNullOrWhiteSpace(ExceptionMessage)) bld.Append(ExceptionMessage);
            else bld.Append(Message);
            if (!String.IsNullOrWhiteSpace(StackTrace)) bld.AppendLine().Append(StackTrace);
            return bld.ToString();
        }


        private void ParseJson(String text)
        {    
         
            Exception1 ex = JsonConvert.DeserializeObject<Exception1>(text);

            if (!String.IsNullOrEmpty(ex.Message)) {

                var msgs   = new List<String> { ex.Message };
                var exmsgs = new List<String> { ex.ExceptionMessage };
                var stacks = new List<String> { ex.StackTrace };
                exceptionType = ex.ExceptionType;
            
                if (ex.InnerException != null) {
                
                    msgs.Add(ex.InnerException.Message);
                    exmsgs.Add(ex.InnerException.ExceptionMessage);
                    stacks.Add(ex.InnerException.StackTrace);
                    exceptionType = ex.InnerException.ExceptionType;
            
                    if (ex.InnerException.InnerException != null) {

                        msgs.Add(ex.InnerException.InnerException.Message);
                        exmsgs.Add(ex.InnerException.InnerException.ExceptionMessage);
                        stacks.Add(ex.InnerException.InnerException.StackTrace);
                        exceptionType = ex.InnerException.InnerException.ExceptionType;
                    }
                }  

                message = String.Join(System.Environment.NewLine, msgs.Where(p => !p.Contains("n error has occurred") && !p.Contains("ee the inner exception for details")));
                exceptionMessage = String.Join(System.Environment.NewLine, exmsgs.Where(p => !String.IsNullOrEmpty(p) && !p.Contains("ee the inner exception for details")));
                if (String.IsNullOrEmpty(message)) message = exceptionMessage;
                var sep = String.Format("{0}---inner exception{0}", System.Environment.NewLine);
                stackTrace = String.Join(sep, stacks);
                if (String.IsNullOrEmpty(message)) message = "Ein Serverfehler ist aufgetreten.";
                
                return;
            }
            
            Error er = JsonConvert.DeserializeObject<Error>(text);
            exceptionMessage = er.error;
            message = er.error_description;  
        }


        private void ParseHtml(String text)
        {

            if (text.Contains(@"<!DOCTYPE html")) {

                int start = text.IndexOf("<title>");  
                int ende = text.IndexOf("</title>");  

                if (start > -1 && ende > start) message = text.Substring(start + 7, ende - start - 7);
            }
        }


        private static String ParseMessage(String text)
        {

            if (text.Trim().StartsWith("{")) {

                var stack = new Stack<String>();

                Exception1 ex = JsonConvert.DeserializeObject<Exception1>(text);
                if (ex != null && !String.IsNullOrEmpty(ex.Message)) {

                    if (ex != null && ex.InnerException != null && ex.InnerException.InnerException != null) {
                
                        if (!String.IsNullOrEmpty(ex.InnerException.InnerException.Message)) stack.Push(ex.InnerException.InnerException.Message);
                        if (!String.IsNullOrEmpty(ex.InnerException.InnerException.ExceptionMessage)) stack.Push(ex.InnerException.InnerException.ExceptionMessage);
                    }

                    if (ex != null && ex.InnerException != null) {
                
                        if (!String.IsNullOrEmpty(ex.InnerException.Message)) stack.Push(ex.InnerException.Message);
                        if (!String.IsNullOrEmpty(ex.InnerException.ExceptionMessage)) stack.Push(ex.InnerException.ExceptionMessage);
                    }
                
                    if (ex != null) {
                
                        if (!String.IsNullOrEmpty(ex.Message)) stack.Push(ex.Message);
                        if (!String.IsNullOrEmpty(ex.ExceptionMessage)) stack.Push(ex.ExceptionMessage);
                    }

                    if (stack.Count > 0) return stack.FirstOrDefault(p => !p.Contains("n error has occurred") && !p.Contains("ee the inner exception for details"));
                    return "Ein Serverfehler ist aufgetreten.";

                } else {

                    Error er = JsonConvert.DeserializeObject<Error>(text);
                    return er.error_description;
                }           
            }

            if (text.Contains(@"<!DOCTYPE html")) {

                int start = text.IndexOf("<title>");  
                int ende = text.IndexOf("</title>");  

                if (start > -1 && ende > start) return text.Substring(start + 7, ende - start - 7);
            }
            
            return null;
        }
    }
}
