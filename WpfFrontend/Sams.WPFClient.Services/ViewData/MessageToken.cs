﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Sams.ViewData
{
    
    public class MessageToken
    {

        private IList<String> reasons;


        public String Header { get; set; }

        public IList<String> Reasons 
        { 
            get { return reasons; } 
            set {
            
                reasons = new List<String>();
                if (value == null) return;
                value.ForEach(p => reasons.Add((p.StartsWith("• ")) ? p: "• " + p));  
            } 
        }


        public MessageToken()
        {
            Reasons = new List<String>();   
        }


        public MessageToken(SamsValidationException exception)
        {
            
            Header = exception.Message;
            Reasons = exception.Explanations;   
        }
    }
}
