﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sams.ViewData
{
    public class CreateMeldungToken : MessageToken
    {
        public int meldungsTyp_Id { get; set; }

        public CreateMeldungToken(int m_Id = 0) : base()
        {
            this.meldungsTyp_Id = m_Id;
        }
    }
}
