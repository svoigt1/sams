﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Sams.ViewData
{

    public class Suchkriterien
    {

        public Suchkriterien()
        {
            Kriterien = new Dictionary<String, object>();
        }


        public Dictionary<String, Object> Kriterien    { get; private set; }
        
        
        public void Add(String key, object val)
        {
            //if (val != null) Kriterien.Add(key, val.ToString());
            if (val != null) Kriterien.Add(key, val);
        }  
        
        
        public String AsString(String key)
        {
        
            if (!Kriterien.ContainsKey(key)) return null;
            return (String)Kriterien[key];
        } 

        
        public int? AsInt(String key)
        {
        
            int dummy;

            if (!Kriterien.ContainsKey(key)) return null;
            if (!Int32.TryParse((String)Kriterien[key], out dummy)) return null;
            return dummy;
        } 

        
        public decimal? AsDecimal(String key)
        {
        
            decimal dummy;

            if (!Kriterien.ContainsKey(key)) return null;
            if (!Decimal.TryParse((String)Kriterien[key], out dummy)) return null;
            return dummy;
        } 

        
        public bool AsBoolean(String key)
        {
        
            bool dummy;

            if (!Kriterien.ContainsKey(key)) return false;
            return Boolean.TryParse((String)Kriterien[key], out dummy) && dummy;
        } 

        
        public DateTime? AsDateTime(String key)
        {
        
            DateTime dummy;

            if (!Kriterien.ContainsKey(key)) return null;
            if (!DateTime.TryParse((String)Kriterien[key], out dummy)) return null;
            return dummy;
        } 
    }
}
