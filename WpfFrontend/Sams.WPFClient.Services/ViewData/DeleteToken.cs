﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Sams.ViewData
{
    
    public class DeleteToken : MessageToken
    {

        public bool    IsDeleteable    { get; set; }
    }
}
