﻿using System.Threading;
using System.Threading.Tasks;
using Sams.Gateways;
using Sams.Models;


namespace Sams.Services
{

    public class UserService : IUserService
    {

        private readonly IRepository store;


        public UserService (IRepository store)
        {
            this.store = store;            
        }


        public async Task<Nutzer> CurrentUserAsync(CancellationToken ct = default(CancellationToken))
        {
            
            var user = await store.GetAsync<Nutzer>("/api/user", ct).ConfigureAwait(false);
            return user;
        }
    }
}
