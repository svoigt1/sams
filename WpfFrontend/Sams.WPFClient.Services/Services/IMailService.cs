﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Sams.Services
{
    /// <summary>
    /// Hier werden alle Api Calls für das versenden von Email definiert
    /// </summary>
    public interface IMailService
    {
        Task SendOpinionAsync(String Header, String Body, CancellationToken ct = default(CancellationToken));
    }
}
