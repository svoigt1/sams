﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Sams.Gateways;
using Sams.Models;
using Sams.Dtos;
using Sams.ViewData;

namespace Sams.Services
{
    public class EinrichtungenService : IEinrichtungenService
    {
        private readonly IRepository store;

        private IList<Einrichtung1> einrichtungen;

        public EinrichtungenService(IRepository store)
        {
            this.store = store;
        }

        public async Task<IList<Einrichtung1>> GetEinrichtungenAsync(CancellationToken ct = default(CancellationToken))
        {
            if(einrichtungen == null)
            {
                einrichtungen = await store.GetAsync<List<Einrichtung1>>("/api/einrichtungen", ct).ConfigureAwait(false);
            }
           
            return einrichtungen;
        }
    }
}
