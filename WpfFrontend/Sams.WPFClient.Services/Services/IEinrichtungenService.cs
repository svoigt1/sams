﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Sams.Models;
using Sams.ViewData;

namespace Sams.Services
{
    public interface IEinrichtungenService
    {
        Task<IList<Einrichtung1>> GetEinrichtungenAsync(CancellationToken ct = default(CancellationToken));
    }
}
