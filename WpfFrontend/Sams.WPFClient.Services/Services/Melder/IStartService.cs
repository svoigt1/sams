﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Sams.Models;
using Sams.ViewData;


namespace Sams.Services.Melder
{
   
    public interface IStartService
    {
        
        Task<IList<Meldung>> PublicTop10Async(CancellationToken ct = default(CancellationToken));
        Task<IList<Meldungstyp>> PublicMeldungsTypsAsync(CancellationToken ct = default(CancellationToken));
        Task<IList<Meldung>> OwnTop10Async(Suchkriterien suche, CancellationToken ct = default(CancellationToken));
    }
}
