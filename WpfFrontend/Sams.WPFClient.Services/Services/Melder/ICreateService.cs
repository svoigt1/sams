﻿using Sams.Models;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Sams.Services.Melder
{
    public interface ICreateService
    {
        Task SaveAsync(Meldung meldung, CancellationToken ct = default(CancellationToken));

        Task<IList<Einrichtung1>> GetEinrichtungenAsync(CancellationToken ct = default(CancellationToken));

        Task<IList<Meldungstyp>> GetMeldungstypenAsync(CancellationToken ct = default(CancellationToken));
    }
}
