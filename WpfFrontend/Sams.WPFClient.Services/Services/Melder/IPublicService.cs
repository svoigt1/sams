﻿using Sams.Models;
using Sams.ViewData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Sams.Services.Melder
{
    public interface IPublicService
    {
        Task<IList<Meldungstyp>> GetMeldungstypenAsync(CancellationToken ct = default(CancellationToken));
        Task<IList<Meldung>> PublicAllAsync(CancellationToken ct = default(CancellationToken));
        Task<IList<Meldung>> PrivateAllAsync(CancellationToken ct = default(CancellationToken));

        Task<Meldung> GetMeldungAsync(int id, CancellationToken ct = default(CancellationToken));

        Task<IList<Einrichtung1>> getEinrichtungenAsync(CancellationToken ct = default(CancellationToken));
    }
}
