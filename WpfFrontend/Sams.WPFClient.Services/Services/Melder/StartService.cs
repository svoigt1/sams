﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Sams.Gateways;
using Sams.Models;
using Sams.Dtos;
using Sams.ViewData;


namespace Sams.Services.Melder
{

    public class StartService : IStartService
    {

        private readonly IRepository store;


        public StartService (IRepository store)
        {
            this.store = store;            
        }


        public async Task<IList<Meldung>> PublicTop10Async(CancellationToken ct = default(CancellationToken))
        {

            var list = await store.GetAsync<List<Meldung>>("/api/meldungen/publicTop10", ct).ConfigureAwait(false);
            return list;
        }

        public async Task<IList<Meldungstyp>> PublicMeldungsTypsAsync(CancellationToken ct = default(CancellationToken))
        {

            var list = await store.GetAsync<List<Meldungstyp>>("/api/meldungstypen", ct).ConfigureAwait(false);
            return list;
        }

        public async Task<IList<Meldung>> OwnTop10Async(Suchkriterien query, CancellationToken ct = default(CancellationToken))
        {
             await Task.Yield();
        return null;
           //var query = new { Page = 1, Count = 10, Ownership = "private", Draw = 0 };
          //  var list  = await store.PostAsync<Dictionary<string,object>, MeldungListDto>("/api/meldungen", query.Kriterien, ct).ConfigureAwait(false);
          //  return list.Data;
        }
    }
}
