﻿using Sams.Gateways;
using Sams.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Sams.Services.Melder
{
    public class CreateService : ICreateService
    {
        private readonly IRepository store;
        private readonly IMeldungenService meldungenService;
        private readonly IEinrichtungenService einrichtungenService;

        public CreateService(IRepository store, IMeldungenService meldungenService, IEinrichtungenService einrichtungenService)
        {
            this.store = store;
            this.meldungenService = meldungenService;
            this.einrichtungenService = einrichtungenService;
        }

        public async Task SaveAsync(Meldung meldung, CancellationToken ct = default(CancellationToken))
        {
            await meldungenService.SaveMeldungAsync(meldung, ct);
        }

        public async Task<IList<Einrichtung1>> GetEinrichtungenAsync(CancellationToken ct = default(CancellationToken))
        {
            return await einrichtungenService.GetEinrichtungenAsync(ct);
        }

        public async Task<IList<Meldungstyp>> GetMeldungstypenAsync(CancellationToken ct = default(CancellationToken))
        {
            return await meldungenService.GetMeldungtypenAsync(ct);
        }
    }
}
