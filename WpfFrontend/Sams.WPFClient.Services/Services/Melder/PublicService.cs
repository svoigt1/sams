﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Sams.Gateways;
using Sams.Models;
using Sams.Dtos;
using Sams.ViewData;
using System;

namespace Sams.Services.Melder
{
    public class PublicService : IPublicService
    {
        private readonly IRepository store;
        private readonly IMeldungenService meldungenService;
        private readonly IEinrichtungenService einrichtungenService;

        public PublicService(IRepository store, IMeldungenService meldungenService, IEinrichtungenService einrichtungenService)
        {
            this.store = store;
            this.meldungenService = meldungenService;
            this.einrichtungenService = einrichtungenService;
        }

        public async Task<IList<Meldungstyp>> GetMeldungstypenAsync(CancellationToken ct = default(CancellationToken))
        {
            return await meldungenService.GetMeldungtypenAsync(ct);
        }

        public async Task<IList<Meldung>> PublicAllAsync(CancellationToken ct = default(CancellationToken))
        {
            Console.WriteLine("SERVICE PublicAllAsync ...");
   
            Suchkriterien query = new Suchkriterien();
            query.Add("Page", 1);
            query.Add("Count", 100000);
            query.Add("Ownership", "all");
            query.Add("Draw", 0);

            query.Add("MeldeTypen", new int[] { 0, 1, 2, 3, 4, 5 });

            //var query = new { Page = 1, Count = 10, Ownership = "private", Draw = 0 };
            return await meldungenService.MeldungenAsync(query, ct);
        }

        public async Task<IList<Meldung>> PrivateAllAsync(CancellationToken ct = default(CancellationToken))
        {

            Console.WriteLine("SERVICE PrivateAllAsync ...");

            Suchkriterien query = new Suchkriterien();
            query.Add("Page", 1);
            query.Add("Count", 10000);
            query.Add("Ownership", "private");
            query.Add("Draw", 0);

            query.Add("MeldeTypen", new int[] { 0, 1, 2, 3, 4, 5 });

            //var query = new { Page = 1, Count = 10, Ownership = "private", Draw = 0 };
            return await meldungenService.MeldungenAsync(query, ct);
        }


        public async Task<Meldung> GetMeldungAsync(int id, CancellationToken ct = default(CancellationToken))
        {
            return await meldungenService.GetMeldungAsync(id, ct);
        }

        public async Task<IList<Einrichtung1>> getEinrichtungenAsync(CancellationToken ct = default(CancellationToken))
        {
            var list = await einrichtungenService.GetEinrichtungenAsync(ct);
            return list;
        }
    }
}
