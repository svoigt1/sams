﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Sams.Models;
using Sams.ViewData;
using Sams.Dtos;

namespace Sams.Services
{
    public interface IMeldungenService
    {
        Task<IList<Meldungstyp>> GetMeldungtypenAsync(CancellationToken ct = default(CancellationToken));

        Task<IList<Meldung>> MeldungenAsync(Suchkriterien suche, CancellationToken ct = default(CancellationToken));

        Task<Meldung> GetMeldungAsync(int id, CancellationToken ct = default(CancellationToken));

        Task<Meldung> SaveMeldungAsync(Meldung meldung, CancellationToken ct = default(CancellationToken));

        Task<MeldungListDto> SearchMeldungenAsync(MeldungFilter filter, CancellationToken ct = default(CancellationToken));
    }
}
