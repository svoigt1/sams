﻿using System.Threading;
using System.Threading.Tasks;
using Sams.Models;

namespace Sams.Services
{
    public interface IUserService
    {
        Task<Nutzer> CurrentUserAsync(CancellationToken ct = default(CancellationToken));
    }
}