﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Sams.Gateways;
using Sams.Models;
using Sams.Dtos;
using Sams.ViewData;
using System;

namespace Sams.Services
{
    public class MeldungenService : IMeldungenService
    {
        private readonly IRepository store;

        private IList<Meldungstyp> meldungstypen;

        public MeldungenService(IRepository store)
        {
            this.store = store;
        }

        public async Task<IList<Meldungstyp>> GetMeldungtypenAsync(CancellationToken ct = default(CancellationToken))
        {
            if (meldungstypen == null)
            {
                meldungstypen = await store.GetAsync<IList<Meldungstyp>>("api/meldungstypen", ct);
            }

            return meldungstypen;
        }

        public async Task<IList<Meldung>> MeldungenAsync(Suchkriterien query, CancellationToken ct = default(CancellationToken))
        {
            await Task.Yield();
        return null;
            //var query = new { Page = 1, Count = 10, Ownership = "private", Draw = 0 };
            //var list = await store.PostAsync<Dictionary<string, object>, MeldungListDto>("/api/meldungen", query.Kriterien, ct).ConfigureAwait(false);
            //return list.Data;
        }

        public async Task<Meldung> GetMeldungAsync(int id, CancellationToken ct = default(CancellationToken))
        {
            var Meldung = await store.GetAsync<Meldung>("api/meldung/" + id, ct);
            return Meldung;
        }

        public async Task<Meldung>SaveMeldungAsync(Meldung meldung, CancellationToken ct = default(CancellationToken))
        {
           var Meldung = await store.PostAsync<Meldung, Meldung>("/api/meldung", meldung, ct);
           return Meldung;
        }

        public async Task<MeldungListDto> SearchMeldungenAsync(MeldungFilter filter, CancellationToken ct = default(CancellationToken))
        {
            var Suchergebnis = await store.PostAsync<MeldungFilter, MeldungListDto>("/api/meldungen", filter, ct);

            return Suchergebnis;
        }
    }
}
