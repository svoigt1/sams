﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Sams.Models;
using Sams.ViewData;

namespace Sams.Services.Global
{
    public interface ICommonService
    {
        Task<IList<Einrichtung1>> EinrichtugenAsync(CancellationToken ct = default(CancellationToken));
    }
}
