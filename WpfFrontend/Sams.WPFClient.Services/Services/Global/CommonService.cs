﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Sams.Gateways;
using Sams.Models;
using Sams.Dtos;
using Sams.ViewData;


namespace Sams.Services.Global
{

    public class CommonService : ICommonService
    {

        private readonly IRepository store;


        public CommonService(IRepository store)
        {
            this.store = store;
        }


        public async Task<IList<Einrichtung1>> EinrichtugenAsync(CancellationToken ct = default(CancellationToken))
        {

            var list = await store.GetAsync<List<Einrichtung1>>("/api/einrichtungen", ct).ConfigureAwait(false);
            return list;
        }
    }
}