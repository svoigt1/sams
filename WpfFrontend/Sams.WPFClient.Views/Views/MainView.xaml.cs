﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Sams.Services;
using Sams.ViewModels;


namespace Sams.Views
{

    public partial class MainView : MetroWindow, IMainView
    {
        
        private readonly SynchronizationContext context;
        private TaskCompletionSource<bool> flyoutTcs;
      

        public MainView()
        {
            
            InitializeComponent();
            context = SynchronizationContext.Current;
            FlyoutControl.IsOpenChanged += async (s, e) => { if (flyoutTcs != null && !await FlyoutIsOpenAsync()) flyoutTcs.TrySetResult(false); }; 
        }


        public Task<bool> OpenFlyoutAsync(FlyoutConfig view)
        {

            flyoutTcs = new TaskCompletionSource<bool>();
            
            context.Send(new SendOrPostCallback((o) => {

                    this.FlyoutView.Content  = view.ChildView;
                    FlyoutControl.Header     = view.Header;
                    FlyoutControl.Position   = view.Position;
                    FlyoutControl.IsModal    = view.IsModal;
                    FlyoutControl.IsPinned   = view.IsPinned;
                    FlyoutControl.Theme      = view.Theme;
                    FlyoutControl.IsOpen     = true;
            }), null);

            return flyoutTcs.Task;
        }


        public void OpenFlyout(FlyoutConfig view)
        {
            
            context.Send(new SendOrPostCallback((o) => {

                    this.FlyoutView.Content  = view.ChildView;
                    FlyoutControl.Header     = view.Header;
                    FlyoutControl.Position   = view.Position;
                    FlyoutControl.IsModal    = view.IsModal;
                    FlyoutControl.IsPinned   = view.IsPinned;
                    FlyoutControl.Theme      = view.Theme;
                    FlyoutControl.IsOpen     = true;
            }), null);
        }


        public async Task CloseFlyoutAsync()
        {
            await context.SendAsync(new SendOrPostCallback((o) => { FlyoutControl.IsOpen = false; }), null);
        }


        public async Task<bool> FlyoutIsOpenAsync()
        {

            bool b = false;
            await context.SendAsync(new SendOrPostCallback((o) => { b = FlyoutControl.IsOpen; }), null);
            return b;
        }


        public void ShowMessage(String header, String message)
        {

            context.Post(new SendOrPostCallback((o) => {

                MetroDialogOptions.ColorScheme = MetroDialogColorScheme.Accented;
                this.ShowMessageAsync(header, message);
            }), null);
        }


        public Task<bool> ConfirmExitAsync(string msg = null)
        {
            
            var st = new MetroDialogSettings();
            st.AffirmativeButtonText = "Ja";
            st.NegativeButtonText = "Nein";
            st.AnimateShow = true;
            st.AnimateHide = false;
            String header = "Ihre Bearbeitung wurde noch nicht gespeichert";
            String body = String.IsNullOrEmpty(msg)? "Möchten Sie fortfahren?": msg;

            var tcs = new TaskCompletionSource<bool>();

            context.Send(new SendOrPostCallback(async (o) => {

                var mdr = await this.ShowMessageAsync(header, body, MessageDialogStyle.AffirmativeAndNegative, st);
                tcs.TrySetResult(mdr == MessageDialogResult.Affirmative);
            }), null);

            return tcs.Task;
        }


        private void WindowClosing(object sender, CancelEventArgs e)
        {
            
            var ctx = DataContext as MainViewModel;
            if (ctx == null) return;
            
            e.Cancel = true;
            ctx.ExitCommand.Execute(null);
        }
    }
}
