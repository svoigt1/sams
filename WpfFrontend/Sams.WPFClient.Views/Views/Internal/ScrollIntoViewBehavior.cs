﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Interactivity;


namespace Sams.Views.Internal
{
    
    /// <summary>
    /// zum aktuellen Item scrollen
    /// </summary>
    /// <remarks>
    /// http://www.codeproject.com/Tips/125583/ScrollIntoView-for-a-DataGrid-when-using-MVVM
    /// </remarks>
    public class ScrollIntoViewBehavior : Behavior<DataGrid>
    {
        
        protected override void OnAttached()
        {
            
            base.OnAttached();
            this.AssociatedObject.SelectionChanged += new SelectionChangedEventHandler(AssociatedObject_SelectionChanged);
        }


        void AssociatedObject_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            if (sender is DataGrid) {
                
                DataGrid grid = (sender as DataGrid);
                    
                grid.Dispatcher.InvokeAsync(delegate {
                        
                    grid.UpdateLayout();
                    if (grid.SelectedItem != null) grid.ScrollIntoView(grid.SelectedItem, null);
                });
            }
        }


        protected override void OnDetaching()
        {
            
            base.OnDetaching();
            this.AssociatedObject.SelectionChanged -= new SelectionChangedEventHandler(AssociatedObject_SelectionChanged);
        }
    }
}
