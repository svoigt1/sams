﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using System.Windows.Data;


namespace Sams.Views.Internal
{

    public class ValidationConverter : IValueConverter
    {
        
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            
            var errors = value as ReadOnlyObservableCollection<ValidationError>;
            if (errors == null) return value;
            if (errors.Count > 0) return errors[0].ErrorContent;
            return String.Empty;            
        }


        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
