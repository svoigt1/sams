﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

using System.Windows.Media;


namespace Sams.Views.Internal
{

    public class DateConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value.ToString() == "")
            {
                return "";
            }
            else
            {
                return ((DateTime)value).ToShortDateString();
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return DateTime.Parse(value.ToString());
        }
    }
}