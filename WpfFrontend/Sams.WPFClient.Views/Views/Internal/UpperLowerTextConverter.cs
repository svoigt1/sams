﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;


namespace Sams.Views.Internal
{
    public class UpperLowerTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return string.Empty;
            if (parameter == null)
                return value;

            string mode = parameter.ToString().ToUpper();
            var _value  = value.ToString();

            switch(mode)
            {
                case "UPPER":
                    return _value.ToUpper();
                case "LOWER":
                    return _value.ToLower();
            }

            return _value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
