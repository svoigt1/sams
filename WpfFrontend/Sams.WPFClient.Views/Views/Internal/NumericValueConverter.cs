﻿using System;
using System.Linq;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Sams.Views.Internal
{
   

    public class NumericValueConverter : IValueConverter
    {
 
        private const String NullValue = "?";

        /// <summary>
        /// decimal to String 
        /// </summary>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            
            if (value == null) return NullValue;
            if (IsNullableType(value.GetType())) {

                var nv = (decimal?)value;
                if (!nv.HasValue) return NullValue;
                return Math.Round(nv.Value);
            }
            return Math.Round((decimal)value);
        }
 
        /// <summary>
        /// String to decimal
        /// </summary>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException("String to decimal");
        }


 		public static bool IsNullableType(Type type)
		{
			return type.IsGenericType && type.GetGenericTypeDefinition().Equals(typeof(Nullable));
		}   
    }
}
