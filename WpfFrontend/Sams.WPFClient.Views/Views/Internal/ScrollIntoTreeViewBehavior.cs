﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;


namespace Sams.Views.Internal
{
    
    /// <remarks>
    /// http://druss.co/2015/09/wpf-mvvm-treeview-scroll-to-selected-item/
    /// </remarks>
    public class ScrollIntoTreeViewBehavior
    {

        public static readonly DependencyProperty ScrollIntoTreeViewProperty = DependencyProperty.RegisterAttached(
            "ScrollIntoTreeView", typeof (bool), typeof(ScrollIntoTreeViewBehavior), new PropertyMetadata(default(bool), PropertyChangedCallback));


        public static void SetScrollIntoTreeView(DependencyObject element, bool value)
        {
            element.SetValue(ScrollIntoTreeViewProperty, value);
        }


        public static bool GetScrollIntoTreeView(DependencyObject element)
        {
            return (bool) element.GetValue(ScrollIntoTreeViewProperty);
        }


        private static void PropertyChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            
            var treeViewItem = dependencyObject as TreeViewItem;
            if (treeViewItem == null) return;

            if (!((bool) dependencyPropertyChangedEventArgs.OldValue) &&
                ((bool) dependencyPropertyChangedEventArgs.NewValue)) {

                treeViewItem.Unloaded += TreeViewItemOnUnloaded;
                treeViewItem.Selected += TreeViewItemOnSelected;
            }
        }


        private static void TreeViewItemOnUnloaded(object sender, RoutedEventArgs routedEventArgs)
        {
            
            var treeViewItem = sender as TreeViewItem;
            if (treeViewItem == null) return;

            treeViewItem.Unloaded -= TreeViewItemOnUnloaded;
            treeViewItem.Selected -= TreeViewItemOnSelected;
        }


        private static void TreeViewItemOnSelected(object sender, RoutedEventArgs routedEventArgs)
        {
           
            var treeViewItem = sender as TreeViewItem;
            if (treeViewItem == null) return;

            treeViewItem.BringIntoView();
        }
    }
}
