﻿using System.Diagnostics;
using System.Windows.Controls;
using System.Windows.Navigation;


namespace Sams.Views.Settings
{

    public partial class AboutView:UserControl ,IAboutView
    {

        public AboutView()
        {
            InitializeComponent();
        }


        private void RequestNavigateHandler(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(e.Uri.ToString());
        }
    }
}
