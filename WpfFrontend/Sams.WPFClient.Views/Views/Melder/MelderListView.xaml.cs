﻿using Sams.ViewModels.Melder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Sams.Views.Melder
{
    /// <summary>
    /// Interaction logic for MelderListView.xaml
    /// </summary>
    public partial class MelderListView : UserControl, IMelderListView
    {
        public MelderListView()
        {
            InitializeComponent();
        }

        private void DataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            Console.WriteLine("DataGrid_MouseUp");
            var vm = DataContext as MelderListViewModel;
            if (vm != null) vm.DetailCommand.Execute(sender);

        }

        private void DataGrid_ScrollIntoView(object sender, SelectedCellsChangedEventArgs e)
        {
            Console.WriteLine("DataGrid_ScrollIntoView");
            var grid = sender as DataGrid;
            if (grid != null && grid.SelectedItem != null)
            {
                Console.WriteLine("SCROLLLLL....");
                grid.ScrollIntoView(grid.SelectedItem, null);
            }

        }
        
    }
}
