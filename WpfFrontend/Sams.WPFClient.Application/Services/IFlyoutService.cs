﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;


namespace Sams.Services
{

    public interface IFlyoutService
    {

        ICommand YesCommand { get; }
        ICommand NoCommand { get; }

        Task<bool> IsOpenAsync();
        /// <summary>
        /// Flyout wird nicht blockirend geöffnet (wenn mehrere abfolgende Schritte präsentiert werden sollen)
        /// </summary>
        Task OpenAsync(FlyoutConfig config);
        /// <summary>
        /// Task wird blockiert bis Flyout geschlossen wird
        /// </summary>
        Task<bool> OpenConfirmAsync(FlyoutConfig view);
        Task CloseAsync(bool confirmed = false);
    }
}
