﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Sams.Controllers;


namespace Sams.Services
{

    public class ControllerService : IControllerService
    {

        private readonly IEnumerable<IController>   controllers;
        private readonly IConfigController          configController;
       
        public IConfigController ConfigController { get { return configController; } }


        public ControllerService(IEnumerable<IController> controllers)
        {

            this.controllers = controllers;
            configController = (IConfigController)controllers.First(p => p is IConfigController);
        }


        public void InitializeAll()
        {
            controllers.ForEach(p => p.Initialize());            
        }


        public void ShutdownAll()
        {
            controllers.ForEach(p => p.Shutdown());            
        }
    }
}
