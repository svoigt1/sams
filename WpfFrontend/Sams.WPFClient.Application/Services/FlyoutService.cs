﻿using System;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Sams.Views;


namespace Sams.Services
{
   
    public class FlyoutService : IFlyoutService
    {

        private readonly IMainView view;
        private bool confirmed;


        public ICommand YesCommand { get; private set; }
        public ICommand NoCommand  { get; private set; }


        public FlyoutService(IMainView view)
        {

            this.view = view;
            YesCommand = new AsyncDelegateCommand(_ => CloseAsync(true));
            NoCommand = new AsyncDelegateCommand(_ => CloseAsync(false));
        }

        
        public async Task OpenAsync(FlyoutConfig config)
        {
            
            if (await IsOpenAsync()) await CloseAsync();
            view.OpenFlyout(config);
        }
        

        public async Task<bool> OpenConfirmAsync(FlyoutConfig config)
        {
            
            if (await IsOpenAsync()) await CloseAsync();
            confirmed = false;
            await view.OpenFlyoutAsync(config);
            return confirmed;
        }


        public async Task CloseAsync(bool confirmed = false)
        {
            
            this.confirmed = confirmed;
            await view.CloseFlyoutAsync();
        }


        public async Task<bool> IsOpenAsync()
        {
            return await view.FlyoutIsOpenAsync();
        }
    }
}
