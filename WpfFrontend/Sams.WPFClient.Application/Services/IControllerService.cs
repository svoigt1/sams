﻿using System;
using Sams.Controllers;

namespace Sams.Services
{

    public interface IControllerService
    {
        
        IConfigController ConfigController { get; }
        void InitializeAll();
        void ShutdownAll();
    }
}
