﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sams.ViewModels;


namespace Sams.Services
{
    
    public class BreadcrumbService : IBreadcrumbService
    {

        private readonly ConcurrentStack<String> stack;
        private readonly MainViewModel view;
        private readonly ISettings settings;
        private String currentRoot;


        public BreadcrumbService(MainViewModel view, ISettings settings)
        {

            this.view = view;
            this.settings = settings;
            stack = new ConcurrentStack<String>();
            currentRoot = settings.ApplicationName;
            Clear();
        }


        public int Depth { get { return stack.Count; } } 


        public void Add(String pos)
        {
            
            stack.Push(pos);
            SetCrumb();
        }


        public void Remove()
        {
            
            String dummy;
            stack.TryPop(out dummy);
            SetCrumb();
        }

        
        public void Trim(int depth)
        {
            
            String dummy;
            while (stack.Count > depth) stack.TryPop(out dummy);
            SetCrumb();
        }


        public void SetLogin(String login)
        {
            
            if (String.IsNullOrEmpty(login)) {

                currentRoot = settings.ApplicationName;
            
            } else {
            
                currentRoot = String.Format("{0} :: {1}", settings.ApplicationName, login);
            }

            Clear();
        }


        public void Clear()
        {
            
            stack.Clear();
            Add(currentRoot);
            SetCrumb();
        }


        private void SetCrumb()
        { 
            view.Title = String.Join(" • ", stack.Reverse().ToArray()); 
        }
    }
}
