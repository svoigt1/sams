﻿using System;
namespace Sams.Services
{
    /// <summary>
    /// Titlebar zeigt an wo sich der User gerade befindet
    /// </summary>
    public interface IBreadcrumbService
    {
        void Add(string pos);
        void Clear();
        void SetLogin(String login);
        void Remove();
        void Trim(int depth);
        int Depth { get; }
    }
}
