﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Waf.Applications;
using MahApps.Metro.Controls.Dialogs;
using Sams.ViewModels.Services;
using Sams.Views;
using Sams.ViewData;
using Sams.ViewModels;
using System.Collections.Generic;


namespace Sams.Services
{

    public class MessageService : IMessageService
    {

        private readonly IMainView view;
        private readonly ILogger logger;
        private readonly IFlyoutService flyouts;
        private readonly DeleteViewModel deleteView;
        private readonly MessageViewModel messageView;
        private readonly ConfirmViewModel confirmView;


        public delegate void OverlayRaisedEventHandler(object sender, EventArgs e);
        public event EventHandler OverlayRaisedEvent;


        public MessageService(IMainView view, ILogger logger, IFlyoutService flyouts, 
                              DeleteViewModel deleteView, ConfirmViewModel confirmView, MessageViewModel messageView)
        {
            
            this.view = view;
            this.logger = logger;
            this.flyouts = flyouts;
            this.deleteView = deleteView;
            this.confirmView = confirmView;
            this.messageView = messageView;
            deleteView.ExecuteCommand = flyouts.YesCommand;
            confirmView.ExecuteCommand = flyouts.YesCommand;
        }


        public void Show(Exception x)
        {
            
            if (IsCanceledException(x)) {
            
                var place = x.StackTrace.Split().FirstOrDefault(p => p.StartsWith("Comain"));
                logger.InfoFormat("Current task cancelled. ({0})", place);
                return;
            }
            logger.Error(x.ToString());
            
            bool b = x is SamsOperationException || x is UserException;
            String msg = b? x.Message: x.UnwrapMessage();
            
            OnOverlayRaiseEvent(new EventArgs());
            view.ShowMessage("Ein Fehler ist aufgetreten.", msg);
        }


        public void Show(String body, String header = null)
        {
            
            OnOverlayRaiseEvent(new EventArgs());
            view.ShowMessage(header, body);
        }


        public Task ShowMessageAsync(MessageToken token)
        {

            messageView.SetContent(token);
            return flyouts.OpenConfirmAsync(new FlyoutConfig { ChildView = messageView.View });
        }


        public Task<bool> ConfirmAsync(MessageToken token)
        {

            confirmView.SetContent(token);
            return flyouts.OpenConfirmAsync(new FlyoutConfig { ChildView = confirmView.View });
        }


        public Task<bool> ConfirmDeleteAsync(DeleteToken token)
        {

            deleteView.SetContent(token);
            return flyouts.OpenConfirmAsync(new FlyoutConfig { ChildView = deleteView.View });
        }


        public async Task<bool> ConfirmExitAsync(string msg = null)
        {
            
            OnOverlayRaiseEvent(new EventArgs());
            return await view.ConfirmExitAsync(msg);
        }


        private bool IsCanceledException(Exception x)
        {

            if (x is OperationCanceledException) return true;
            if (x is TaskCanceledException) return true;
            if (x.InnerException == null) return false;
            return IsCanceledException(x.InnerException);
        }


        protected virtual void OnOverlayRaiseEvent(EventArgs e)
        {

            EventHandler handler = OverlayRaisedEvent;
            if (handler != null) handler(this, e);
        }
    }
}
