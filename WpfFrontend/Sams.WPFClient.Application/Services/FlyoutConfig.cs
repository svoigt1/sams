﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MahApps.Metro.Controls;


namespace Sams.Services
{
    
    public class FlyoutConfig
    {

        public FlyoutConfig()
        {

            IsModal = false;
            Position = MahApps.Metro.Controls.Position.Bottom;
            Theme = FlyoutTheme.Accent;
            IsPinned = false;   
        }


        public String       Header      { get; set; }
        public Position     Position    { get; set; }
        public FlyoutTheme  Theme       { get; set; }
        public object       ChildView   { get; set; }
        public bool         IsModal     { get; set; }
        public bool         IsPinned    { get; set; }
    }
}
