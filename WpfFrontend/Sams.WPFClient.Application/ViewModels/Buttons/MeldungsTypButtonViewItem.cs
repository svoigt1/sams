﻿using Sams.Models;
using Sams.Views;
using Sams.Views.Buttons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows;

namespace Sams.ViewModels.Buttons
{
    public class MeldungsTypButtonViewItem
    {
        private readonly Meldungstyp model;
  
        public Meldungstyp Model { get { return model; } }

        public int Id { get; set; }
        public String Name { get; set; }
        public String Farbe { get; set; }
        public bool IstAktiv { get; set; }

        public MeldungsTypButtonViewItem(Meldungstyp model) 
        {
            this.model = model;

            this.Id = model.Id;
            this.Name = model.Name.ToUpper();
            this.Farbe = model.Farbe;
            this.IstAktiv = model.IstAktiv;
        }

        public String BGColor {
            get {

                var color = "#FFFFFFFF";
                switch(Id)
                {
                    case 1: color = "#FFA62655"; break; //BEVOR
                    case 2: color = "#FFFDCA58"; break; //CIRS
                    case 3: color = "#FF8BBEA8"; break; //IDEE
                    case 4: color = "#FF004A78"; break; //KRITIK
                    case 5: color = "#FF000000"; break; //RISIKO
                    case 6: color = "#FF969696"; break; //SONSTIGES

                }
                return color;
            }
        }

        public Thickness BtnMargin
        {
            get
            {
                if (Id == 1) return new Thickness(0,0,10,0);
                if (Id == 6) return new Thickness(10, 0, 0, 0);

                return new Thickness(10,0,10,0);  
            }
        }

    }
    
}
