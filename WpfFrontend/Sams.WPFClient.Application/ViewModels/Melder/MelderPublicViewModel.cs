﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Waf.Applications;
using Sams.Views.Melder;

namespace Sams.ViewModels.Melder
{
    
    public class MelderPublicViewModel : ViewModel<IMelderPublicView>, IDelayableViewModel
    {

        public MelderPublicViewModel(IMelderPublicView view) : base(view)
        {
        }

        #region P R O P E R T I E S

        private object filterView;
        public object FilterView
        {
            get { return filterView; }
            set { SetProperty(ref filterView, value); }
        }


        private object listView;
        public object ListView
        {
            get { return listView; }
            set { SetProperty(ref listView, value); }
        }


        private object detailView;
        public object DetailView
        {
            get { return detailView; }
            set { SetProperty(ref detailView, value); }
        }

        private object legendeView;
        public object LegendeView
        {
            get { return legendeView; }
            set { SetProperty(ref legendeView, value); }
        }

        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }

        #endregion

    }
}
