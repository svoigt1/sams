﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Waf.Applications;
using Sams.Views.Melder;
using Sams.ViewModels.Meldungen;

namespace Sams.ViewModels.Melder
{
    
    public class MyListViewModel : ViewModel<IMyListView>, IDelayableViewModel
    {

        public MyListViewModel(IMyListView view) : base(view)
        {
        }

        #region P R O P E R T I E S

        private IEnumerable meldungen;
        public IEnumerable Meldungen
        {
            get { return meldungen; }
            set { SetProperty(ref meldungen, value); }
        }

        private MeldungViewItemBase selected;
        public MeldungViewItemBase Selected
        {
            get { return selected; }
            set
            {

                SetProperty(ref selected, value);
                Console.WriteLine(selected == null ? "leer" : "+++"+selected.Id.ToString());

            }
        }

        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }

        #endregion

    }
}
