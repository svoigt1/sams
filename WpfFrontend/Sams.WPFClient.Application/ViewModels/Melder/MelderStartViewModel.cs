﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Sams.Views.Melder;
using Sams.Models;
using Sams.ViewModels.Buttons;

namespace Sams.ViewModels.Melder
{
    
    public class MelderStartViewModel : ViewModel<IMelderStartView>
    {

        public MelderStartViewModel(IMelderStartView view) : base(view)
        {
        }


        #region P R O P E R T I E S
        private IList<MeldungsTypButtonViewItem> btnCollection;
        public IList<MeldungsTypButtonViewItem> BtnCollection
        {
            get { return btnCollection; }
            set { if (SetProperty(ref btnCollection, value)) RaisePropertyChanged("NzBtns"); }
        }

        //private int nzBtns;
        public int NzBtns
        {
            get { return btnCollection == null ? 0 : btnCollection.Count; }
        }


        private object currentView;
        public object CurrentView
        { 
            get { return currentView; }
            set { SetProperty(ref currentView, value); }
        }


        private object myView;
        public object MyView
        { 
            get { return myView; }
            set { SetProperty(ref myView, value); }
        }

        private object legendeView;
        public object LegendeView
        {
            get { return legendeView; }
            set { SetProperty(ref legendeView, value); }
        }


        private ICommand createCommand;
        public ICommand CreateCommand
        { 
            get { return createCommand; }
            set { SetProperty(ref createCommand, value); }
        }


#endregion

    }
}
