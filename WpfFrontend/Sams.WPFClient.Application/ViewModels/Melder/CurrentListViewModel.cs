﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Sams.Models;
using Sams.Views.Melder;
using Sams.ViewModels.Meldungen;

namespace Sams.ViewModels.Melder
{
    
    public class CurrentListViewModel : ViewModel<ICurrentListView>, IDelayableViewModel
    {

        public CurrentListViewModel(ICurrentListView view) : base(view)
        {
        }


#region P R O P E R T I E S


        private IEnumerable topMeldungen;
        public IEnumerable TopMeldungen
        { 
            get { return topMeldungen; }
            set { SetProperty(ref topMeldungen, value); }
        }


        private ICommand detailCommand;
        public ICommand DetailCommand
        { 
            get { return detailCommand; }
            set {
                Console.WriteLine("Set DetailCommant in CurrentListViewModel");
                SetProperty(ref detailCommand, value); }
        }


        private MeldungViewItemBase selected;
        public MeldungViewItemBase Selected
        { 
            get { return selected; }
            set { 
            
                SetProperty(ref selected, value);
                Debug.WriteLine(selected == null? "leer": selected.Id.ToString());
            
            }
        }

        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }

        #endregion

    }
}
