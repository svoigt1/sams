﻿using System.Diagnostics;

using System.Windows.Input;

using System.Collections;
using System.Waf.Applications;

using Sams.Views.Melder;
using Sams.ViewModels.Meldungen;
using System.Collections.Generic;

namespace Sams.ViewModels.Melder
{
    public class MelderListViewModel : ViewModel<IMelderListView>, IDelayableViewModel
    {
        public MelderListViewModel(IMelderListView view) : base(view)
        {
        }

#region  P R O P E R T I E S

        private ICommand detailCommand;
        public ICommand DetailCommand
        {
            get { return detailCommand; }
            set { SetProperty(ref detailCommand, value); }
        }

        private IList<MeldungViewItemBase> meldungenList;
        public IList<MeldungViewItemBase> MeldungenList
        {
            get { return meldungenList; }
            set { SetProperty(ref meldungenList, value); }
        }

        private MeldungViewItemBase current;
        public MeldungViewItemBase Current
        {
            get { return current; }
            set
            {

                SetProperty(ref current, value);
                Debug.WriteLine(current == null ? "leer" : current.Id.ToString());

            }
        }

        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }

#endregion

    }
}
