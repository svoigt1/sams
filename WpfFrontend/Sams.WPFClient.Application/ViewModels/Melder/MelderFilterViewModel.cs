﻿using System.Waf.Applications;
using Sams.Views.Melder;
using System;
using System.Collections.Generic;
using System.Threading;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;

namespace Sams.ViewModels.Melder
{
    public class MelderFilterViewModel : ViewModel<IMelderFilterView>, INotifyPropertyChanged
    {
        public class Ownership
        {
            public String Name { get; set; }
            public String FilterTerm { get; set; }
            public Boolean IsSelected { get; set; }
        }
        public List<Ownership> OwnerFilterList { get; private set; }
            
        private readonly SynchronizationContext context;
        public MelderFilterViewModel(IMelderFilterView view) : base(view)
        {
            context = SynchronizationContext.Current;

            OwnerFilterList = new List<Ownership>
            {
                new Ownership {Name = "Meine", FilterTerm = "private" },
                new Ownership {Name = "Öffentliche", FilterTerm = "all" },
            };

            SelectedOwnershipButton = OwnerFilterList.First();
        }

        public void setFilterOptions(string filterTerm)
        {
            SelectedOwnershipButton = OwnerFilterList.SingleOrDefault(p => p.FilterTerm == filterTerm);
        }

#region P R O P E R T I E S

        private Ownership selectedOwnershipButton;

        public Ownership SelectedOwnershipButton
        {
            get { return selectedOwnershipButton; }

            set
            {
                Console.WriteLine("Set SelectedOwnershipButton:" + value.Name);
                selectedOwnershipButton = value;
                RaisePropertyChanged("SelectedOwnershipButton");
            }
        }


        // Commands

        private ICommand searchCommand;
        public ICommand SearchCommand
        {
            get { return searchCommand; }
            set { SetProperty(ref searchCommand, value); }
        }

        private ICommand resetCommand;
        public ICommand ResetCommand
        {
            get { return resetCommand; }
            set { SetProperty(ref resetCommand, value); }
        }

        #endregion

    }
}
