﻿using Sams.Models;
using Sams.ViewModels.Meldungen;
using Sams.Views.Melder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows;
using System.Windows.Input;

namespace Sams.ViewModels.Melder
{
    public class MelderCreateViewModel : ValidatableViewModel<IMelderCreateView, Meldung>, IDelayableViewModel
    {

        public MelderCreateViewModel(IMelderCreateView view) : base(view)
        {
            Current = new Meldung();
        }

        public void PresetMeldungsTyp(int m_Id)
        {
            Meldungstyp = this.MeldungentypList.SingleOrDefault(p => p.Id == m_Id);
        }

        public bool CanSave
        {
            get { return Current.Error == String.Empty ? true : false; }
        }

        public void ResetForm()
        {
            Current = new Meldung();
            Einrichtung1 = null;
            updateView();
        }

        public void updateView()
        {
            RaisePropertyChanged("CanSave");
        }

        #region P R O P E R T I E S

        private bool istCurrentChanged;
        public bool IstCurrentChanged
        {
            get { return istCurrentChanged; }
            set { SetProperty(ref istCurrentChanged, value); }
        }

        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }

        #region M E L D U N G
   
        public bool IstNichtAnonym
        {
            get { return IstAnonym == true ? false : true; }
        }
        public bool IstAnonym
        {
            get { return Current.IstAnonym; }
            set {
                Current.IstAnonym = value;
                IstCurrentChanged = true;
                
                RaisePropertyChanged("IstAnonym");
                RaisePropertyChanged("IstNichtAnonym");
                RaisePropertyChanged("Meldername");
                updateView();
            }
        }

        private String meldername;
        public String Meldername
        {
            get { return Current.IstAnonym ? "anonym" : meldername; }
            set
            {
                if (Current != null && value != null)
                {
                    SetProperty(ref meldername, value);
                    Current.Meldername = value;
                    updateView();
                }
            }
        }

        public String Betreff
        {

            get { return Current?.Betreff; }
            set
            {
                if (Current != null && Current.Betreff != value)
                {
                    Current.Betreff = value;
                    updateView();
                }
            }
        }

        public String Kurzbeschreibung
        {

            get { return Current?.Kurzbeschreibung; }
            set
            {
                if (Current != null && Current.Kurzbeschreibung != value)
                {
                    Current.Kurzbeschreibung = value;
                    updateView();
                }
            }
        }

        #endregion

        #region M E L D U N G S T Y P E N

        private MeldungstypViewItem meldungstyp;
        public MeldungstypViewItem Meldungstyp
        {
            get { return meldungstyp; }
            set
            {
                if (Current != null && value != null)
                {
                    SetProperty(ref meldungstyp, value);
                    Current.Meldungstyp = value.model;
                    Current.Meldungstyp_Id = value.model.Id;
            
                    RaisePropertyChanged("Meldungstyp");
                    updateView();
                }
            }
        }

        private List<MeldungstypViewItem> meldungtypList;
        public List<MeldungstypViewItem> MeldungentypList
        {
            get { return meldungtypList; }
            set { SetProperty(ref meldungtypList, value); }
        }

        #endregion

        #region E I N R I C H T U N G E N

        //private int einrichtung1_Id;
        //public int Einrichtung1_Id
        //{
        //    get { return Current == null ? 0 : Current.Einrichtung1_Id; }
        //    set { SetProperty(ref einrichtung1_Id, value); Current.Einrichtung1_Id = value; IstCurrentChanged = true;}
        //}

      
        public int Einrichtung1_Id
        {
            get { return Current.Einrichtung1_Id; }
            set
            {
                Console.WriteLine("Set Einrichtung1_Id:" + value.ToString());
                if (Current != null && Current.Einrichtung1_Id != value)
                {
                    Console.WriteLine("Set Einrichtung1_Id -->");

                    Current.Einrichtung1_Id = value;
                    RaisePropertyChanged("Einrichtung1_Id");
                    updateView();
                }
                else
                {
                    Console.WriteLine("Set NOT Einrichtung1_Id");
                }
            }
        }

        private int einrichtung2_Id;
        public int Einrichtung2_Id
        {
            get { return Current == null ? 0 : Current.Einrichtung2_Id; }
            set { SetProperty(ref einrichtung2_Id, value); Current.Einrichtung2_Id = value; updateView(); }
        }

        private int? einrichtung3_Id;
        public int? Einrichtung3_Id
        {
            get { return Current == null ? null : Current.Einrichtung3_Id; }
            set { SetProperty(ref einrichtung3_Id, value); Current.Einrichtung3_Id = value; }
        }

        private int? einrichtung4_Id;
        public int? Einrichtung4_Id
        {
            get { return Current == null ? null : Current.Einrichtung4_Id; }
            set { SetProperty(ref einrichtung4_Id, value); Current.Einrichtung4_Id = value; }
        }
                
        // lists
        private List<Einrichtung1> einrichtungen1;
        public List<Einrichtung1> Einrichtungen1
        {
            get { return einrichtungen1; }
            set { SetProperty(ref einrichtungen1, value); }
        }

        private List<Einrichtung2> einrichtungen2;
        public List<Einrichtung2> Einrichtungen2
        {
            get { return einrichtungen2; }
            set { SetProperty(ref einrichtungen2, value); }
        }

        private List<Einrichtung3> einrichtungen3;
        public List<Einrichtung3> Einrichtungen3
        {
            get { return einrichtungen3; }
            set { SetProperty(ref einrichtungen3, value); }
        }

        private List<Einrichtung4> einrichtungen4;
        public List<Einrichtung4> Einrichtungen4
        {
            get { return einrichtungen4; }
            set { SetProperty(ref einrichtungen4, value); }
        }

        // singles 

        private Einrichtung1 einrichtung1;
        public Einrichtung1 Einrichtung1
        {
            get { return einrichtung1; }
            set
            {
                SetProperty(ref einrichtung1, value);

                Einrichtungen2 = (einrichtung1 != null && einrichtung1.Children != null) ? einrichtung1.Children.ToList<Einrichtung2>() : null;

                //Console.WriteLine("Set Einrichtung1 ID:"+einrichtung1.Id);
                Einrichtung1_Id = einrichtung1 != null ? einrichtung1.Id : 0;
                RaisePropertyChanged("Einrichtung1_Id");
                updateView();
            }
        }

        private Einrichtung2 einrichtung2;
        public Einrichtung2 Einrichtung2
        {
            get { return einrichtung2; }
            set
            {
                SetProperty(ref einrichtung2, value);
                Einrichtungen3 = einrichtung2 != null ? einrichtung2.Children.ToList<Einrichtung3>() : null;

                Einrichtung2_Id = einrichtung2 != null ? einrichtung2.Id : 0;
                RaisePropertyChanged("Einrichtung2_Id");
                updateView();
            }
        }

        private Einrichtung3 einrichtung3;
        public Einrichtung3 Einrichtung3
        {
            get { return einrichtung3; }
            set
            {
                SetProperty(ref einrichtung3, value);
                Einrichtungen4 = einrichtung3 != null ? einrichtung3.Children.ToList<Einrichtung4>() : null;

                Einrichtung3_Id = einrichtung3 != null ? einrichtung3.Id : 0;
                RaisePropertyChanged("Einrichtung3_Id");
            }
        }

        private Einrichtung4 einrichtung4;
        public Einrichtung4 Einrichtung4
        {
            get { return einrichtung4; }
            set
            {
                SetProperty(ref einrichtung4, value);

                Einrichtung4_Id = einrichtung4 != null ? einrichtung4.Id : 0;
                RaisePropertyChanged("Einrichtung4_Id");
            }
        }
        #endregion

        #region C O M M A N D S

        private ICommand saveCommand;
        public ICommand SaveCommand
        {
            get { return saveCommand; }
            set { SetProperty(ref saveCommand, value); }
        }

        #endregion

        #endregion
    }
}
