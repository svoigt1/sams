﻿using Sams.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sams.ViewModels.Meldungen
{
    public class MeldungExtendedViewItem : MeldungViewItemBase
    {
        /// <summary>
        /// ist meldung eigene "private" oder öffentlich "all" 
        /// </summary>
        public string ownership { get; }
        public MeldungExtendedViewItem(Meldung model, string ownership) : base(model)
        {
            this.ownership = ownership;
        }
    }
}
