﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Sams.Models;
using Sams.Views.Melder;
using Sams.Views.Meldung;

namespace Sams.ViewModels.Meldungen
{
    public class MeldungDetailViewModel : ValidatableViewModel<IMeldungDetailView, Meldung>, IDelayableViewModel
    {
        public MeldungDetailViewModel(IMeldungDetailView view) : base(view)
        {
        }

        #region P R O P E R T I E S

        private MeldungViewItemBase meldung;
        public MeldungViewItemBase Meldung
        {
            get { return meldung; }
            set { SetProperty(ref meldung, value); }
        }

        private ICommand backCommand;
        public ICommand BackCommand
        {
            get { return backCommand; }
            set { SetProperty(ref backCommand, value); }
        }

        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }

        #endregion
    }
}

