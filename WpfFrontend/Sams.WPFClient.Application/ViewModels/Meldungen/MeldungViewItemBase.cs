﻿using Sams.Models;
using Sams.ViewModels.Colors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Sams.ViewModels.Meldungen
{
    public class MeldungViewItemBase
    {
        private readonly Meldung model;


        public int Id { get; set; }

        public Meldungstyp Meldungstyp { get; set; }
        public int Meldungstyp_Id { get; set; }
        public DateTime Erstellt { get; set; }
        public String   Nummer { get; set; }
        public String Betreff { get; set; }
        public String Kurzbeschreibung { get; set; }
        public int Status { get; set; }

        public String IconName { get; set; }
        public System.Windows.Media.SolidColorBrush IconBrush { get; set; }  


        public Meldung Model { get { return model;}}

        public MeldungViewItemBase(Meldung model)
        {
            this.model            = model;

            this.Id               = model.Id;
            this.Status           = model.Status;
            this.Nummer           = (String.IsNullOrEmpty(model.Nummer)) ? String.Empty : model.Nummer.PadLeft(5, '0');
            this.Erstellt         = model.Erstellt;

            this.Meldungstyp      = model.Meldungstyp;
            this.Meldungstyp_Id   = model.Meldungstyp_Id;

            this.IconName         = model.Meldungstyp.Name.Cut(2, false).ToUpper();
            this.IconBrush        = SamsColors.getBrushByTypeId(model.Meldungstyp.Id);

            this.Betreff          = model.Betreff.Cut(120);
            this.Kurzbeschreibung = model.Kurzbeschreibung.RemoveLineBreaks().Cut(120, true, "...");

        }

        //Neu = 0,
        //Bearbeitung = 1,
        //Wartend = 2,
        //Abgeschlossen = 3
        public string StatusIcon
        {
            get {
                //Console.WriteLine("StatusIcon get:" + Status);
                var path = @"pack://application:,,,/Sams.WpfClient.Views;component/Resources/status-neu.png";
                switch (Status)
                {
                    case 0: path = @"pack://application:,,,/Sams.WpfClient.Views;component/Resources/status-neu.png"; break;
                    case 1: path = @"pack://application:,,,/Sams.WpfClient.Views;component/Resources/status-in-bearbeitung.png"; break;
                    case 2: path = @"pack://application:,,,/Sams.WpfClient.Views;component/Resources/status-wartend.png"; break;
                    case 3: path = @"pack://application:,,,/Sams.WpfClient.Views;component/Resources/status-abgeschlossen.png"; break;
                }

                return path;
            }
               
        }

        public string StatusText
        {
            get
            {
                //Console.WriteLine("StatusIcon get:" + Status);
                var text = "nicht bekannt";
                switch (Status)
                {
                    case 0: text = "Neu"; break;
                    case 1: text = "Bearbeitung"; break;
                    case 2: text = "Wartend"; break;
                    case 3: text = "Abgeschlossen"; break;
                }

                return text;
            }

        }
    }
}