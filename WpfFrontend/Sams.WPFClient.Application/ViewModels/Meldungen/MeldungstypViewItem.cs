﻿using Sams.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sams.ViewModels.Meldungen
{
    public class MeldungstypViewItem
    {
        public Meldungstyp model { get; }
        public int Id { get; set; }
        public String Name { get; set; }
        public bool IsSelected { get; set; }


        public MeldungstypViewItem(Meldungstyp model)
        {
            this.model = model;

            this.Id = model.Id;
            this.Name = model.Name;
            this.IsSelected = false;
        }
    }
}
