﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Waf.Applications;
using System.Windows.Input;


namespace Sams.ViewModels
{

    /// <summary>
    /// ViewModel ohne eigene View (Items in Listen etc)
    /// </summary>
    public class ValidatableViewItem<TModel> : NotifyBase, IDataErrorInfo where TModel : INotifyPropertyChanged, IDataErrorInfo
    {

        public TModel Model { get; private set; }


        public ValidatableViewItem(TModel model) 
        {
            
            Model = model;
            PropertyChangedEventManager.AddHandler(model, ModelPropertyChanged, "");
        }
       

        private void ModelPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            RaisePropertyChanged(args.PropertyName);
        }
    

        public virtual String Error
        {
            
            get { 
            
                if (Model == null) return String.Empty;
                return Model.Error;                
            }
        }


        public virtual String this[string columnName]
        {
            
            get { 
            
                if (Model == null) return String.Empty;
                return Model[columnName];                
            }
        }
    }
}
