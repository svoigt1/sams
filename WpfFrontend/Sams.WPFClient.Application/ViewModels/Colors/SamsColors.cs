﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Sams.ViewModels.Colors
{
    public static class SamsColors
    {
        static System.Windows.Media.SolidColorBrush bevorBrush { get { return (SolidColorBrush)(new BrushConverter().ConvertFrom("#a12257")); } }
        static System.Windows.Media.SolidColorBrush cirsBrush { get { return (SolidColorBrush)(new BrushConverter().ConvertFrom("#f7cd5e")); } }
        static System.Windows.Media.SolidColorBrush IdeeBrush { get { return (SolidColorBrush)(new BrushConverter().ConvertFrom("#76b294")); } }
        static System.Windows.Media.SolidColorBrush kritikBrush { get { return (SolidColorBrush)(new BrushConverter().ConvertFrom("#1b4777")); } }
        static System.Windows.Media.SolidColorBrush sontigesBrush { get { return (SolidColorBrush)(new BrushConverter().ConvertFrom("#949494")); } }

        public static System.Windows.Media.SolidColorBrush getBrushByTypeId(int id)
        {
            switch(id)
            {
                case 1:
                    return SamsColors.bevorBrush;
                case 2:
                    return SamsColors.cirsBrush;
                case 3:
                    return SamsColors.IdeeBrush;
                case 4:
                    return SamsColors.kritikBrush;
                case 5:
                    return SamsColors.sontigesBrush;
            }

            return SamsColors.sontigesBrush;
        }
    }
}
