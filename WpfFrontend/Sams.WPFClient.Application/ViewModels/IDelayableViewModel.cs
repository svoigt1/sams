﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Sams.ViewModels
{
   
    public interface IDelayableViewModel
    {

        bool    ShowDelay   { set; }
        object  View        { get; }
    }


    public class VisualDelay : IDisposable
    {


        private readonly IDelayableViewModel vm;


        public VisualDelay(IDelayableViewModel vm)
        {

            this.vm = vm;
            vm.ShowDelay = true;
        }
        

        private bool disposed = false;


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {

            if (!this.disposed) {

                if (disposing) {

                    vm.ShowDelay = false;
                }

                disposed = true;
            }
        }

        ~VisualDelay()
        {
            Dispose(false);
        }
    }
}
