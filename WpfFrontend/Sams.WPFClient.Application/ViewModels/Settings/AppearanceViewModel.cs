﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Waf.Applications;
using System.Windows.Media;
using MahApps.Metro;
using Sams.Views.Settings;


namespace Sams.ViewModels.Settings
{
   
    public class AppearanceViewModel : ViewModel<IAppearanceView>
    {

        private readonly List<Tuple<Color, Accent>> accents;
        private readonly ISettings setting;


        public AppearanceViewModel(IAppearanceView view, ISettings setting) : base(view)
        {

            accents = new List<Tuple<Color, Accent>>();
            accents.Add(new Tuple<Color, Accent>(Color.FromRgb(0xe5, 0x14, 0x00), ThemeManager.Accents.First(p => p.Name == "Red")));   
            accents.Add(new Tuple<Color, Accent>(Color.FromRgb(0x60, 0xa9, 0x17), ThemeManager.Accents.First(p => p.Name == "Green")));   
            accents.Add(new Tuple<Color, Accent>(Color.FromRgb(0x00, 0x00, 0xff), ThemeManager.Accents.First(p => p.Name == "Blue")));
            accents.Add(new Tuple<Color, Accent>(Color.FromRgb(0x80, 0x00, 0x80), ThemeManager.Accents.First(p => p.Name == "Purple")));
            accents.Add(new Tuple<Color, Accent>(Color.FromRgb(0xfa, 0x68, 0x00), ThemeManager.Accents.First(p => p.Name == "Orange")));
            accents.Add(new Tuple<Color, Accent>(Color.FromRgb(0xa4, 0xc4, 0x00), ThemeManager.Accents.First(p => p.Name == "Lime")));   
            accents.Add(new Tuple<Color, Accent>(Color.FromRgb(0x00, 0x8a, 0x00), ThemeManager.Accents.First(p => p.Name == "Emerald")));   
            accents.Add(new Tuple<Color, Accent>(Color.FromRgb(0x00, 0xab, 0xa9), ThemeManager.Accents.First(p => p.Name == "Teal")));   
            accents.Add(new Tuple<Color, Accent>(Color.FromRgb(0x1b, 0xa1, 0xe2), ThemeManager.Accents.First(p => p.Name == "Cyan")));   
            accents.Add(new Tuple<Color, Accent>(Color.FromRgb(0x00, 0x50, 0xef), ThemeManager.Accents.First(p => p.Name == "Cobalt")));   
            accents.Add(new Tuple<Color, Accent>(Color.FromRgb(0x6a, 0x00, 0xff), ThemeManager.Accents.First(p => p.Name == "Indigo")));   
            accents.Add(new Tuple<Color, Accent>(Color.FromRgb(0xaa, 0x00, 0xff), ThemeManager.Accents.First(p => p.Name == "Violet")));   
            accents.Add(new Tuple<Color, Accent>(Color.FromRgb(0xf4, 0x72, 0xd0), ThemeManager.Accents.First(p => p.Name == "Pink")));   
            accents.Add(new Tuple<Color, Accent>(Color.FromRgb(0xd8, 0x00, 0x73), ThemeManager.Accents.First(p => p.Name == "Magenta")));   
            accents.Add(new Tuple<Color, Accent>(Color.FromRgb(0xa2, 0x00, 0x25), ThemeManager.Accents.First(p => p.Name == "Crimson")));   
            accents.Add(new Tuple<Color, Accent>(Color.FromRgb(0xf0, 0xa3, 0x0a), ThemeManager.Accents.First(p => p.Name == "Amber")));   
            accents.Add(new Tuple<Color, Accent>(Color.FromRgb(0xe3, 0xc8, 0x00), ThemeManager.Accents.First(p => p.Name == "Yellow")));   
            accents.Add(new Tuple<Color, Accent>(Color.FromRgb(0x82, 0x5a, 0x2c), ThemeManager.Accents.First(p => p.Name == "Brown")));   
            accents.Add(new Tuple<Color, Accent>(Color.FromRgb(0x6d, 0x87, 0x64), ThemeManager.Accents.First(p => p.Name == "Olive")));   
            accents.Add(new Tuple<Color, Accent>(Color.FromRgb(0x64, 0x76, 0x87), ThemeManager.Accents.First(p => p.Name == "Steel")));   
            accents.Add(new Tuple<Color, Accent>(Color.FromRgb(0x76, 0x60, 0x8a), ThemeManager.Accents.First(p => p.Name == "Mauve")));   
            accents.Add(new Tuple<Color, Accent>(Color.FromRgb(0xa0, 0x52, 0x2d), ThemeManager.Accents.First(p => p.Name == "Sienna"))); 

            this.setting = setting;
            var accent = accents.FirstOrDefault(p => p.Item2.Name == setting.Accent);
            if (accent != null) selectedAccentColor = accent.Item1;
            else selectedAccentColor = Color.FromRgb(0x00, 0x50, 0xef);
             
            SetTheme(setting.Theme); 
        }


        public Color[] AccentColors
        {
            get { return accents.Select(p => p.Item1).ToArray(); }
        }


        private Color selectedAccentColor;
        public Color SelectedAccentColor
        { 
            get { return selectedAccentColor; }
            set { if (SetProperty(ref selectedAccentColor, value)) SetTheme(); }
        }


        private AppTheme currentTheme;

        public bool UseDarkTheme
        {
         
            get { return currentTheme != null && currentTheme.Name == "BaseDark"; }
            set { 
            
                SetTheme(value ? "BaseDark": "BaseLight");
            }
        }


        private void SetTheme(String name)
        {

            currentTheme = ThemeManager.AppThemes.First(x => x.Name == name);
            SetTheme();
        }


        private void SetTheme()
        {

            if (SelectedAccentColor == Color.FromArgb(0, 0, 0, 0)) return;
            var currentAccent = accents.First(p => p.Item1 == SelectedAccentColor).Item2;
            ThemeManager.ChangeAppStyle(System.Windows.Application.Current, currentAccent, currentTheme); 

            setting.Accent = currentAccent.Name;
            setting.Theme = currentTheme.Name;
            setting.Save();
        }
    }
}
