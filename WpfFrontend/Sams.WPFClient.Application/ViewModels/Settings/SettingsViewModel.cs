﻿using System.Waf.Applications;
using Sams.Views;
using Sams.Views.Settings;


namespace Sams.ViewModels.Settings
{
   
    public class SettingsViewModel : ViewModel<ISettingsView>
    {


        public SettingsViewModel(ISettingsView view, AboutViewModel aboutView, AppearanceViewModel appearanceView) : base(view)
        {

            AboutView = aboutView.View;
            AppearanceView = appearanceView.View;
        }


#region P R O P E R T I E S
   
       
        public object AppearanceView { get; private set; }
        public object AboutView      { get; private set; }
   

#endregion


    }
}
