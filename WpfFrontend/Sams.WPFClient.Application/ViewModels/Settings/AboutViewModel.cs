﻿using System;
using System.Waf.Applications;
using Sams.Views.Settings;


namespace Sams.ViewModels.Settings
{
   
    public class AboutViewModel : ViewModel<IAboutView>
    {

        public AboutViewModel(IAboutView view, ISettings settings) : base(view)
        {
            Produkt = "Sams";
        }


#region P R O P E R T I E S
   
       
        public String Version
        { 
            
            get { 
            
                var version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
                return String.Format("{0}.{1}.{2}", version.Major, version.Minor, version.Build);
            }
        }  
   
       
        public String Produkt { get; private set; }


#endregion


    }
}
