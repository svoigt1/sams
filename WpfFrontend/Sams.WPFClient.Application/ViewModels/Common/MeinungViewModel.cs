﻿using Sams.Views.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;

namespace Sams.ViewModels.Common
{
    public class MeinungViewModel : ViewModel<IMeinungView>
    {
        public MeinungViewModel(IMeinungView view) : base(view)
        {
        }

        public void ResetForm()
        {
            this.Betreff = "";
            this.Body = "";
        }

        private String betreff;
        public String Betreff
        {
            get { return betreff; }
            set { SetProperty(ref betreff, value); }
        }

        private String body;
        public String Body
        {
            get { return body; }
            set { SetProperty(ref body, value); }
        }

        private ICommand sendCommand;
        public ICommand SendCommand
        {
            get { return sendCommand; }
            set { SetProperty(ref sendCommand, value); }
        }

        private ICommand cancelCommand;
        public ICommand CancelCommand
        {
            get { return cancelCommand; }
            set { SetProperty(ref cancelCommand, value); }
        }
    }
}
