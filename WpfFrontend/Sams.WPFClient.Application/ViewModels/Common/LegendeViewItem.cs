﻿using Sams.Models;
using Sams.ViewModels.Colors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sams.ViewModels.Common
{
    public class LegendeViewItem
    {
        private readonly Meldungstyp model;

        public int Id { get; set; }
        public String Name { get; set; }

        public System.Windows.Media.SolidColorBrush IconBrush { get; set; }

        public LegendeViewItem(Meldungstyp model)
        {
            this.model = model;

            this.Id = model.Id;
            this.Name = model.Name.ToUpper();

            this.IconBrush = SamsColors.getBrushByTypeId(model.Id);

        }

    }
}
