﻿using Sams.Models;
using Sams.Views.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Waf.Applications;

namespace Sams.ViewModels.Common
{
    public class LegendeViewModel : ViewModel<ILegendeView>
    {
        public LegendeViewModel(ILegendeView view) : base(view)
        {
        }

        private List<LegendeViewItem> meldungtypList;
        public List<LegendeViewItem> MeldungentypList
        {
            get { return meldungtypList; }
            set { SetProperty(ref meldungtypList, value); }
        }
    }
}
