﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Waf.Applications;
using System.Windows.Input;
using Sams.Views;
using Sams.Commands;

namespace Sams.ViewModels
{
   
    public class HomeViewModel : ViewModel<IHomeView>
    {

        public HomeViewModel(IHomeView view) : base(view)
        {
        }

        public void SetActionAktive(String commandParameter)
        {
           Actions.ForEach(p => p.IstAktive = (p.Parameter == commandParameter));
        }


        #region P R O P E R T I E S
        private String testversion;
        public String Testversion
        { 
            get { return testversion; }
            set { SetProperty(ref testversion, value); }
        }


        private String userName;
        public String UserName
        { 
            get { return userName; }
            set { SetProperty(ref userName, value); }
        }


        private String searchText;
        public String SearchText
        { 
            get { return searchText; }
            set { SetProperty(ref searchText, value); }
        }
 

        private ICommand searchCommand;
        public ICommand SearchCommand
        {
            get { return searchCommand; }
            set { SetProperty(ref searchCommand, value); }
        }

        /*
         * private ICommand textBoxButtonCmdWithParameter;

        public ICommand TextBoxButtonCmdWithParameter
        {
            get
            {
                return this.textBoxButtonCmdWithParameter ?? (this.textBoxButtonCmdWithParameter = new SimpleCommand
                {
                    CanExecuteDelegate = x => true,
                    ExecuteDelegate = async x =>
                    {
                        if (x is String)
                        {
                            await ((MetroWindow) Application.Current.MainWindow).ShowMessageAsync("TextBox Button with parameter was clicked!",
                                                                                                  string.Format("Parameter: {0}", x));
                        }
                    }
                });
            }
        }
        */


        private ICommand meinungCommand;
        public ICommand MeinungCommand
        { 
            get { return meinungCommand; }
            set { SetProperty(ref meinungCommand, value); }
        }


        private ICommand kontaktQSCommand;
        public ICommand KontaktQSCommand
        { 
            get { return kontaktQSCommand; }
            set { SetProperty(ref kontaktQSCommand, value); }
        }


        private ICommand kontaktGFCommand;
        public ICommand KontaktGFCommand
        { 
            get { return kontaktGFCommand; }
            set { SetProperty(ref kontaktGFCommand, value); }
        }


        private IEnumerable<IControllerCommand> actions;
        public IEnumerable<IControllerCommand> Actions
        { 
            get { return actions; }
            set { SetProperty(ref actions, value); }
        }


        private ICommand actionCommand;
        public ICommand ActionCommand
        { 
            get { return actionCommand; }
            set { SetProperty(ref actionCommand, value); }
        }


        private object pageView;
        public object PageView
        { 
            get { return pageView; }
            set { SetProperty(ref pageView, value); }
        }


#endregion

    }
}
