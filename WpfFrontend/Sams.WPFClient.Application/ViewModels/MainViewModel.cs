﻿using System;
using System.Collections;
using System.Waf.Applications;
using System.Windows.Input;
using Sams.Views;


namespace Sams.ViewModels
{
   
    public class MainViewModel : ViewModel<IMainView>
    {

        public MainViewModel(IMainView view) : base(view)
        {
        }


        public void Show()
        {
            ViewCore.Show();
        }


#region P R O P E R T I E S


        private ICommand cancelCommand;
        public ICommand CancelCommand
        { 
            get { return cancelCommand; }
            set { SetProperty(ref cancelCommand, value); }
        }


        private ICommand homeCommand;
        public ICommand HomeCommand
        { 
            get { return homeCommand; }
            set { SetProperty(ref homeCommand, value); }
        }
   

        private ICommand configCommand;
        public ICommand ConfigCommand
        { 
            get { return configCommand; }
            set { SetProperty(ref configCommand, value); }
        }
   

        private ICommand loginCommand;
        public ICommand LoginCommand
        { 
            get { return loginCommand; }
            set { SetProperty(ref loginCommand, value); }
        }
   

        private ICommand helpCommand;
        public ICommand HelpCommand
        { 
            get { return helpCommand; }
            set { SetProperty(ref helpCommand, value); }
        }


        private ICommand exitCommand;
        public ICommand ExitCommand
        { 
            get { return exitCommand; }
            set { SetProperty(ref exitCommand, value); }
        }


        private ICommand actionCommand;
        public ICommand ActionCommand
        { 
            get { return actionCommand; }
            set { SetProperty(ref actionCommand, value); }
        }


        private bool isFlyoutOpen;
        public bool IsFlyoutOpen
        { 
            get { return isFlyoutOpen; }
            set { SetProperty(ref isFlyoutOpen, value); }
        }
               

        private object pageView;
        public object PageView
        { 
            get { return pageView; }
            set { if (SetProperty(ref pageView, value)) RaisePropertyChanged("ReportCommandEnabled"); }
        }


        private String title = "";
        public String Title
        { 
            get { return title; }
            set { SetProperty(ref title, value); }
        }


#endregion


    }
}
