﻿using System;
using System.Threading.Tasks;
using System.Waf.Applications;
using Sams.Services;
using MahApps.Metro.Controls.Dialogs;


namespace Sams.Views
{

    public interface IMainView : IView
    {

        void Show();
        void OpenFlyout(FlyoutConfig view);
        Task<bool> OpenFlyoutAsync(FlyoutConfig view);
        Task CloseFlyoutAsync();
        Task<bool> FlyoutIsOpenAsync();
        
        void ShowMessage(String header, String message);
        
        Task<bool> ConfirmExitAsync(string msg);
    }
}       
