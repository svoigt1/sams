﻿using System;
using Sams.Controllers;
using Sams.Models;


namespace Sams.Commands
{
   
    public interface IControllerCommand
    {

        int         Sortierung     { get; }
        String      Name           { get; }
        String      Parameter      { get; }
        IController Controller     { get; }

        /// <summary>
        /// Wurde Command geklickt
        /// </summary>
        bool        IstAktive { get; set; }

        /// <summary>
        /// Darf Command vom Benutzer ausgeführt werden?
        /// </summary>
        bool GrantedCommand(Nutzer nutzer);


        /// <summary>
        /// verwendet für Menu  
        /// </summary>
        String    NameUppercase { get; }
    }
}
