﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sams.Models;


namespace Sams.Commands
{

    public class CommandFactory : ICommandFactory
    {
        
        private readonly IEnumerable<IControllerCommand> actions;

        public CommandFactory(IEnumerable<IControllerCommand> actions)
        {               
            this.actions = actions; 
        }


        public IControllerCommand this[String cmdname]
        {
            get { return actions.SingleOrDefault(p => p.Parameter == cmdname); }
        }


        public IEnumerable<IControllerCommand> VerfügbareAktionen(Nutzer user)
        {
            return actions.Where(p => p.GrantedCommand(user)).OrderBy(p => p.Sortierung).ToList();    
        }
    }
}
