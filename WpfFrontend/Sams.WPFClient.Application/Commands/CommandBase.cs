﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sams.Commands
{
   
    public abstract class CommandBase : NotifyBase
    {

        private bool istAktive;
        public bool IstAktive
        { 
        
            get { return istAktive; }
            set { SetProperty(ref istAktive, value); }
        }

    }
}
