﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sams.Controllers;
using Sams.Controllers.Melder;
using Sams.Models;


namespace Sams.Commands
{

    public class PublicCommand : CommandBase, IControllerCommand
    {

        private readonly IPublicController controller;


        public PublicCommand(IPublicController controller)
        {
            this.controller = controller;
        }

        public static string _Parameter = "Public";


        public IController  Controller  { get { return controller; } }
        public String       Name        { get { return "Öffentliche Meldungen"; } }
        public String       Parameter   { get { return _Parameter; } }
        public int          Sortierung  { get { return 300; } }


        public bool GrantedCommand(Nutzer nutzer)
        {
        
            if (nutzer == null) return false;
            return true;   
        }

        public String NameUppercase { get { return Name.ToUpper(); } }
    }
}
