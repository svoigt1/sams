﻿using System;
using System.Collections.Generic;
using Sams.Models;


namespace Sams.Commands
{

    public interface ICommandFactory
    {
        
        IControllerCommand              this[String cmdname]    { get; }

        IEnumerable<IControllerCommand> VerfügbareAktionen(Nutzer user);
    }
}
