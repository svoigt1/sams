﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sams.Controllers;
using Sams.Controllers.Melder;
using Sams.Models;


namespace Sams.Commands
{

    public class CreateCommand : CommandBase, IControllerCommand
    {

        private readonly ICreateController controller;


        public CreateCommand(ICreateController controller)
        {
            this.controller = controller;
        }

        public static string _Parameter = "Create";

        public IController  Controller  { get { return controller; } }
        public String       Name        { get { return "Neue Meldung"; } }
        public String       Parameter   { get { return _Parameter; } }
        public int          Sortierung  { get { return 200; } }


        public bool GrantedCommand(Nutzer nutzer)
        {
        
            if (nutzer == null) return false;
            return nutzer.HasRoleMA;   
        }

        public String NameUppercase { get { return Name.ToUpper(); } }
    }
}
