﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Sams.Services;
using Sams.Services.Melder;

using Sams.ViewModels;
using Sams.ViewModels.Melder;
using Sams.ViewModels.Meldungen;
using Sams.Commands;
using Sams.ViewData;
using Sams.Models;
using System.Waf.Applications;

namespace Sams.Controllers.Melder
{
    
    public interface ICreateController : IController 
    {}

    
    public class CreateController : CreateControllerBase, ICreateController
    {
        // Flags
        private bool loadCompleted;

        // Services
        private readonly IBreadcrumbService crumbs;
        private readonly ICreateService store;
        private readonly IUserService userStore;

        // ViewModels
        private readonly HomeViewModel homeView;
        private readonly MelderCreateViewModel createView;


        public CreateController(
            IMessageService messages,
            ICreateService store,
            IUserService userStore,
            IBreadcrumbService crumbs,

            MainViewModel mainView,
            HomeViewModel homeView,
            MelderCreateViewModel createView
            ) 
          : base(messages)
        {
            
            this.crumbs = crumbs;
            this.store = store;
            this.userStore = userStore;

            this.homeView = homeView;
            this.createView = createView;
        }


        public void Initialize()
        {
            createView.SaveCommand = new AsyncDelegateCommand(p => SaveAsync()); 
        }


        public async Task RunAsync(object parameter, CancellationToken ct = default(CancellationToken))
        {

            crumbs.Add("Neue Meldung");
            homeView.SetActionAktive(CreateCommand._Parameter);

            homeView.PageView = createView.View;

            await LoadAsync();

            Task <Nutzer> userTask = userStore.CurrentUserAsync(cts.Token);

            await Task.WhenAll(userTask);
            cts.Token.ThrowIfCancellationRequested();

            await context.SendAsync(new SendOrPostCallback((o) =>
            {
                var user = userTask.Result;
                createView.Meldername = (user != null) ? user.Name : "";
            }), null);

            if (parameter != null)
            {
                var token = parameter as CreateMeldungToken;
                if(token != null) createView.PresetMeldungsTyp(token.meldungsTyp_Id);
            }
            
        }


        public void Shutdown()
        {
        }


        protected override Task<bool> UnloadImplAsync()
        {
            return Task.FromResult(true);  
        }


        protected override async Task LoadImplAsync()
        {
            loadCompleted = false;
            
            using (new VisualDelay(createView))
            {
                var typenTask      = store.GetMeldungstypenAsync(cts.Token);
                var facilitiesTask = store.GetEinrichtungenAsync(cts.Token);

                await Task.WhenAll(typenTask, facilitiesTask);

                cts.Token.ThrowIfCancellationRequested();

                await context.SendAsync(new SendOrPostCallback((o) =>
                {
                    
                    createView.Einrichtungen1   = facilitiesTask.Result.ToList();
                    createView.MeldungentypList = typenTask.Result.Select(p => new MeldungstypViewItem(p)).ToList();
                    
                }), null);
            }

            loadCompleted = true;
        }
        
        protected override async Task SaveImplAsync()
        {
            Console.WriteLine("SaveImplAsync.......");

            await store.SaveAsync(createView.Current);

            createView.ResetForm();

            await messages.ShowMessageAsync(new MessageToken { Header = "Meldung wurde gespeichert." });

        }
    }
}
