﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Sams.Services;


namespace Sams.Controllers.Melder
{

    public abstract class PublicControllerBase
    {
      
        private readonly SemaphoreSlim locker = new SemaphoreSlim(1, 1);
        protected CancellationTokenSource cts = new CancellationTokenSource();
        protected readonly SynchronizationContext context = SynchronizationContext.Current;
        protected readonly IMessageService messages;
                                

        public PublicControllerBase(IMessageService messages)
        {
            this.messages = messages;
        }

        
        protected abstract Task<bool> UnloadImplAsync();
        protected abstract Task LoadImplAsync(object obj);
        protected abstract Task SearchImplAsync();
        protected abstract Task DetailImplAsync();


        protected async Task SearchAsync()
        {
            
            await locker.WaitAsync(cts.Token);
            
            try {

                await SearchImplAsync();
            }
            catch (Exception x) {

                messages.Show(x);    
            }
            finally {
   
                locker.Release();
            }
        }


        protected async Task LoadAsync(object obj)
        {
            
            await locker.WaitAsync(cts.Token);
            
            try {

                await LoadImplAsync(obj);
            }
            catch (Exception x) {

                messages.Show(x);    
            }
            finally {
   
                locker.Release();
            }
        }


        public async Task<bool> UnloadAsync()
        {
            
            await locker.WaitAsync(cts.Token);
            
            try {

                return await UnloadImplAsync();
            }
            catch (Exception x) {

                messages.Show(x); 
                return false;   
            }
            finally {
   
                locker.Release();
            }
        }

        protected async Task DetailAsync()
        {

            await locker.WaitAsync(cts.Token);

            try
            {

                await DetailImplAsync();
            }
            catch (Exception x)
            {

                messages.Show(x);
            }
            finally
            {

                locker.Release();
            }
        }


        public void Cancel()
        {
            
            cts.Cancel();
            cts = new CancellationTokenSource();
        }
    }
}
