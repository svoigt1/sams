﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Sams.Services;
using Sams.Services.Melder;
using Sams.ViewModels;
using Sams.ViewModels.Melder;
using Sams.ViewModels.Meldungen;
using Sams.ViewData;
using System.Waf.Applications;
using Sams.Models;
using System.ComponentModel;
using Sams.ViewModels.Common;
using Sams.Commands;
using Sams.Factories;

namespace Sams.Controllers.Melder
{
    
    public interface IPublicController : IController 
    {}

    
    public class PublicController : PublicControllerBase, IPublicController
    {
        // Flags
        private bool loadCompleted;

        // Services
        private readonly IBreadcrumbService crumbs;
        private readonly IPublicService store;

        // ViewModels
        private readonly HomeViewModel homeView;
        private readonly MelderPublicViewModel publicView;
        private readonly MelderFilterViewModel filterView;
        private readonly MelderListViewModel listView;
        private readonly MelderFormViewModel formView;
        private readonly LegendeViewModel legendeView;


        public PublicController(IMessageService messages, IBreadcrumbService crumbs, IPublicService store,
            HomeViewModel homeView,
            MelderPublicViewModel publicView,
            MelderFilterViewModel filterView,
            MelderListViewModel listView,
            MelderFormViewModel formView,
            LegendeViewModel legendeView,

            IFactory<LegendeViewModel> legends
           ) 
          : base(messages)
        {

            this.store = store;

            this.crumbs = crumbs;
            this.homeView = homeView;
            this.publicView = publicView;
            this.filterView = filterView;
            this.listView = listView;
            this.formView = formView;
            this.legendeView = legends.Create();
        }


        public void Initialize()
        {

            Console.WriteLine("Public Controller Initialize........");

            publicView.FilterView = filterView.View;
            publicView.ListView = listView.View;
            publicView.DetailView = formView.View;
            publicView.LegendeView = legendeView.View;

            // Command actions
            // ListView
            listView.DetailCommand = new AsyncDelegateCommand(_ => DetailAsync());

            // FilterView
            filterView.SearchCommand = new AsyncDelegateCommand(_ => SearchAsync());

        }


        public async Task RunAsync(object parameter, CancellationToken ct = default(CancellationToken))
        {


            crumbs.Add("Öffentliche Meldungen");
            homeView.PageView = publicView.View;

            

            // MeldungExtendedViewItem
            var item = parameter as MeldungExtendedViewItem;
            if(item != null)
            {
                Console.WriteLine("Load by Item....");
                await LoadAsync(item);

                var meldung = item.Model;
                if(meldung != null)
                {
                    listView.Current = listView.MeldungenList.FirstOrDefault(p => p.Id == meldung.Id); 
                    if(listView.Current == null)
                    {
                        var tempItem = new MeldungViewItemBase(meldung);
       
                        listView.MeldungenList.Add(tempItem);
                        listView.Current = tempItem;
                                 
                    }
                    
                    formView.Current = meldung;
                    filterView.setFilterOptions(item.ownership);
                    homeView.SetActionAktive(PublicCommand._Parameter);
                }
            }
            else
            {
                Console.WriteLine("Load by NULL....");
                await LoadAsync(null);
            }
        }


        public void Shutdown()
        {
        }


        protected override Task<bool> UnloadImplAsync()
        {
            return Task.FromResult(true);  
        }


        protected override async Task LoadImplAsync(object obj)
        {
            Console.WriteLine("Public Controller LoadImplAsync..................................");

            loadCompleted = false;

            using (new VisualDelay(listView))
            using (new VisualDelay(formView))
            {
                Task<IList<Meldung>> listTask = null;

                if (obj != null)
                {
                   
                    var item = obj as MeldungExtendedViewItem;
                    Console.WriteLine("Item ownership:"+item.ownership);
                    switch (item.ownership)
                    {
                        case "all": listTask = store.PublicAllAsync(cts.Token); break;
                        case "private": listTask = store.PrivateAllAsync(cts.Token); break;
                        default: listTask = store.PublicAllAsync(cts.Token); break;
                    }
                } else
                {
                    listTask = store.PrivateAllAsync(cts.Token);
                }

                var typenTask = store.GetMeldungstypenAsync(cts.Token);
                var facilitiesTask = store.getEinrichtungenAsync(cts.Token);

                await Task.WhenAll(typenTask, listTask, facilitiesTask /*, typTask, pubTask, commonTask */);

                cts.Token.ThrowIfCancellationRequested();

                await context.SendAsync(new SendOrPostCallback((o) =>
                {
                    listView.MeldungenList = listTask.Result.Select(p => new MeldungViewItemBase(p)).ToList();
                    formView.Einrichtungen1 = facilitiesTask.Result.ToList();
                    formView.MeldungentypList = typenTask.Result.Select(p => new MeldungstypViewItem(p)).ToList();

                    legendeView.MeldungentypList = typenTask.Result.Select(p => new LegendeViewItem(p)).ToList();

                }), null);
            }

            loadCompleted = true;
        }


        protected override async Task SearchImplAsync()
        {
            Console.WriteLine("SearchImplAsync ....");
            if (loadCompleted)
            {
                Console.WriteLine("LoadListAsync ....");
                // await Task.Yield();
                await LoadListAsync();
            }
            else
            {
                Console.WriteLine("LoadImplAsync ....");
                await LoadImplAsync(null);
            }            
        }

        private async Task LoadListAsync()
        {

  
            using (new VisualDelay(publicView))
            {

                listView.Current = null;
                SetModel(null);

                Task<IList<Meldung>> listTask = null;

                if (filterView.SelectedOwnershipButton != null)
                {
                    if(filterView.SelectedOwnershipButton.FilterTerm == "all")
                    {
                        Console.WriteLine("Hole Öffentliche");
                        listTask = store.PublicAllAsync(cts.Token);
                    }

                    if (filterView.SelectedOwnershipButton.FilterTerm == "private")
                    {
                        Console.WriteLine("Hole Eigene");
                        listTask = store.PrivateAllAsync(cts.Token);
                    }
                }

                await Task.WhenAll(listTask);

                await context.SendAsync(new SendOrPostCallback((o) =>
                {
                    listView.MeldungenList = listTask.Result.Select(p => new MeldungViewItemBase(p)).ToList();

                }), null);
               
            }
        }


        protected override async Task DetailImplAsync()
        {

            if (listView.Current != null)
            {

                using (new VisualDelay(formView))
                {

                    Console.WriteLine("Current.Id:" + listView.Current.Id);
                    await Task.Yield();

                    var model = await store.GetMeldungAsync(listView.Current.Id);
                    SetModel(model);
                }

            }
            else
            {

                SetModel(null);
            }
        }

        private void SetModel(Meldung current)
        {
            if (current != null)
            {
                formView.Current = current;
                formView.Meldungstyp = formView.MeldungentypList.SingleOrDefault(p => p.Id == current.Meldungstyp.Id);  //Select(p => p).Where(p.Id = current.Meldungstyp.Id);

                formView.Einrichtung1 = formView.Einrichtungen1.FirstOrDefault(p => formView.Current.Einrichtung1_Id == p.Id);
                formView.Einrichtung2 = formView.Einrichtungen2 != null ? formView.Einrichtungen2.FirstOrDefault(p => formView.Current.Einrichtung2_Id == p.Id) : null;
                formView.Einrichtung3 = formView.Einrichtungen3 != null ? formView.Einrichtungen3.FirstOrDefault(p => formView.Current.Einrichtung3_Id == p.Id) : null;
                formView.Einrichtung4 = formView.Einrichtungen4 != null ? formView.Einrichtungen4.FirstOrDefault(p => formView.Current.Einrichtung4_Id == p.Id) : null;
            }

            /*
            UpdateCommands();
            */
        }
    }
}
