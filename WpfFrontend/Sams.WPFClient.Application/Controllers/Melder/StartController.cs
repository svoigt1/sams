﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Sams.Services;
using Sams.Services.Melder;
using Sams.ViewModels;
using Sams.ViewModels.Melder;
using Sams.ViewModels.Meldungen;
using Sams.ViewModels.Buttons;
using Sams.Models;
using Sams.ViewData;
using Sams.Services.Global;
using Sams.Commands;
using System.Windows;
using Sams.Controllers;
using Sams.ViewModels.Common;
using Sams.Factories;

namespace Sams.Controllers.Melder
{
    
    public interface IStartController : IController 
    {}

    
    public class StartController : StartControllerBase, IStartController
    {

        private readonly IBreadcrumbService crumbs;
        private readonly IStartService store;

        private readonly HomeViewModel homeView;

        private readonly MelderStartViewModel startView;
        private readonly MelderPublicViewModel publicView;
        private readonly CurrentListViewModel currentView;
        private readonly MyListViewModel myView;
        private readonly MelderCreateViewModel createView;
        private readonly MelderFormViewModel formView;
        private readonly LegendeViewModel legendeView;

        private readonly IPublicController pubController;
        private readonly ICreateController createController;


        public StartController(
            IMessageService messages,
            IBreadcrumbService crumbs,
            IStartService store,

            IPublicController pubController,
            ICreateController createController,

            HomeViewModel homeView, 
            
            MelderStartViewModel startView,
            MelderPublicViewModel publicView,
            CurrentListViewModel currentView, 
            MyListViewModel myView,
            
            MelderCreateViewModel createView, 
            MelderFormViewModel formView,

            IFactory<LegendeViewModel> legends

        ) 
          : base(messages)
        {

            this.crumbs = crumbs;
            this.homeView = homeView;
            this.startView = startView;
            this.publicView = publicView;
            this.currentView = currentView;
            this.myView = myView;
            this.createView = createView;
            this.formView = formView;
            this.store = store;

            this.pubController = pubController;
            this.createController = createController;
            legendeView = legends.Create();
        }


        public void Initialize()
        {

            startView.CurrentView = currentView.View;
            startView.MyView = myView.View;
           // startView.LegendeView = legends.Create().View;
            startView.LegendeView = legendeView.View;

            startView.CreateCommand = new AsyncDelegateCommand(p => CreateAsync(p));
            currentView.DetailCommand = new AsyncDelegateCommand(_ => DetailAsync());

            PropertyChangedEventManager.AddHandler(currentView, ViewPropertyChanged, "");
            PropertyChangedEventManager.AddHandler(myView, ViewPropertyChanged, "");

        }


        public async Task RunAsync(object parameter, CancellationToken ct = default(CancellationToken))
        {

            crumbs.Add("Start");
            homeView.PageView = startView.View;


            using (new VisualDelay(myView))
            using (new VisualDelay(currentView))
            {
                Suchkriterien query = new Suchkriterien();
                query.Add("Page", 1);
                query.Add("Count", 10);
                query.Add("Ownership", "private");
                query.Add("Draw", 0);

                query.Add("MeldeTypen", new int[] { 1,2,3 });

                var ownTask = store.OwnTop10Async(query, cts.Token);
                var typTask = store.PublicMeldungsTypsAsync(cts.Token);
                var pubTask = store.PublicTop10Async(cts.Token);

                await Task.WhenAll(ownTask, typTask, pubTask);

                await context.SendAsync(new SendOrPostCallback((o) => {
            //startView.LegendeView = legendeView.View;
                    myView.Meldungen = ownTask.Result.Select(p => new MeldungViewItemBase(p)).ToList();
                    startView.BtnCollection = typTask.Result.Select(p => new MeldungsTypButtonViewItem(p)).ToList();
                    currentView.TopMeldungen = pubTask.Result.Select(p => new MeldungViewItemBase(p)).ToList();

                    legendeView.MeldungentypList = typTask.Result.Select(p => new LegendeViewItem(p)).ToList();
                }), null);
                
            }
        }


        public void Shutdown()
        {
            PropertyChangedEventManager.RemoveHandler(currentView, ViewPropertyChanged, "");
            PropertyChangedEventManager.RemoveHandler(myView, ViewPropertyChanged, "");
        }


        private void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {

            try {
          
                if (args.PropertyName == "Selected")
                {
                    Console.WriteLine("Selected changed");

                    if (sender == currentView && currentView.Selected != null)
                    {
                        Console.WriteLine("Detail anzeigen currentView");
                       
                        pubController.RunAsync(new MeldungExtendedViewItem(currentView.Selected.Model, "all"));
   
                    }
                    
                    if(sender == myView && myView.Selected != null)
                    {
                        Console.WriteLine("Detail anzeigen myView");

                        pubController.RunAsync(new MeldungExtendedViewItem(myView.Selected.Model, "private"));
                    }                   
                }
            }
            catch (Exception x) {

                messages.Show(x);    
            }
        }



        protected override Task<bool> UnloadImplAsync()
        {
            return Task.FromResult(true);  
        }


        protected override async Task LoadImplAsync()
        {
            await Task.Yield();
        }


        protected override async Task CreateImplAsync(object p)
        {
            var obj = p as MeldungsTypButtonViewItem;
            await createController.RunAsync(new CreateMeldungToken(obj.Id));
        }


        protected override async Task DetailImplAsync()
        {
            Console.WriteLine("DetailImplAsync ...............................................");
            await Task.Yield();
        }
    }
}
