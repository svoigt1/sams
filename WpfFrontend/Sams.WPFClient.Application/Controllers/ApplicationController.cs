﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows;
using Sams.Commands;
using Sams.Controllers.Melder;
using Sams.Dtos;
using Sams.Services;
using Sams.ViewData;
using Sams.ViewModels;
using Sams.ViewModels.Common;


namespace Sams.Controllers
{
    
    public class ApplicationController
    {
        
        private readonly SynchronizationContext context = SynchronizationContext.Current;
        private readonly IMessageService messages;
        private readonly MainViewModel mainView;
        private readonly HomeViewModel homeView;
        private readonly MeinungViewModel meinungView;
        private readonly IFlyoutService flyouts;
        private readonly IBreadcrumbService crumbs;
        private readonly IControllerService controllers;
        private readonly IUserService userStore;
        private readonly IMailService mailerStore;
        private readonly IMeldungenService meldungenStore;
        private readonly ICommandFactory commands;
        private readonly IStartController StartController;
        private CancellationTokenSource cts = new CancellationTokenSource();

        
        private IController currentController;


        public ApplicationController(MainViewModel mainView, HomeViewModel homeView, MeinungViewModel meinungView, IMessageService messages, ICommandFactory commands, 
                                     IFlyoutService flyouts, IBreadcrumbService crumbs, IControllerService controllers, IUserService userStore, IMailService mailerStore, IMeldungenService meldungenStore,
        IStartController StartController)
        {
        
            this.messages = messages;
            this.mainView = mainView;
            this.homeView = homeView;
            this.meinungView = meinungView;
            this.flyouts = flyouts;
            this.crumbs = crumbs;
            this.controllers = controllers;
            this.commands = commands;
            this.userStore = userStore;
            this.mailerStore = mailerStore;
            this.meldungenStore = meldungenStore;
            this.StartController = StartController;
        }


        public void Initialize()
        {

            try {

                mainView.ConfigCommand = new AsyncDelegateCommand(_ => OpenConfigAsync());
                mainView.CancelCommand = new DelegateCommand(CancelCurrentController);
                mainView.HomeCommand = new AsyncDelegateCommand(_ => GoHomeAsync());
                mainView.ActionCommand = new AsyncDelegateCommand(p => ExecuteActionAsync(p));
                mainView.HelpCommand = new DelegateCommand(Help);
                mainView.ExitCommand = new AsyncDelegateCommand(_ => BeendenAsync());
                homeView.ActionCommand = mainView.ActionCommand;
                homeView.MeinungCommand = new AsyncDelegateCommand(_ => MeinungAsync());
                homeView.SearchCommand = new AsyncDelegateCommand(p => SearchAsync(p));
                crumbs.Clear();
                controllers.InitializeAll();
            }
            catch (Exception x) {

                messages.Show(x);    
            } 
        }


        public async void Run()
        {

            try {

                mainView.PageView = homeView.View;
                mainView.Show();
                
                var user = await userStore.CurrentUserAsync(cts.Token);
                homeView.Actions = commands.VerfügbareAktionen(user);
                homeView.UserName = user.Name;

                homeView.SetActionAktive(StartCommand._Parameter);
               
                await StartController.RunAsync(null, cts.Token);
            }
            catch (Exception x) {

                messages.Show(x);    
            } 
        }


        /// <summary>
        /// aktives Modul entladen
        /// </summary>
        /// <returns>false: neues Modul nicht laden da noch ungespeicherte Daten vorliegen</returns>
        private async Task<bool> StopCurrentControllerAsync()
        {
            
            if (currentController == null) return true;
            await flyouts.CloseAsync();
            return await currentController.UnloadAsync();
        }

        /// <summary>
        /// aktuelle Aktion abbrechen (CancellationToken senden)
        /// </summary>
        private void CancelCurrentController()
        {
            
            if (currentController == null) {

                cts.Cancel();
                cts = new CancellationTokenSource();

            } else {

                currentController.Cancel();   
            }
        }


        private async Task RunControllerAsync(IController controller, object parameter)
        {

            try {

                crumbs.Clear();
                currentController = controller;
                mainView.IsFlyoutOpen = false;
               
                if (controller == null) await GoHomeAsync();
                else await controller.RunAsync(parameter);
            }
            catch (Exception x) {

                messages.Show(x);    
            } 
        }


        public async Task ExecuteActionAsync(object obj)
        {

            if (obj == null) return;
            var action = commands[obj.ToString()];
            if (action != null) {
           
                var command = action as IControllerCommand;
                if (command != null && command.Controller != null) {

                    this.SetAllCommandsInaktive(action);
                   
                    await RunControllerAsync(command.Controller, obj);

                    return;
                }
            }

            await messages.ShowMessageAsync(new MessageToken { Header = "Dieses Modul steht leider derzeit nicht zur Verfügung." }); 
            return;
        }


        public void Shutdown()
        {
            controllers.ShutdownAll();
        }


        private async Task GoHomeAsync()
        {

            try {

                if (!await StopCurrentControllerAsync()) return;
                currentController = null;    
                await flyouts.CloseAsync();
                crumbs.Clear();
                mainView.PageView = homeView.View;

            }
            catch (Exception x) {

                messages.Show(x);    
            } 
        }


        private void Help()
        {

            try {

                if (!File.Exists("Handbuch.pdf")) return;
                System.Diagnostics.Process.Start("Handbuch.pdf");
            }
            catch (Exception x) {

                messages.Show(x);    
            } 
        }


        private async Task BeendenAsync()
        {
            
            if (!await StopCurrentControllerAsync()) return;
            context.Send(new SendOrPostCallback((o) => { Application.Current.Shutdown(); }), null);
        }


        private async Task OpenConfigAsync()
        {
            
            if (!await StopCurrentControllerAsync()) return;
            await RunControllerAsync(controllers.ConfigController, null);
        }

        private async Task MeinungAsync()
        {

            meinungView.SendCommand = flyouts.YesCommand;
            meinungView.CancelCommand = flyouts.NoCommand;

            if (!await flyouts.OpenConfirmAsync(new FlyoutConfig { ChildView = meinungView.View, Header = "", Position = MahApps.Metro.Controls.Position.Right }))
            {
                meinungView.ResetForm();
                return;
            }
            else
            {
                // sende meinung über store
                await mailerStore.SendOpinionAsync(meinungView.Betreff, meinungView.Body, cts.Token);

                meinungView.ResetForm();
                return;
            }
               
        }

        private async Task SearchAsync(Object obj)
        {
            Console.WriteLine("SearchAsync ..............................................."+obj.ToString());

            MeldungFilter filter = new MeldungFilter();
            filter.Page = 1;
            filter.Count = 10000; // TODO ändere api im Backend
            filter.Query = obj.ToString();

            MeldungListDto Suchergebnis = await meldungenStore.SearchMeldungenAsync(filter, cts.Token);

            await Task.Yield();
        }

        private void SetAllCommandsInaktive(IControllerCommand a)
        {
            foreach(IControllerCommand action in homeView.Actions)
            {
                action.IstAktive = action.Name == a.Name;
            }
        }
    }
}
