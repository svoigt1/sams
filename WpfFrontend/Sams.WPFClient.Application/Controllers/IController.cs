﻿using System;
using System.Threading;
using System.Threading.Tasks;


namespace Sams.Controllers
{
  
    public interface IController
    {

        /// <summary>
        /// Handler aufsetzen und Ressourcen anfordern
        /// </summary>
        void Initialize();
        /// <summary>
        /// Daten laden
        /// </summary>
        Task RunAsync(object parameter, CancellationToken ct = default(CancellationToken));
        /// <summary>
        /// Aktuelle Aufgabe abbrechen
        /// </summary>
        void Cancel();
        /// <summary>
        /// Modul wechseln
        /// </summary>
        /// <remarks>
        /// Bei ungespeicherter Bearbeitung nachfragen
        /// </remarks>
        /// <returns>true: kann fortfahren, false: abbrechen (Bearbeitung noch nicht gespeichert)</returns>
        Task<bool> UnloadAsync();
        /// <summary>
        /// Ressourcen freigeben
        /// </summary>
        void Shutdown();
    }
}
