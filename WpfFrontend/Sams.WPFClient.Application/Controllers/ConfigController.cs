﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Sams.Services;
using Sams.ViewModels;
using Sams.ViewModels.Settings;


namespace Sams.Controllers
{
    
    public class ConfigController : IConfigController
    {
        
        private readonly IMessageService messageSvc;
        private readonly SettingsViewModel settingsView;
        private readonly MainViewModel mainView;
        private readonly IBreadcrumbService crumbs;
        
        private CancellationTokenSource cts = new CancellationTokenSource();      


        public ConfigController(MainViewModel mainView, SettingsViewModel settingsView, IMessageService messageSvc,
                                IBreadcrumbService crumbs)
        {

            this.settingsView = settingsView;
            this.mainView = mainView;
            this.messageSvc = messageSvc;
            this.crumbs = crumbs;
        }


        public virtual void Initialize()
        {
        }


        public virtual Task RunAsync(object parameter, CancellationToken ct = default(CancellationToken))
        {

            mainView.PageView = settingsView.View;
            crumbs.Add("Einstellungen");
            return Task.FromResult(true);
        }


        public virtual void Cancel()
        {

            cts.Cancel();
            cts = new CancellationTokenSource();
        }


        public virtual Task<bool> UnloadAsync()
        {
            return Task.FromResult(true);
        }


        public virtual void Shutdown()
        {
        }
    }
}
