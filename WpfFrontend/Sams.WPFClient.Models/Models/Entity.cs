﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Sams.Models
{

    public abstract class Entity : NotifyBase
    {

        public int Id { get; set; }
        public bool IstAngefügt { get { return Id <= 0; } }
    }
}
