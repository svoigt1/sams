﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Sams.Models
{
    
    public class Person : Entity
    {

        public String   Sid                 { get; set; }
        public String   Loginname           { get; set; }
        public String   Name                { get; set; }
        public String   Organization        { get; set; }
        public String   EmailAddress        { get; set; }
    }
}
