﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;


namespace Sams.Models
{

    public  class Meldung : Entity, IDataErrorInfo, INotifyPropertyChanged
    {

        /// <summary>
        /// Nummer der Meldung zur Referenz, muss noch spezifiziert werden (bleibt leer bei Neuerstellung durch den Melder)
        /// </summary>
        public String           Nummer              { get; set; }
        /// <summary>
        /// Melder wünscht keine Speicherung seiner Identität (Backend liest den Melder nicht aus dem AD aus)
        /// </summary>
        public bool             IstAnonym           { get; set; }
        /// <summary>
        /// Bevor, Cirs, (es wird das gesamte Meldungstyp-Objekt gegeben, damit die Farbinformationen enthalten sind. 
        /// Bei Neuerstellung kann auch einfach ein Objekt gegeben werden bei dem die nur die Id gesetzt ist, Name und Color bleiben leer) 
        /// </summary>
        public Meldungstyp      Meldungstyp         { get; set; }
        /// <summary>
        /// ID Meldungstyp zum Senden neuer Meldung
        /// </summary>
        public int              Meldungstyp_Id      { get; set; }
        /// <summary>
        /// Erstellung der Meldung (nicht unbedingt Zeitpunkt des Auftretens des Vorkommnisses, bleibt leer bei Neuerstellung durch den Melder)
        /// </summary>
        public DateTime         Erstellt            { get; set; }
        /// <summary>
        /// alternative Angabe des Melders (falls Station nur ein AD-Account besitzt) 
        /// </summary>
        public String           Meldername          { get; set; }
        /// <summary>
        /// alternative Angabe der E-Mail des Melders (falls Station nur ein AD-Account besitzt) 
        /// </summary>
        public String           MelderEMail         { get; set; }
        /// <summary>
        /// Textform der Einrichtung zu einfachen Darstellung (bleibt leer bei Neuerstellung durch den Melder)
        /// </summary>
        public String           Einrichtung         { get; set; }
        /// <summary>
        /// zum Speichern der Meldung: ID der Einrichtung erstes Level (z.B. Krankenhaus), Pflichtfeld
        /// </summary>
        public int              Einrichtung1_Id     { get; set; }
        /// <summary>
        /// zum Speichern der Meldung: ID der Einrichtung zweites Level (z.B. Abteilung), Pflichtfeld
        /// </summary>
        public int              Einrichtung2_Id     { get; set; }
        /// <summary>
        /// zum Speichern der Meldung: ID der Einrichtung drittes Level (z.B. Station), optional
        /// </summary>
        public int?             Einrichtung3_Id     { get; set; }
        /// <summary>
        /// zum Speichern der Meldung: ID der Einrichtung viertes Level (z.B. Raum), optional
        /// </summary>
        public int?             Einrichtung4_Id     { get; set; }
        /// <summary>
        /// Neu = 0, *Bearbeitung = 1*, Wartend = 2, Abgeschlossen = 3 (bleibt leer bei Neuerstellung durch den Melder)
        /// </summary>
        public int              Status              { get; set; }
        /// <summary>
        /// Meldung ist öffentlich (bleibt leer bei Neuerstellung durch den Melder)
        /// </summary>
        public bool             IstPublic           { get; set; }
        /// <summary>
        /// Undefiniert = 0, Abgelehnt = 1, Akzeptiert = 2, Realisiert = 3, Geplant = 4,  Spam = 5 (bleibt leer bei Neuerstellung durch den Melder)
        /// </summary>
        public int              Ergebnis            { get; set; }
        /// <summary>
        /// Begründung Ergebnis durch den Sachbearbeiter (bleibt leer bei Neuerstellung durch den Melder)
        /// </summary>
        public String           Begruendung         { get; set; }
        /// <summary>
        /// Titel der Meldung, ausgefüllt vom Melder
        /// </summary>
        [Required(ErrorMessage = "Login can not be empty")]
        public String           Betreff             { get; set; }
        /// <summary>
        /// Body der Meldung, ausgefüllt vom Melder
        /// </summary>
        public String           Kurzbeschreibung    { get; set; }
        /// <summary>
        /// Verbesserungsvorschlag, ausgefüllt vom Melder, optional
        /// </summary>
        public String           Vorschlag           { get; set; }
        /// <summary>
        /// Undefiniert= 0, Planung= 1, Personal= 2, Organisation= 3,OperativesGeschäft= 4,Rechnungswesen= 5,Sicherheit= 6,ITRisiken= 7,
        /// JuristischeRisiken  = 8,MedizinischeRisiken = 9,Umweltschutz= 10 (bleibt leer bei Neuerstellung durch den Melder)
        /// </summary>
        public int              Risikoklasse        { get; set; }
        /// <summary>
        /// Melder (Anzeige bei QS, bleibt leer bei Neuerstellung durch den Melder)
        /// </summary>
        public Person           Melder              { get; set; }
        /// <summary>
        /// Bearbeiter der Meldung (Anzeige bei QS, bleibt leer bei Neuerstellung durch den Melder)
        /// </summary>
        public Person           Bearbeiter          { get; set; }
        /// <summary>
        /// Verantwortlicher der Meldung (Anzeige bei QS, bleibt leer bei Neuerstellung durch den Melder)
        /// </summary>
        public Person           Verantwortlicher    { get; set; }

        public string Error
        {
            get
            {
                var test = this["Betreff"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Einrichtung1_Id"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Einrichtung2_Id"];
                if (!String.IsNullOrEmpty(test)) return test;

                return String.Empty;
            }
        }

        public string this[string columnName]
        {
            get
            {
                //Console.WriteLine("columname :" + columnName);
                switch(columnName)
                {
                    case "Betreff":
                        if (String.IsNullOrEmpty(Betreff))
                            return columnName + " ist ein Pflichtfeld!";
                        if (Betreff.Length > 100)
                            return columnName + " darf nur aus 100 Zeichen bestehen!";
                        break;
                    case "Einrichtung1":
                    case "Einrichtung1_Id":
                        if (Einrichtung1_Id == 0) return "Ort ist ein Pflichtfeld!";
                        break;
                    case "Einrichtung2":
                    case "Einrichtung2_Id":
                        if (Einrichtung2_Id == 0) return "Einrichtung ist ein Pflichtfeld!";
                        break;
                }
                return null;
            }
        }

        #region INotifyPropertyChanged Members

        public new event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}
