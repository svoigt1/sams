﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Sams.Models
{
    
    public class Nutzer : Person
    {

        public bool     HasRoleGF           { get; set; }
        public bool     HasRoleMA           { get; set; }
        public bool     HasRoleQS           { get; set; }
        public bool     HasRoleADM          { get; set; }
        public bool     HasRoleRM           { get; set; }
    }
}
