﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;


namespace Sams
{

    [Serializable]
    public abstract class NotifyBase : INotifyPropertyChanged, IChangeTracking 
    {

        public HashSet<String> ChangedProps { get; private set; }

        
        public void AcceptChanges()
        {
            ChangedProps = null;
        }
      

        public bool IsChanged { get { return ChangedProps != null && ChangedProps.Count > 0; } }


        [field:NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;


        protected bool SetProperty<T>(ref T field, T value, [CallerMemberName] string propertyName = null)
        {
            
            if (object.Equals(field, value)) return false;
            field = value;
            RaisePropertyChanged(propertyName);
            return true;
        }

        /// <remarks>
        /// Leerstrings in Nullwerte wandeln damit NotNull-Constraints in der DB nicht ausgehebelt werden
        /// </remarks>
        protected bool SetProperty(ref String field, String value, [CallerMemberName] string propertyName = null)
        {
            
            if (object.Equals(field, value)) return false;
            if (value == String.Empty) field = null;
            else field = value;
            RaisePropertyChanged(propertyName);
            return true;
        }


        public void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            
            if (propertyName != "IsChanged" && !String.IsNullOrEmpty(propertyName)) {

                if (ChangedProps == null) ChangedProps = new HashSet<String>();
                if (!ChangedProps.Contains(propertyName)) ChangedProps.Add(propertyName);
            }
            OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }
        

        protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, e); 
        }     
    }
}
