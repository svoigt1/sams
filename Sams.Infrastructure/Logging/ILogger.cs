﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Sams
{

    public interface ILogger
    {

        void Debug(string message);
        void DebugFormat(string format,params object[] args);
        void Error(String message);
        void Error(Exception ex);
        void Error(Exception ex, String message);
        void ErrorFormat(string format,params object[] args);
        void ErrorFormat(Exception ex, string format,params object[] args);
        void Info(string message);
        void InfoFormat(string format,params object[] args);
        void Warn(string message);
        void WarnFormat(string format,params object[] args);
    }
}
