﻿using System;
using System.Linq;
using System.IO;
using System.Security;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;

namespace Sams
{
    
    public static class StringExtensions
    {

        public static bool Contains(this String source, String toCheck, StringComparison comp) 
        {
  
            if (source == null) return false;
            return source.IndexOf(toCheck, comp) >= 0;
        }       


        public static String AsFilename(this String docname)
        {

            if (String.IsNullOrEmpty(docname)) return docname;
            var exclist = Path.GetInvalidFileNameChars()
                              .Union(
                          Path.GetInvalidPathChars()).ToList();

            String res = new String(docname.Select(p => exclist.Contains(p)? '_': p).ToArray());
            return res;
        }


        public static String JsonEscaping(this String text)
        {

            if (String.IsNullOrEmpty(text)) return text;
            return text.Replace(@"""", @"\""");
        }


        [SuppressUnmanagedCodeSecurity]
        internal static class SafeNativeMethods
        {
            [DllImport("shlwapi.dll", CharSet = CharSet.Unicode)]
            public static extern int StrCmpLogicalW(string psz1, string psz2);
        }

           
        public static int CompareNatural(this string a, string b)
        {
            return SafeNativeMethods.StrCmpLogicalW(a, b);
        }

        
        public static String Cut(this String text, int maxLen, bool atWord = true, string suffix = "")
        {
       
            if (String.IsNullOrEmpty(text)) return text;
            if (text.Length > maxLen)
            {
                if(atWord)
                {
                    int iNextSpace = text.LastIndexOf(" ", maxLen);
                    return string.Format("{0}{1}", text.Substring(0, (iNextSpace > 0) ? iNextSpace : maxLen).Trim(), suffix);
                }
                return text.Substring(0, maxLen) + suffix;
            }
            return text;
        }

        public static String RemoveLineBreaks(this String text)
        {
            return Regex.Replace(text, @"\r\n?|\n", String.Empty);
        }
    }
}
