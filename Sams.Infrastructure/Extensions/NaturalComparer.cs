﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Sams
{

    public class NaturalComparer : IComparer<String>
    {
        
        public int Compare(string x, string y)
        {
            return x.CompareNatural(y);
        }
    }
}
