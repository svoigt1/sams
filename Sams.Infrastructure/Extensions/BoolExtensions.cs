﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Sams
{

    public static class BoolExtensions
    {

        static public bool AllEqual(this bool firstValue, params bool[] bools)
        {
            return bools.All(thisBool => thisBool == firstValue);
        }    
    }
}
