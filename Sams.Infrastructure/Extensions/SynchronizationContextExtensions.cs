﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace Sams
{
   
    /// <remarks>
    /// http://blogs.msdn.com/b/pfxteam/archive/2012/01/20/10259082.aspx
    /// </remarks>
    public static class SynchronizationContextExtensions
    {

        public static Task SendAsync(this SynchronizationContext context, SendOrPostCallback d, object state)
        {
            
            var tcs = new TaskCompletionSource<bool>();
            
            context.Post(delegate {
                
                try { 
                    
                    d(state);
                    tcs.SetResult(true);
                }
                catch(Exception e) { tcs.SetException(e); }
            
            }, null);
            
            return tcs.Task;
        }
    }
}
