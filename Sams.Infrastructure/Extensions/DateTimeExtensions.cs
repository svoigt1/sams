﻿using System;
using System.Globalization;
using System.Threading;


namespace Sams
{

    /// <summary>
    /// Zeitberechnungs-Hilfsmethoden
    /// </summary>
    public static class DateTimeExtended
    {
    
        /// <summary>
        /// Kalenderwoche ermitteln, dirty workaround Calendar.GetWeekOfYear-Bug
        /// </summary>
        /// <remarks>
        /// Source: http://www.artiso.com/ProBlog/PermaLink,guid,02aee2cd-15f1-4e6e-9e0f-a56645d1f5c8.aspx
        /// </remarks>
        /// <returns>KW des Datums</returns>
        public static int WeekOfYear(this DateTime t)
        {
        
            //Berechnung in sicheren Bereich verschieben
            if (t.DayOfWeek >= DayOfWeek.Monday && t.DayOfWeek <= DayOfWeek.Wednesday) t = t.AddDays(3);
    
            //Aufruf der vorgesehenen DotNet-Funktion
            System.Globalization.Calendar c = Thread.CurrentThread.CurrentCulture.Calendar;

            return c.GetWeekOfYear(t, System.Globalization.CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }

        /// <summary>
        /// Kalenderwoche mit Jahr ermitteln, dirty workaround Calendar.GetWeekOfYear-Bug, Monat mit Jahr ausgeben
        /// </summary>
        /// <returns>Jahr/KW des Datums in der Form 200812 (KW 12 des Jahres 2008)</returns>
        public static int WeekWithYear(this DateTime t)
        {       

            //Berechnung in sicheren Bereich verschieben
            if (t.DayOfWeek >= DayOfWeek.Monday && t.DayOfWeek <= DayOfWeek.Wednesday)
                t = t.AddDays(3);
        
            //Über-/Unterlauf des Jahres ermitteln
            System.Globalization.Calendar c = Thread.CurrentThread.CurrentCulture.Calendar;
            int week = c.GetWeekOfYear(t, System.Globalization.CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
            //Unterlauf
            if (week > 50 && c.GetMonth(t) == 1) return (c.GetYear(t) - 1) * 100 + week;
            //Überlauf
            if (week < 2 && c.GetMonth(t) == 12) return (c.GetYear(t) + 1) * 100 + week;
            //Normales Format
            return c.GetYear(t) * 100 + week;
        }

        /// <summary>
        /// Kalenderwoche als String geben 
        /// </summary>
        public static String KWString(this DateTime? date)
        {

            if (!date.HasValue) return null;
            return KWString(date.Value);
        }

        /// <summary>
        /// Kalenderwoche als String geben 
        /// </summary>
        public static String KWString(this DateTime date)
        {

            int key = WeekWithYear(date);
            return String.Format("{0:D2}/{1:D4}", key % 100, (int)(key / 100));
        }
        
        /// <summary>
        /// Datum des des Montages einer Kalenderwoche berechnen
        /// </summary>
        /// <param name="year">zu verwendendes Jahr</param>
        /// <param name="week">zu verwendende Kalenderwoche</param>
        /// <returns>Datum des des Montages der Kalenderwoche</returns>
        public static DateTime FirstDateOfCalendarWeek(int year, int week)
        {
        
            DateTime date = new DateTime(year, 1, 1);

            //zum Montag hangeln
            while (Thread.CurrentThread.CurrentCulture.Calendar.GetDayOfWeek(date) != DayOfWeek.Monday) date = date.AddDays(-1);
            //Überhang berücksichtigen
            if (date.WeekOfYear() > 50) date = date.AddDays(7);
            //date ist jetzt Datum der KW 1 des Jahres
            return date.AddDays(7 * (week - 1));    
        }
        
        /// <summary>
        /// Montag einer Woche ermitteln, Zeitteil wird nicht verändert
        /// </summary>
        /// <param name="date">beliebiger Tag der Woche</param>
        /// <returns>Montag der Woche, Zeitteil wird nicht verändert, bei Sonntag wird bis zum vorigen Montag vorgelaufen</returns>
        public static DateTime MondayOf(this DateTime date)
        {
            
            while (Thread.CurrentThread.CurrentCulture.Calendar.GetDayOfWeek(date) != DayOfWeek.Monday) date = date.AddDays(-1);
            return date;
        }

        /// <summary>
        /// Formulareingabe: Datum aus DatePicker und Uhrzeit aus Textbox zusammensetzen 
        /// </summary>
        public static DateTime? ParseWithTime(this DateTime? date, String time)
        {

            if (!date.HasValue) return null;
            TimeSpan zeit;
            if (!TimeSpan.TryParseExact(time, @"hh\:mm", null, out zeit)) 
                TimeSpan.TryParseExact(time, @"hhmm", null, out zeit);
            return date + zeit;
        }

        /// <summary>
        /// Formulareingabe: Eingabe von 1200 für 12:00 Uhr ermöglichen
        /// </summary>
        public static TimeSpan? ParseTime(this String time)
        {

            if (String.IsNullOrEmpty(time)) return null;
            TimeSpan zeit;
            if (TimeSpan.TryParseExact(time, @"hh\:mm", null, out zeit)) return zeit;
            if (TimeSpan.TryParseExact(time, @"hhmm", null, out zeit)) return zeit;
            if (TimeSpan.TryParseExact(time, @"h\:mm", null, out zeit)) return zeit;
            if (TimeSpan.TryParseExact(time, @"hmm", null, out zeit)) return zeit;
            return null;
        }


        public static double MonthsBetween(this DateTime von, DateTime bis)
        {

            const double daysPerMonth = 30.4375;

            TimeSpan zeit = bis - von;
            double result = zeit.TotalDays / daysPerMonth;
            return result;
        }


        public static DateTime RoundToSeconds(this DateTime source)
        {

            var date = source.Date;
            var time = source.TimeOfDay;
            time = new TimeSpan(time.Hours, time.Minutes, time.Seconds);

            var dest = date.Add(time);
            return dest;
        }


        public static DateTime? RoundToSeconds(this DateTime? time)
        {

            if (!time.HasValue) return time;
            return time.Value.RoundToSeconds();
        }


        public static TimeSpan RoundToSeconds(this TimeSpan source)
        {

            const long ticksPerSecond = 10 * 1000 * 1000;
            long seconds = source.Ticks / ticksPerSecond;
            var time = new TimeSpan(seconds * ticksPerSecond);
            return time;
        }


        public static TimeSpan? RoundToSeconds(this TimeSpan? time)
        {

            if (!time.HasValue) return time;
            return time.Value.RoundToSeconds();
        }
    }
}
