﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Sams
{
   
    [Serializable]
    public class SamsValidationException : UserException
    {
        
        public IList<String> Explanations   { get; private set; }
        
        
        public SamsValidationException(String message, IList<String> explanations) : base(message)
        {
            Explanations = explanations;
        }                           


        public SamsValidationException(String message, IList<String> explanations, Exception source) : base(message, source)
        {
            Explanations = explanations;
        }


        public override String Explain()
        {
            
            if (Explanations.IsNullOrEmpty()) return Message;
            var list = new List<String> { Message };
            list.AddRange(Explanations);
            return String.Join(System.Environment.NewLine, list);
        }                         
    }
}
