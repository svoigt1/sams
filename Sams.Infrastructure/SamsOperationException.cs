﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Sams
{
   
    [Serializable]
    public class SamsOperationException : UserException
    {

        public SamsOperationException(String message) : base(message)
        {}                           

        public SamsOperationException(String message, Exception source) : base(message, source)
        {}
    }
}
