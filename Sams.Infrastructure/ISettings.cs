﻿using System;
using System.Collections.Generic;


namespace Sams
{
    
    public interface ISettings
    {
        
        String       Theme                      { get; set; }
        String       Accent                     { get; set; }
        String       ApplicationVersion         { get; }
        String       ApplicationName            { get; }
        String       ServerAddress              { get; }

        void         Save();
    }
}
