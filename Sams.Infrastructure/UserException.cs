﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Sams
{
   
    /// <summary>
    /// Vom Anwender verschuldete Fehler (unvollständig ausgefüllte Formulare etc)
    /// </summary>
    [Serializable]
    public class UserException : ApplicationException
    {

        public UserException(String message) : base(message)
        {}


        public UserException(String message, Exception source) : base(message, source)
        {}


        public virtual String Explain() 
        { 
            return Message; 
        } 
    }
}
