﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Sams
{
   
    [Serializable]
    public class SamsNotFoundException : UserException
    {

        public SamsNotFoundException() : base("Das Objekt wurde nicht gefunden.")
        {}                           

        public SamsNotFoundException(String message) : base(message)
        {}                           

        public SamsNotFoundException(String message, Exception source) : base(message, source)
        {}
    }
}
